import { ItemProps } from "types";

export const dummyTravels: ItemProps[] = [
  {
    id: "1",
    date: new Date(),
    type: "traveller",
    rateORitem: "RS 1500",
    spaceORweight: "30KG",
    toDestination: "Karachi",
    fromDestination: "New York",
  },
  {
    id: "2",
    date: new Date(),
    type: "traveller",
    rateORitem: "RS 1500",
    spaceORweight: "30KG",
    toDestination: "Karachi",
    fromDestination: "New York",
  },
  {
    id: "3",
    date: new Date(),
    type: "traveller",
    rateORitem: "RS 1500",
    spaceORweight: "30KG",
    toDestination: "Karachi",
    fromDestination: "New York",
  },
  {
    id: "4",
    date: new Date(),
    type: "traveller",
    rateORitem: "RS 1500",
    spaceORweight: "30KG",
    toDestination: "Karachi",
    fromDestination: "New York",
  },
];

export const dummyOrders: ItemProps[] = [
  {
    id: "5",
    date: new Date(),
    type: "orderer",
    rateORitem: "iPhone X",
    spaceORweight: "1KG",
    toDestination: "Karachi",
    fromDestination: "Saudia",
  },
  {
    id: "6",
    date: new Date(),
    type: "orderer",
    rateORitem: "MacBook Pro 15",
    spaceORweight: "7KG",
    toDestination: "Karachi",
    fromDestination: "California",
  },
  {
    id: "7",
    date: new Date(),
    type: "orderer",
    rateORitem: "PS 6",
    spaceORweight: "20KG",
    toDestination: "Karachi",
    fromDestination: "New York",
  },
  {
    id: "8",
    date: new Date(),
    type: "orderer",
    rateORitem: "Air Pods",
    spaceORweight: "0.7KG",
    toDestination: "Karachi",
    fromDestination: "New York",
  },
];
