export const SuggestionTypes = [
  {
    id: Math.random(),
    name: "Improvement",
  },
  {
    id: Math.random(),
    name: "Feature Request",
  },
  {
    id: Math.random(),
    name: "Bug",
  },
  {
    id: Math.random(),
    name: "General Feedback",
  },
];
