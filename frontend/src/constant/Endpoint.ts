const Endpoints = {
  Requests: "/request",
  SignUp: "/auth/signup",
  VerifyCode: "/auth/verifyCode",
  SignIn: "/auth/signin",
  SetPassword: "/auth/setPassword",
  SetProfile: "/users/profile",
  GetStats: "/dashboard/stats",
  GetRecentPlans: "/plan/recent",
  GetRecentOrders: "/order/recent",
  GetOrders: "/order",
  GetCities: "/city",
  Request: "/request/create",
  MarkRequest: "/request/mark",
  GetOrderById: "/order/byId",
  GetPlanById: "/plan/byId",
  MyPlans: "/plan/myplans",
  Plan: "/plan/create",
  Order: "/order/create",
  GetPlan: "/plan",
  GetOrder: "/order",
  SearchPlans: "/plan/search",
  SearchOrder: "/order/search",
  MyOrders: "/order/myorders",
  MarkOrderFull: "/order/markfull",
  MarkPlanFull: "/plan/markfull",
  FavouriteOrder: "/order/favourite",
  FavouritePlan: "/plan/favourite",
  MyApprovedRequests: "/request/myapprovedrequest",
  MyPendingRequests: "/request/mypendingrequest",
};

export default Endpoints;
