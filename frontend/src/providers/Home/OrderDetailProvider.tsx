import useGetOrderDetails from "@hooks/Home/useGetOrderDetails";
import * as React from "react";

const OrderDetailContext = React.createContext<any>([]);

export const OrderDetailProvider = ({
  children,
}: {
  children: React.ReactChild;
}) => {
  const [...values] = useGetOrderDetails();

  return (
    <OrderDetailContext.Provider value={[...values]}>
      {children}
    </OrderDetailContext.Provider>
  );
};

const useOrderDetailProvider = () => {
  return React.useContext(OrderDetailContext);
};

export default useOrderDetailProvider;
