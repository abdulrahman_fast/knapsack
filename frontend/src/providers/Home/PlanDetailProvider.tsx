import useGetPlanDetails from "@hooks/Home/useGetPlanDetails";
import * as React from "react";

const PlanDetailContext = React.createContext<any>([]);

export const PlanDetailProvider = ({
  children,
}: {
  children: React.ReactChild;
}) => {
  const [...values] = useGetPlanDetails();

  return (
    <PlanDetailContext.Provider value={[...values]}>
      {children}
    </PlanDetailContext.Provider>
  );
};

const usePlanDetailProvider = () => {
  return React.useContext(PlanDetailContext);
};

export default usePlanDetailProvider;
