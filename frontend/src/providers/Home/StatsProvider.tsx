import useStats from "@hooks/Home/useStats";
import * as React from "react";

const StatsContext = React.createContext<any>([]);

export const StatsProvider = ({ children }: { children: React.ReactChild }) => {
  const [...values] = useStats();

  return (
    <StatsContext.Provider value={[...values]}>
      {children}
    </StatsContext.Provider>
  );
};

const useStatsProvider = () => {
  return React.useContext(StatsContext);
};

export default useStatsProvider;
