import useMyPendingRequest from "@hooks/Menu/useMyPendingRequest";
import * as React from "react";

const MyPendingsRequestContext = React.createContext<any>([]);

export const MyPendingsRequestProvider = ({
  children,
}: {
  children: React.ReactChild;
}) => {
  const [...values] = useMyPendingRequest();

  return (
    <MyPendingsRequestContext.Provider value={[...values]}>
      {children}
    </MyPendingsRequestContext.Provider>
  );
};

const useMyPendingsRequestProvider = () => {
  return React.useContext(MyPendingsRequestContext);
};

export default useMyPendingsRequestProvider;
