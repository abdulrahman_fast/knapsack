import useMyOrders from "@hooks/Menu/useMyOrder";
import * as React from "react";

const MyOrdersContext = React.createContext<any>([]);

export const MyOrderProvider = ({
  children,
}: {
  children: React.ReactChild;
}) => {
  const [...values] = useMyOrders();

  return (
    <MyOrdersContext.Provider value={[...values]}>
      {children}
    </MyOrdersContext.Provider>
  );
};

const useMyOrderProvider = () => {
  return React.useContext(MyOrdersContext);
};

export default useMyOrderProvider;
