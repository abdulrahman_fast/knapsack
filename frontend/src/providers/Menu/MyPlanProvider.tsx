import useMyPlans from "@hooks/Menu/useMyPlan";
import * as React from "react";

const MyPlansContext = React.createContext<any>([]);

export const MyPlansProvider = ({
  children,
}: {
  children: React.ReactChild;
}) => {
  const [...values] = useMyPlans();

  return (
    <MyPlansContext.Provider value={[...values]}>
      {children}
    </MyPlansContext.Provider>
  );
};

const useMyPlanProvider = () => {
  return React.useContext(MyPlansContext);
};

export default useMyPlanProvider;
