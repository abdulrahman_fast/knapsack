import useMyApprovedRequest from "@hooks/Menu/useMyApprovedRequest";
import * as React from "react";

const MyApprovedRequestContext = React.createContext<any>([]);

export const MyApprovedRequestProvider = ({
  children,
}: {
  children: React.ReactChild;
}) => {
  const [...values] = useMyApprovedRequest();

  return (
    <MyApprovedRequestContext.Provider value={[...values]}>
      {children}
    </MyApprovedRequestContext.Provider>
  );
};

const useMyApprovedRequestProvider = () => {
  return React.useContext(MyApprovedRequestContext);
};

export default useMyApprovedRequestProvider;
