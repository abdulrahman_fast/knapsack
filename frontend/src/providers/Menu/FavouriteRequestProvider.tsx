import useMyFavouriteOrder from "@hooks/Menu/useMyFavouriteOrders";
import useMyFavouritePlans from "@hooks/Menu/useMyFavouritePlans";
import * as React from "react";

const MyFavouriteRequestContext = React.createContext<any>([]);

export const MyFavouriteRequestProvider = ({
  children,
}: {
  children: React.ReactChild;
}) => {
  const [...OrderValues] = useMyFavouriteOrder();
  const [...PlanValues] = useMyFavouritePlans();

  return (
    <MyFavouriteRequestContext.Provider value={[...OrderValues, ...PlanValues]}>
      {children}
    </MyFavouriteRequestContext.Provider>
  );
};

const useMyFavouriteRequestProvider = () => {
  return React.useContext(MyFavouriteRequestContext);
};

export default useMyFavouriteRequestProvider;
