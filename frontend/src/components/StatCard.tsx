import * as React from "react";
import { StyleSheet, TouchableOpacity } from "react-native";

import { Theme } from "@constant/Base";
import {
  Box,
  MaterialIcon,
  Text,
  Svg,
  makeStyles,
  useAppTheme,
} from "components";

interface StatCardProps {
  label: string;
  count: number | string;
  type: "orderer" | "traveller";
  btnAction: () => void;
}

const useStyles = makeStyles((theme: Theme) =>
  StyleSheet.create({
    card: {
      display: "flex",
      position: "relative",
      shadowRadius: 4,
      shadowOpacity: 0.1,
      shadowColor: theme.colors.shadow,
      shadowOffset: { width: 4, height: 4 },
    },
    btn: {
      display: "flex",
      position: "absolute",
      flexDirection: "row",
      alignItems: "center",
      left: theme.spacing.m,
      bottom: theme.spacing.m,
    },
  }),
);

const StatCard: React.FC<StatCardProps> = ({
  count = "361",
  type = "traveller",
  label = "Travellers with space",
  btnAction,
}) => {
  const styles = useStyles();
  const theme = useAppTheme();
  return (
    <Box
      flex={0.47}
      height={200}
      padding={"m"}
      style={styles.card}
      position={"relative"}
      borderRadius={theme.spacing.s}
      backgroundColor={"mainBackground"}>
      <Text variant={"xxlargerPrimaryBold"} color={"buttonGreen"}>
        {count}
      </Text>
      <Text
        color={"textSecondary"}
        textTransform={"uppercase"}
        variant={"smallPrimaryLight"}>
        {label}
      </Text>

      <Box
        position={"absolute"}
        width={100}
        height={100}
        right={-30}
        bottom={20}>
        <Svg name={type === "traveller" ? "plane" : "order"} />
      </Box>

      <TouchableOpacity style={styles.btn} onPress={btnAction}>
        <Text variant={"xsmallPrimary"} color={"textBlack"}>
          EXPLORE
        </Text>
        <MaterialIcon
          size={theme.spacing.l}
          name={"keyboard-arrow-right"}
          color={theme.colors.textPrimary}
        />
      </TouchableOpacity>
    </Box>
  );
};

export default StatCard;
