import * as React from "react";

import { Box, Text, GradientBox, useAppTheme } from "components";

const AsyncReadMore = React.lazy(
  () =>
    // @ts-ignore
    import(
      "react-native-read-more-text" /* webpackPreload: true, webpackChunkName: "read-more" */
    ),
);

interface OrderDetailsProps {
  type: "traveller" | "orderer";
  orderDetails: {
    price?: string | number;
    nameORDate?: string;
    sapceORWeight?: string;
    note?: string;
  };
}

const travellerLabels = {
  nameORDateLabel: "Departure Date",
  sapceORWeightLabel: "Space",
};
const ordererLabels = {
  nameORDateLabel: "Item Name",
  sapceORWeightLabel: "Weight",
};

const OrderDetails: React.FC<OrderDetailsProps> = ({
  type,
  orderDetails: { price = 1500, nameORDate, sapceORWeight, note },
}) => {
  const theme = useAppTheme();
  const labels = type === "traveller" ? travellerLabels : ordererLabels;

  const _renderTruncatedFooter = (handlePress: any) => {
    return (
      <Text
        textAlign={"center"}
        marginTop={"xxs"}
        style={{ color: theme.colors.showMoreBtnColor }}
        onPress={handlePress}>
        Show more
      </Text>
    );
  };

  const _renderRevealedFooter = (handlePress: any) => {
    return (
      <Text
        textAlign={"center"}
        marginTop={"xxs"}
        style={{ color: theme.colors.showMoreBtnColor }}
        onPress={handlePress}>
        Show less
      </Text>
    );
  };

  return (
    <>
      <Box
        margin={"m"}
        marginTop={"l"}
        flexDirection={"row"}
        alignItems={"flex-start"}
        justifyContent={"space-between"}>
        <Box
          flex={1}
          height={theme.spacing.xxl}
          backgroundColor={"mainBackground"}
          justifyContent={"space-between"}>
          <Text color={"textSecondary"} variant={"xxsmallPrimaryMedium"}>
            {labels.nameORDateLabel || ""}
          </Text>
          <Text color={"textBlack"} variant={"xsmallPrimaryMedium"}>
            {nameORDate || "01 Oct 2020"}
          </Text>
        </Box>
        <Box
          flex={1}
          height={theme.spacing.xxl}
          justifyContent={"space-between"}
          alignItems={type === "traveller" ? "center" : "flex-end"}>
          <Text color={"textSecondary"} variant={"xxsmallPrimaryMedium"}>
            {labels.sapceORWeightLabel || ""}
          </Text>
          <Text color={"textBlack"} variant={"xsmallPrimaryMedium"}>
            {sapceORWeight || "30KG"}
          </Text>
        </Box>
        {type === "traveller" && price ? (
          <Box flex={1} alignItems={"flex-end"} height={theme.spacing.xxl}>
            <GradientBox height={theme.spacing.xxl} opacity={"99"}>
              <>
                <Text variant={"xsmallPrimaryMedium"} color={"mainBackground"}>
                  RS {price || 1500}
                </Text>
                <Text
                  style={{ alignSelf: "flex-end" }}
                  variant={"xxsmallPrimary"}
                  color={"mainBackground"}>
                  /KG
                </Text>
              </>
            </GradientBox>
          </Box>
        ) : null}
      </Box>

      <Box flex={1} margin={"m"}>
        <Text
          color={"textSecondary"}
          variant={"xxsmallPrimaryBold"}
          style={{ marginBottom: theme.spacing.xs }}>
          Details
        </Text>
        <React.Suspense
          fallback={
            <Text
              textAlign={"center"}
              marginTop={"xxs"}
              style={{ color: theme.colors.showMoreBtnColor }}>
              Show more
            </Text>
          }>
          <AsyncReadMore
            numberOfLines={1}
            renderRevealedFooter={_renderRevealedFooter}
            renderTruncatedFooter={_renderTruncatedFooter}>
            <Text
              lineHeight={15}
              color={"textSecondary"}
              variant={"xxsmallPrimaryLight"}>
              {note}
            </Text>
          </AsyncReadMore>
        </React.Suspense>
      </Box>
    </>
  );
};

export default OrderDetails;
