import * as React from "react";
import { Image, ImageSourcePropType } from "react-native";

import { useAppTheme } from ".";
import { Box, RatingBadge } from "components";

interface AvatarProps {
  size: number;
  requireRating: boolean;
  src: ImageSourcePropType;
  type: "square" | "round";
}
const DEFAULT_SIZE = 50;
const DEFAULT_IMAGE = {
  uri: "https://jacksimonvineyards.com/wp-content/uploads/2016/05/mplh.jpg",
};

const Avatar = ({
  type = "round",
  src = DEFAULT_IMAGE,
  size = DEFAULT_SIZE,
  requireRating = false,
}: AvatarProps) => {
  const {
    spacing: { s },
  } = useAppTheme();

  const square = {
    borderRadius: s,
  };
  const round = {
    borderRadius: size / 2,
  };

  return (
    <Box position={"relative"} alignItems={"center"} justifyContent={"center"}>
      <Image
        style={[
          {
            width: size,
            height: size,
            marginVertical: s,
          },
          type === "round" ? round : square,
        ]}
        source={src}
      />
      {requireRating ? (
        <Box position={"absolute"} bottom={0}>
          <RatingBadge rating={"5.0"} />
        </Box>
      ) : null}
    </Box>
  );
};

export default Avatar;
