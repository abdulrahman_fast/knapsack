import * as React from "react";

const AsyncRBSheet = React.lazy(
  () =>
    // @ts-ignore
    import(
      "react-native-raw-bottom-sheet" /* webpackPreload: true, webpackChunkName: "sheet" */
    ),
);

const RBSheet: React.FC<any> = React.forwardRef((props, ref) => {
  return (
    <React.Suspense fallback={null}>
      <AsyncRBSheet {...props} ref={ref} />
    </React.Suspense>
  );
});

export { AsyncRBSheet };

export default RBSheet;
