import * as React from "react";

import { useAppTheme } from ".";

const AsyncLinearGradient = React.lazy(
  () =>
    // @ts-ignore
    import(
      "react-native-linear-gradient" /* webpackPreload: true, webpackChunkName: "linear-gradient" */
    ),
);

interface GradientBoxProps {
  opacity?: string;
  height: string | number;
  children: React.ReactChildren | React.ReactChild;
}

const GradientBox: React.FC<GradientBoxProps> = ({
  height,
  opacity = "",
  ...props
}) => {
  const theme = useAppTheme();
  return (
    <React.Suspense fallback={null}>
      {/* @ts-ignore */}
      <AsyncLinearGradient
        end={{ x: 1, y: 1 }}
        start={{ x: 0, y: 0 }}
        colors={[`#38B411${opacity}`, `#1FC2CE${opacity}`]}
        style={{
          alignItems: "center",
          height: height,
          borderRadius: theme.spacing.xxs,
          paddingHorizontal: theme.spacing.s,
          paddingVertical: theme.spacing.xxs,
        }}>
        {props.children}
      </AsyncLinearGradient>
    </React.Suspense>
  );
};

export default GradientBox;
