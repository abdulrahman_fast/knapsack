import * as React from "react";
import { StyleSheet } from "react-native";

import { Box, Text, Avatar, RatingBadge, useAppTheme } from "components";

interface ReviewCardProps {}

const ReviewCard: React.FC = () => {
  const theme = useAppTheme();
  return (
    <>
      <Box
        minHeight={100}
        maxHeight={100}
        flexDirection={"row"}
        borderBottomColor={"borderGrey"}
        backgroundColor={"mainBackground"}
        borderBottomWidth={StyleSheet.hairlineWidth}>
        <Box justifyContent={"flex-start"} alignItems={"flex-start"}>
          <Avatar
            size={50}
            type={"round"}
            requireRating={false}
            src={{
              uri:
                "https://jacksimonvineyards.com/wp-content/uploads/2016/05/mplh.jpg",
            }}
          />
        </Box>

        <Box flex={1} justifyContent={"center"}>
          <Box
            marginTop={"m"}
            flexDirection={"row"}
            marginHorizontal={"s"}
            justifyContent={"space-between"}>
            <Text
              numberOfLines={1}
              color={"textBlack"}
              variant={"xsmallPrimaryBold"}>
              Rukhsar
            </Text>
            <RatingBadge rating={"5.0"} />
          </Box>

          <Box
            flex={1}
            marginHorizontal={"s"}
            height={theme.spacing.xxl}
            justifyContent={"flex-start"}>
            <Text
              lineHeight={15}
              numberOfLines={3}
              color={"textSecondary"}
              variant={"xxsmallPrimary"}>
              Lorem Ipsum is simply dummy text of the printing and typesetting
              industry. Lorem Ipsum is simply dummy text of the printing and
              typesetting industry. Lorem Ipsum is simply dummy text of the
              printing and typesetting industry.
            </Text>
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default ReviewCard;
