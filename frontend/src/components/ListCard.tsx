import * as React from "react";
import { format } from "date-fns";
import { useNavigation } from "@react-navigation/native";
import { StyleSheet, TouchableOpacity } from "react-native";

import {
  Box,
  Text,
  FromTo,
  Avatar,
  makeStyles,
  MaterialIcon,
} from "components";
import { Theme } from "@constant/Base";
import { useAppTheme } from "./makeStyles";
import { ItemProps } from "types";

interface ListCardProps {
  item: ItemProps;
  propagation?: "horizontal" | "vertical";
}

const TravellerLabels = {
  dateLabel: "Departure Date",
  spaceORweightLabel: "Space",
  rateORitemLabel: "Rate/KG",
};
const OrdererLabels = {
  dateLabel: "Delivery Before",
  spaceORweightLabel: "Weight",
  rateORitemLabel: "Item",
};

const useStyles = makeStyles((theme: Theme) =>
  StyleSheet.create({
    card: {
      elevation: 3,
      shadowRadius: 4,
      shadowOpacity: 0.1,
      shadowColor: theme.colors.shadow,
      shadowOffset: { width: 4, height: 4 },
    },
    btn: {
      right: 0,
      bottom: 0,
      position: "absolute",
      flexDirection: "row",
      alignItems: "center",
    },
    dashes: {
      width: 1,
      height: 70,
      marginLeft: 15,
      flexDirection: "column",
    },
  }),
);

const ListCard: React.FC<ListCardProps> = ({
  item,
  propagation = "horizontal",
}) => {
  const {
    date,
    userImage,
    type,
    id,
    rateORitem,
    toDestination,
    spaceORweight,
    fromDestination,
  } = item;
  const { navigate } = useNavigation();
  let labels = TravellerLabels;
  let routeName = "travel-request";
  if (type === "orderer") {
    labels = OrdererLabels;
    routeName = "order-request";
  }
  const styles = useStyles();
  const theme = useAppTheme();
  return (
    <Box
      style={styles.card}
      position={"relative"}
      flexDirection={"row"}
      borderColor={"borderGrey"}
      borderRadius={theme.spacing.s}
      height={theme.spacing.xxl * 4.5}
      backgroundColor={"mainBackground"}
      borderWidth={StyleSheet.hairlineWidth}
      marginRight={propagation === "horizontal" ? "s" : "xxs"}
      width={propagation === "horizontal" ? theme.spacing.xxl * 8 : "100%"}>
      <FromTo {...{ fromDestination, toDestination }} />

      <Box margin={"s"} width={"45%"}>
        <Box flexDirection={"row"} justifyContent={"space-between"}>
          <Box>
            <Text variant={"xxsmallPrimary"} color={"textSecondary"}>
              {labels["dateLabel"]}
            </Text>
            <Text
              numberOfLines={1}
              color={"textBlack"}
              variant={"xsmallPrimary"}
              style={{ marginTop: theme.spacing.xs }}>
              {format(new Date(date), "dd MMM yyyy") || ""}
            </Text>
          </Box>

          <Box>
            <Text variant={"xxsmallPrimary"} color={"textSecondary"}>
              {labels["spaceORweightLabel"]}
            </Text>
            <Text
              numberOfLines={1}
              color={"textBlack"}
              variant={"xsmallPrimary"}
              style={{ marginTop: theme.spacing.xs }}>
              {spaceORweight}
            </Text>
          </Box>
        </Box>

        <Box
          marginTop={"m"}
          flexDirection={"row"}
          alignItems={"center"}
          justifyContent={"space-between"}>
          <Box>
            <Text variant={"xxsmallPrimary"} color={"textSecondary"}>
              {labels["rateORitemLabel"]}
            </Text>
            <Text
              numberOfLines={1}
              color={"textBlack"}
              variant={"xsmallPrimary"}
              style={{ marginTop: theme.spacing.xs }}>
              {rateORitem}
            </Text>
          </Box>

          <Box>
            <Avatar
              size={50}
              type={"round"}
              requireRating={true}
              src={
                userImage
                  ? userImage
                  : {
                      uri:
                        "https://jacksimonvineyards.com/wp-content/uploads/2016/05/mplh.jpg",
                    }
              }
            />
          </Box>
        </Box>
        <TouchableOpacity
          style={styles.btn}
          onPress={() => navigate(routeName, { id })}>
          <Text variant={"xsmallPrimary"} color={"textBlack"}>
            DETAILS
          </Text>
          <MaterialIcon
            size={theme.spacing.l}
            name={"keyboard-arrow-right"}
            color={theme.colors.textPrimary}
          />
        </TouchableOpacity>
      </Box>
    </Box>
  );
};

export default ListCard;
