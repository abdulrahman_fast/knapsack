import * as React from "react";

const AsyncModal = React.lazy(
  () =>
    // @ts-ignore
    import(
      "react-native-modal" /* webpackPreload: true, webpackChunkName: "modal" */
    ),
);

const Modal: React.FC<any> = (props) => {
  return (
    <React.Suspense fallback={null}>
      <AsyncModal {...props} />
    </React.Suspense>
  );
};

export { AsyncModal };

export default Modal;
