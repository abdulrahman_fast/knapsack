import * as React from "react";

import { Box, Text, CustomIcon, useAppTheme } from "components";

const RatingBadge: React.FC<{ rating: string }> = ({ rating = "5.0" }) => {
  const theme = useAppTheme();

  return (
    <Box
      width={45}
      height={20}
      alignItems={"center"}
      flexDirection={"row"}
      justifyContent={"space-evenly"}
      borderRadius={theme.spacing.xxs}
      backgroundColor={"mainBackground"}>
      <CustomIcon
        name={"star"}
        size={theme.spacing.s}
        color={theme.colors.buttonGreen}
      />
      <Text variant={"xxsmallPrimary"}>{rating}</Text>
    </Box>
  );
};

export default RatingBadge;
