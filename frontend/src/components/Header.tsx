import React from "react";
import { SafeAreaView } from "react-native";
import { Text } from ".";

interface HeaderProps {
  title: string;
}

export default function Header({ title }: HeaderProps) {
  return (
    <SafeAreaView>
      <Text variant="smallPrimary">{title}</Text>
    </SafeAreaView>
  );
}
