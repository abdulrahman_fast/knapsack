import * as React from "react";
import { format } from "date-fns";
import { StyleSheet, TouchableOpacity } from "react-native";

import { Box, Text, Avatar, makeStyles, useAppTheme } from "components";
import { Theme } from "@constant/Base";
import { DetailsRequest } from "types";

interface RequestCardProps {
  item: DetailsRequest;
  onPress?: (item: DetailsRequest) => void;
}

const TravellerLabels = {
  placeLabel: "Delivery In",
  dateLabel: "Need Before",
  weightLabel: "Weight (Aprx)",
  detailsLabel: "Details",
};
const OrdererLabels = {
  placeLabel: "Delivery In",
  dateLabel: "Need Before",
  weightLabel: "Weight (Aprx)",
  detailsLabel: "Details",
};

const useStyles = makeStyles((theme: Theme) =>
  StyleSheet.create({
    card: {
      elevation: 3,
      shadowRadius: 4,
      shadowOpacity: 0.1,
      shadowColor: theme.colors.shadow,
      shadowOffset: { width: 4, height: 4 },
    },
    btn: {
      flexDirection: "row",
      position: "relative",
      borderRadius: theme.spacing.s,
      height: theme.spacing.xxl * 5,
      marginHorizontal: theme.spacing.m,
      borderColor: theme.colors.borderGrey,
      borderWidth: StyleSheet.hairlineWidth,
      backgroundColor: theme.colors.mainBackground,
    },
  }),
);

const RequestCard: React.FC<RequestCardProps> = ({ item, onPress }) => {
  const styles = useStyles();
  const theme = useAppTheme();
  const {
    date,
    type,
    toDestination,
    spaceORweight,
    nameORDate,
    userImage,
    note,
  } = item;
  let labels = TravellerLabels;
  let routeName = "travel-request";
  if (type === "orderer") {
    labels = OrdererLabels;
    routeName = "order-request";
  }

  return (
    <TouchableOpacity
      style={[styles.card, styles.btn]}
      onPress={() => (onPress ? onPress(item) : undefined)}>
      <Box padding={"s"} width={"100%"}>
        <Box marginBottom={"m"} marginTop={"s"}>
          <Text
            numberOfLines={1}
            color={"textBlack"}
            variant={"mediumPrimaryBold"}>
            {nameORDate}
          </Text>
        </Box>
        <Box flexDirection={"row"} justifyContent={"space-between"}>
          <Box>
            <Text variant={"xxsmallPrimary"} color={"textSecondary"}>
              {labels["placeLabel"]}
            </Text>
            <Text
              numberOfLines={1}
              color={"textBlack"}
              variant={"xsmallPrimary"}
              style={{ marginTop: theme.spacing.xs }}>
              {toDestination}
            </Text>
          </Box>

          <Box>
            <Text variant={"xxsmallPrimary"} color={"textSecondary"}>
              {labels["dateLabel"]}
            </Text>
            <Text
              numberOfLines={1}
              color={"textBlack"}
              variant={"xsmallPrimary"}
              style={{ marginTop: theme.spacing.xs }}>
              {format(new Date(date), "dd MMM yyyy") || ""}
            </Text>
          </Box>

          <Box>
            <Text variant={"xxsmallPrimary"} color={"textSecondary"}>
              {labels["weightLabel"]}
            </Text>
            <Text
              numberOfLines={1}
              color={"textBlack"}
              variant={"xsmallPrimary"}
              style={{ marginTop: theme.spacing.xs }}>
              {spaceORweight}
            </Text>
          </Box>
        </Box>

        <Box
          marginTop={"m"}
          flexDirection={"row"}
          alignItems={"flex-end"}
          justifyContent={"space-between"}>
          <Box width={"70%"}>
            <Text variant={"xxsmallPrimary"} color={"textSecondary"}>
              {labels["detailsLabel"]}
            </Text>
            <Text
              numberOfLines={2}
              color={"textBlack"}
              variant={"xsmallPrimary"}
              style={{ marginTop: theme.spacing.xs }}>
              {note}
            </Text>
          </Box>

          <Box width={"20%"}>
            <Avatar
              size={50}
              type={"round"}
              requireRating={true}
              src={
                userImage
                  ? userImage
                  : {
                      uri:
                        "https://jacksimonvineyards.com/wp-content/uploads/2016/05/mplh.jpg",
                    }
              }
            />
          </Box>
        </Box>
      </Box>
    </TouchableOpacity>
  );
};

export default RequestCard;
