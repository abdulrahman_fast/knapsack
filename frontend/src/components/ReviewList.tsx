import * as React from "react";
import { FlatList } from "react-native";

import { Box, Text, ReviewCard } from "components";

interface ReviewListProps {
  data: any;
  title: string;
}

const ReviewList: React.FC<ReviewListProps> = ({ data, title }) => {
  return (
    <Box flex={1} marginHorizontal={"m"}>
      <Box flexDirection={"row"} justifyContent={"space-between"}>
        <Text variant={"xsmallPrimaryBold"} color={"textBlack"}>
          {title}
        </Text>
      </Box>
      <Box marginVertical={"m"}>
        <FlatList
          data={data}
          showsVerticalScrollIndicator={false}
          keyExtractor={(_item, idx) => idx.toString()}
          renderItem={({ item, index }) => (
            <ReviewCard key={index.toString()} {...{ item }} />
          )}
        />
      </Box>
    </Box>
  );
};

export default ReviewList;
