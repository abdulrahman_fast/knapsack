import { useTheme } from "@shopify/restyle";
import * as React from "react";
import { FlatList, StyleSheet } from "react-native";
import { ItemProps } from "types";
import { Box, MenuHeader, ListCard } from ".";
import Loader from "./Loader";

const SearchListingResult: React.FC<{
  data: ItemProps[];
  refreshing: boolean;
  loading: boolean;
  onRefresh: () => void;
}> = ({ data, refreshing, onRefresh, loading }) => {
  const theme = useTheme();

  return (
    <Box flex={1} backgroundColor={"mainBackground"}>
      <MenuHeader />
      {loading ? (
        <Loader />
      ) : (
        <Box
          flex={3}
          borderColor={"borderGrey"}
          backgroundColor={"appGreyBg"}
          borderTopLeftRadius={theme.spacing.l}
          borderTopRightRadius={theme.spacing.l}
          borderWidth={StyleSheet.hairlineWidth}>
          <Box marginHorizontal={"m"}>
            <FlatList
              data={data}
              showsVerticalScrollIndicator={false}
              refreshing={refreshing}
              onRefresh={onRefresh}
              keyExtractor={(_item, idx) => idx.toString()}
              renderItem={({ item, index }) => {
                const first = index === 0;
                const last = data.length - 1 === index;
                return (
                  <>
                    {first ? <Box height={theme.spacing.m} /> : null}
                    <Box height={195}>
                      {/* @ts-ignore */}
                      <ListCard
                        {...{ item }}
                        key={index.toString()}
                        propagation={"vertical"}
                      />
                    </Box>
                    {/* Added to bring content above bottom bar, Can't give margin or padding */}
                    {last ? <Box height={70} /> : null}
                  </>
                );
              }}
            />
          </Box>
        </Box>
      )}
    </Box>
  );
};

export default SearchListingResult;
