import * as React from "react";
import { TouchableOpacity } from "react-native";
import { useNavigation, useRoute } from "@react-navigation/native";

import {
  Box,
  Text,
  CustomIcon,
  Avatar,
  NotificationBadge,
  useAppTheme,
} from "components";
import useAuth from "@hooks/useAuth";
import Config from "react-native-config";

const HomeHeader: React.FC<{ screenName?: boolean }> = ({
  screenName = true,
}) => {
  const { user } = useAuth();
  const route = useRoute();
  const { navigate } = useNavigation();
  const theme = useAppTheme();

  let name = route?.name || "";
  if (name?.includes("-")) {
    // @ts-ignore
    name = name?.replace(/-/g, " ");
  }

  return (
    <Box
      height={80}
      flexDirection={"row"}
      alignItems={"center"}
      marginHorizontal={"m"}
      justifyContent={"space-between"}>
      <Box
        flexDirection={"row"}
        alignItems={"center"}
        style={{
          elevation: 3,
          shadowRadius: 4,
          shadowOpacity: 0.1,
          shadowColor: theme.colors.shadow,
          shadowOffset: { width: 4, height: 4 },
        }}>
        <Avatar
          size={60}
          type={"square"}
          requireRating={true}
          src={
            user.user.avatarUrl
              ? { uri: `${Config.STATIC_URL}/${user.user.avatarUrl}` }
              : {
                  uri:
                    "https://themes.potenzaglobalsolutions.com/cardealer-wp/wp-content/uploads/2017/01/01-5-430x450.jpg",
                }
          }
        />
      </Box>

      {screenName ? (
        <Box flex={1} alignItems={"center"} justifyContent={"flex-end"}>
          <Text
            color={"textBlack"}
            variant={"mediumPrimary"}
            style={{ textTransform: "capitalize" }}>
            {name || ""}
          </Text>
        </Box>
      ) : (
        <Box
          flex={1}
          marginLeft={"s"}
          alignItems={"flex-start"}
          justifyContent={"space-between"}>
          <Text
            variant={"xxsmallPrimary"}
            color={"textSecondary"}
            marginVertical={"s"}>
            WELCOME
          </Text>
          <Text variant={"mediumPrimaryBold"} color={"textBlack"}>
            {user.user.firstName}
          </Text>
        </Box>
      )}

      <TouchableOpacity
        onPress={() => navigate("notifications")}
        style={{ width: 60, position: "relative", alignItems: "flex-end" }}>
        <CustomIcon
          color={"black"}
          name={"notfication"}
          size={theme.spacing.l + 4}
        />
        <NotificationBadge count={1} />
      </TouchableOpacity>
    </Box>
  );
};

export default HomeHeader;
