import React from 'react';
import {Image} from 'react-native';
interface LogoProps {
  size: number;
  height?: number;
  width?: number;
}
export default function Logo({size, height, width}: LogoProps) {
  return (
    <Image
      style={{
        resizeMode: 'contain',
        width: width ? width : size,
        height: height ? height : size,
      }}
      source={require('@assets/images/Logo.png')}
    />
  );
}
