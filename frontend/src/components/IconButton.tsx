import * as React from "react";
import { TouchableOpacity } from "react-native";
import { Icon, CustomIcon } from "../components";

interface IconButtonProps {
  icon: string;
  color: string;
  size: number;
  custom?: boolean;
  action: () => void;
}

const IconButton: React.FC<IconButtonProps> = ({
  icon,
  color,
  size,
  custom = false,
  action,
  ...props
}) => {
  return (
    <TouchableOpacity {...props} onPress={action}>
      {custom ? (
        <CustomIcon name={icon} size={size} color={color} />
      ) : (
        <Icon name={icon} size={size} color={color} />
      )}
    </TouchableOpacity>
  );
};

export default IconButton;
