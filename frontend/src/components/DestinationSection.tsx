import React from "react";
import { StyleSheet } from "react-native";

import { Theme } from "@constant/Base";
import { Box, Text, CustomIcon, makeStyles, useAppTheme } from "components";

interface DestinationSectionProps {
  type: "from" | "to";
  location: string;
  secondaryUsage?: boolean;
}

const useStyles = makeStyles((theme: Theme) =>
  StyleSheet.create({
    placesIcon: {
      width: 30,
      height: 30,
      borderRadius: 2,
      alignItems: "center",
      justifyContent: "center",
      backgroundColor: theme.colors.placeIconBg,
    },
  }),
);

const DestinationSection: React.FC<DestinationSectionProps> = ({
  type,
  location,
  secondaryUsage,
}) => {
  const styles = useStyles();
  const theme = useAppTheme();
  return (
    <Box
      flex={1}
      flexDirection={"row"}
      alignItems={type === "from" ? "flex-start" : "flex-end"}>
      <Box style={styles.placesIcon}>
        <CustomIcon
          name={type}
          size={theme.spacing.m}
          color={theme.colors.buttonGreen}
        />
      </Box>
      <Box marginHorizontal={"xxs"}>
        <Text
          variant={"xxsmallPrimary"}
          style={{ textTransform: "uppercase" }}
          color={secondaryUsage ? "textPrimary" : "textSecondary"}>
          {type}
        </Text>
        <Text
          numberOfLines={1}
          variant={"smallPrimary"}
          color={secondaryUsage ? "textBlack" : "buttonGreen"}>
          {location}
        </Text>
      </Box>
    </Box>
  );
};

export default DestinationSection;
