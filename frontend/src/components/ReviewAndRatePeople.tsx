import * as React from "react";
import {
  StyleSheet,
  ImageSourcePropType,
  TouchableWithoutFeedback,
} from "react-native";

import {
  Box,
  Text,
  Avatar,
  CustomIcon,
  useAppTheme,
  RatingComponent,
} from "components";

interface ReviewAndRatePeopleProps {
  name?: string;
  rating?: string;
  image?: ImageSourcePropType;
  enableRateUser?: boolean;
}

const ReviewAndRatePeople: React.FC<ReviewAndRatePeopleProps> = ({
  name,
  image,
  rating,
  enableRateUser = false,
}) => {
  const childRef = React.useRef();
  const theme = useAppTheme();

  const closeRatingComponent = () => {
    // @ts-ignore
    if (childRef.current.expanded) {
      // @ts-ignore
      childRef.current.closeRatingComponent();
    }
  };

  return (
    <TouchableWithoutFeedback style={{ position: "relative"}} onPress={closeRatingComponent}>
      <Box
        marginVertical={"xxs"}
        flexDirection={"row"}
        paddingHorizontal={"m"}
        borderTopColor={"borderGrey"}
        backgroundColor={"placeIconBg"}
        height={theme.spacing.xxl * 1.8}
        borderBottomColor={"borderGrey"}
        borderTopWidth={StyleSheet.hairlineWidth}
        borderBottomWidth={StyleSheet.hairlineWidth}>
        <Avatar
          size={50}
          type={"round"}
          requireRating={false}
          src={
            image
              ? image
              : {
                  uri:
                    "https://jacksimonvineyards.com/wp-content/uploads/2016/05/mplh.jpg",
                }
          }
        />
        <Box
          flex={1}
          marginLeft={"m"}
          flexDirection={"row"}
          alignItems={"center"}
          justifyContent={"space-between"}>
          <Box>
            <Text variant={"smallPrimary"} color={"textBlack"}>
              {name}
            </Text>
            <Box marginTop={"xxs"} flexDirection={"row"} alignItems={"center"}>
              <CustomIcon
                name="star"
                size={theme.spacing.m}
                color={theme.colors.buttonGreen}
                style={{ marginRight: theme.spacing.xxs }}
              />
              <Text variant={"xsmallPrimary"} color={"textSecondary"}>
                {rating || "4.4/5 (15 Reviews)"}
              </Text>
            </Box>
          </Box>

          {enableRateUser ? <RatingComponent ref={childRef} /> : null}
        </Box>
      </Box>
    </TouchableWithoutFeedback>
  );
};

export default ReviewAndRatePeople;
