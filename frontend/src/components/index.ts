import { createBox, createText } from "@shopify/restyle";
import { Theme } from "@constant/Base";
import { createIconSetFromIcoMoon } from "react-native-vector-icons";
import { makeStyles, useAppTheme } from "./makeStyles";

export { default as Header } from "./Header";
export { default as Button } from "./Button";
export { default as TextInput } from "./TextInput";
export { default as Logo } from "./Logo";
export { default as Avatar } from "./Avatar";
export { default as DashedCircle } from "./DashedCircle";
export { default as Heading } from "./Heading";
export { default as BottomButtonBar } from "./BottomButtonBar";
export { default as IconButton } from "./IconButton";
export { default as BottomTabs } from "./BottomTabs";
export { default as HomeHeader } from "./HomeHeader";
export { default as RatingBadge } from "./RatingBadge";
export { default as StatCard } from "./StatCard";
export { default as ListCard } from "./ListCard";
export { default as HomeList } from "./HomeList";
export { default as Svg } from "./Svg";
export { default as SearchTab } from "./SearchTab";
export { default as Dropdown } from "./Dropdown";
export { default as DatePicker } from "./DatePicker";
export { default as DestinationSection } from "./DestinationSection";
export { default as FromTo } from "./FromTo";
export { default as RequestsHeader } from "./RequestsHeader";
export { default as ReviewCard } from "./ReviewCard";
export { default as GradientBox } from "./GradientBox";
export { default as ReviewList } from "./ReviewList";
export { default as OrderDetails } from "./OrderDetails";
export { default as NotificationBadge } from "./NotificationBadge";
export { default as RequestCard } from "./RequestCard";
export { default as RequestList } from "./RequestList";
export { default as RequestDetailsSheet } from "./RequestDetailsSheet";
export { default as MenuHeader } from "./MenuHeader";
export { default as EmptyAreaLabel } from "./EmptyAreaLabel";
export { default as NotificationCard } from "./NotificationCard";
export { default as ReviewAndRatePeople } from "./ReviewAndRatePeople";
export { default as RatingUsersSheet } from "./RatingUsersSheet";
export { default as RatingComponent } from "./RatingComponent";
export { default as RateBackModel } from "./RateBackModel";

import Icon from "react-native-vector-icons/FontAwesome5";
import MaterialIcon from "react-native-vector-icons/MaterialIcons";

import icoMoonConfig from "../assets/customIcons/selection.json";
const CustomIcon = createIconSetFromIcoMoon(
  icoMoonConfig,
  "icomoon",
  "icomoon.ttf",
);

export const Box = createBox<Theme>();
export const Text = createText<Theme>();

export { Icon, MaterialIcon, CustomIcon, makeStyles, useAppTheme };
