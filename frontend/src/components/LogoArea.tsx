import * as React from "react";
import { StyleSheet } from "react-native";
import { useNavigation } from "@react-navigation/native";

import { Box, Logo, IconButton, makeStyles, useAppTheme } from "components";
import { Theme } from "@constant/Base";

const useStyles = makeStyles((theme: Theme) =>
  StyleSheet.create({
    backBtn: {
      alignItems: "center",
      justifyContent: "center",
      width: theme.spacing.xxl,
      height: theme.spacing.xxl,
      borderRadius: theme.spacing.xxl / 2,
      backgroundColor: theme.colors.placeIconBg,
    },
  }),
);

const LogoArea: React.FC<{ backBtn: boolean }> = ({ backBtn }) => {
  const { goBack } = useNavigation();
  const styles = useStyles();
  const theme = useAppTheme();
  return (
    <Box flex={1} flexDirection={"row"} alignItems="center">
      <Logo {...{ size: 70 }} />
      {backBtn ? (
        <Box flex={1} alignItems={"flex-end"} justifyContent={"center"}>
          <IconButton
            icon={"back"}
            custom={true}
            // @ts-ignore
            style={styles.backBtn}
            size={theme.spacing.m}
            action={() => goBack()}
            color={theme.colors.black}
          />
        </Box>
      ) : null}
    </Box>
  );
};

export default LogoArea;
