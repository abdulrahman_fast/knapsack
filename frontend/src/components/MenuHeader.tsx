import * as React from "react";
import { TouchableOpacity } from "react-native";
import { useNavigation, useRoute } from "@react-navigation/native";

import { Box, CustomIcon, Text, useAppTheme } from "components";

const MenuHeader: React.FC<{ screenName?: string }> = ({
  screenName = "",
}: {
  screenName?: string;
}) => {
  const { goBack } = useNavigation();
  const route = useRoute();
  const theme = useAppTheme();

  let name = route?.name || screenName;
  if (name?.includes("-")) {
    // @ts-ignore
    name = name?.replace(/-/g, " ");
  }

  return (
    <Box
      height={80}
      flexDirection={"row"}
      alignItems={"center"}
      marginHorizontal={"m"}
      justifyContent={"space-between"}>
      <TouchableOpacity style={{ width: "10%" }} onPress={goBack}>
        <CustomIcon
          name="back"
          size={theme.spacing.gm}
          color={theme.colors.black}
        />
      </TouchableOpacity>
      <Text
        color={"textBlack"}
        variant={"mediumPrimary"}
        style={{ textTransform: "capitalize" }}>
        {name || ""}
      </Text>
      <Box width={"10%"} />
    </Box>
  );
};

export default MenuHeader;
