import * as React from "react";
import { FlatList } from "react-native";

import RBSheet from "@components/AsyncRBSheet";
import { Box, RequestCard, RequestDetailsSheet, useAppTheme } from "components";
import { DetailsRequest } from "types";
import ContactDetailsSheet from "@features/Home/Home/components/ContactDetailsSheet";

interface HomeListProps {
  data: DetailsRequest[];
  removeFromRequest?: (item: any) => void;
  secondaryUsage?: boolean;
}

const ApprovedList: React.FC<HomeListProps> = ({ data, removeFromRequest }) => {
  const theme = useAppTheme();
  const sheetRef = React.useRef(null);
  const [active, setActive] = React.useState<any>({});

  const showRequestDetails = (item: any) => {
    setActive(item);
    console.log({ item });
    // @ts-ignore
    sheetRef?.current?.open();
  };

  return (
    <>
      <Box flex={1}>
        <FlatList
          data={data}
          showsVerticalScrollIndicator={false}
          keyExtractor={(_item, idx) => idx.toString()}
          renderItem={({ item, index }) => {
            return (
              <>
                {index === 0 ? <Box height={20} /> : null}
                <Box
                  marginLeft={"xxs"}
                  marginRight={"xxs"}
                  height={theme.spacing.xxl * 5.5}>
                  <RequestCard
                    key={index.toString()}
                    {...{ item, onPress: showRequestDetails, index }}
                  />
                </Box>
                {data.length - 1 === index ? <Box height={20} /> : null}
              </>
            );
          }}
        />
      </Box>

      <RBSheet
        ref={sheetRef}
        closeOnDragDown
        dragFromTopOnly
        closeOnPressBack
        openDuration={250}
        height={theme.spacing.xxl * 15}
        customStyles={{
          container: { borderRadius: theme.spacing.l },
          draggableIcon: { backgroundColor: theme.colors.textBlack },
        }}>
        <ContactDetailsSheet
          dopdownSheetlabel={"Contact Details"}
          {...{ phone: active.mobile, mail: active.email }}
        />
      </RBSheet>
    </>
  );
};

export default ApprovedList;
