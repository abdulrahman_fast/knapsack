import * as React from "react";
import {
  Platform,
  StyleSheet,
  ImageBackground,
  TouchableOpacity,
  ImageSourcePropType,
} from "react-native";

import {
  Box,
  FromTo,
  Avatar,
  CustomIcon,
  makeStyles,
  useAppTheme,
} from "components";
import { Theme } from "@constant/Base";

interface RequestsHeaderProps {
  type: "traveller" | "orderer";
  fromDestination?: string;
  toDestination?: string;
  id?: string;
  isFav?: boolean;
  markRequestAsFavourite?: (id: string, onFav?: () => void) => void;
  userImage?: ImageSourcePropType;
  myOrder?: boolean;
  onFav: () => void;
}

const useStyles = makeStyles((theme: Theme) =>
  StyleSheet.create({
    image: {
      flex: 1,
      resizeMode: "contain",
      height: theme.spacing.xxl * 7.5,
    },
    btn: {
      opacity: 0.8,
      alignItems: "center",
      justifyContent: "center",
      width: theme.spacing.xl,
      height: theme.spacing.xl,
      borderRadius: theme.spacing.xxs,
      backgroundColor: theme.colors.textPrimary,
    },
  }),
);

const RequestsHeader: React.FC<RequestsHeaderProps> = ({
  type = "traveller",
  fromDestination,
  toDestination,
  userImage = {
    uri: "https://jacksimonvineyards.com/wp-content/uploads/2016/05/mplh.jpg",
  },
  id,
  markRequestAsFavourite,
  isFav = false,
  myOrder = false,
  onFav,
}) => {
  const styles = useStyles();
  const theme = useAppTheme();
  const image =
    type === "traveller"
      ? require("@assets/images/travel-bg.png")
      : require("@assets/images/order-bg.png");

  return (
    <ImageBackground style={styles.image} source={image}>
      <Box
        flexDirection={"row"}
        paddingVertical={"l"}
        paddingHorizontal={"s"}
        justifyContent={"space-between"}
        height={theme.spacing.xxl * 6.25}
        marginTop={Platform.OS === "ios" ? "m" : "xxs"}>
        <FromTo
          {...{
            fromDestination: fromDestination ? fromDestination : "",
            toDestination: toDestination ? toDestination : "",
            secondaryUsage: true,
          }}
        />

        <Box
          right={0}
          marginBottom={"s"}
          alignItems={"center"}
          justifyContent={"space-between"}>
          <Avatar
            size={50}
            type={"round"}
            requireRating={true}
            src={userImage}
          />
          <TouchableOpacity
            style={styles.btn}
            onPress={() => {
              if (!myOrder) {
                if (markRequestAsFavourite && id) {
                  markRequestAsFavourite(id, onFav);
                }
              }
            }}>
            <CustomIcon
              name={myOrder ? "edit" : isFav ? "bookmark-selected" : "bookmark"}
              size={theme.spacing.m}
              color={
                myOrder
                  ? theme.colors.mainBackground
                  : theme.colors.bookmarkGolden
              }
            />
          </TouchableOpacity>
        </Box>
      </Box>
    </ImageBackground>
  );
};

export default RequestsHeader;
