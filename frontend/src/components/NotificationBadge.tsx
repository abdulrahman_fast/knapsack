import * as React from "react";

import { Box, Text, useAppTheme } from "components";

interface NotificationBadgeProps {
  active?: boolean;
  count: string | number;
  position?: "relative" | "absolute";
  size?: { height: number; width: number };
}

const NotificationBadge: React.FC<NotificationBadgeProps> = ({
  size,
  count,
  active = false,
  position = "absolute",
}) => {
  const theme = useAppTheme();
  const plus = count > 9 ? "+" : "";
  const value = count > 9 ? "9" : count;
  return count > 0 ? (
    <Box
      position={position}
      alignItems={"center"}
      width={size?.width || 14}
      justifyContent={"center"}
      height={size?.height || 14}
      borderRadius={theme.spacing.xxs}
      backgroundColor={active ? "mainBackground" : "buttonGreen"}>
      <Text
        variant={"smallWhiteMedium"}
        style={{ fontSize: theme.spacing.lm }}
        color={active ? "buttonGreen" : "mainBackground"}>
        {value}
        {plus}
      </Text>
    </Box>
  ) : null;
};

export default NotificationBadge;
