import * as React from "react";
import { Box, Text } from "components";

interface EmptyAreaLabelProps {
  label?: string;
}

const EmptyAreaLabel: React.FC<EmptyAreaLabelProps> = ({ label }) => {
  return (
    <Box justifyContent="center" alignItems="center" margin="m">
      <Text
        textAlign="center"
        color="textSecondary"
        variant="xsmallPrimaryBold">
        {label || "Sorry, nothing to display!"}
      </Text>
    </Box>
  );
};

export default EmptyAreaLabel;
