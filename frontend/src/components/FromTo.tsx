import * as React from "react";
import { StyleSheet } from "react-native";

import { Theme } from "@constant/Base";
import { Box, DestinationSection, makeStyles, useAppTheme } from "components";
import { useTheme } from "@shopify/restyle";

const AsyncDash = React.lazy(
  () =>
    // @ts-ignore
    import(
      "react-native-dash" /* webpackPreload: true, webpackChunkName: "dash" */
    ),
);

interface FromToProps {
  fromDestination: string;
  toDestination: string;
  secondaryUsage?: boolean;
}

const useStyles = makeStyles((theme: Theme) =>
  StyleSheet.create({
    dashes: {
      width: 1,
      flexDirection: "column",
      marginLeft: theme.spacing.m,
      height: theme.spacing.xxl * 2,
    },
  }),
);

const FromTo: React.FC<FromToProps> = ({
  secondaryUsage,
  toDestination,
  fromDestination,
}) => {
  const styles = useStyles();
  const theme = useAppTheme();
  return (
    <Box
      margin={"s"}
      width={"40%"}
      borderRightColor={"iconPrimary"}
      borderRightWidth={secondaryUsage ? 0 : StyleSheet.hairlineWidth}>
      <DestinationSection
        type={"from"}
        location={fromDestination}
        secondaryUsage={secondaryUsage}
      />
      <React.Suspense fallback={null}>
        <AsyncDash
          dashGap={7}
          dashLength={4}
          dashThickness={1}
          style={styles.dashes}
          dashColor={theme.colors.iconPrimary}
        />
      </React.Suspense>

      <DestinationSection
        type={"to"}
        location={toDestination}
        secondaryUsage={secondaryUsage}
      />
    </Box>
  );
};

export default FromTo;
