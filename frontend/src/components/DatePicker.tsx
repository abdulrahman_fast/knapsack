import * as React from "react";
import { StyleSheet } from "react-native";

import { Box, Text, MaterialIcon, makeStyles, useAppTheme } from "components";
import { Theme } from "@constant/Base";

const AsyncDatePicker = React.lazy(
  () =>
    // @ts-ignore
    import(
      "react-native-datepicker" /* webpackPreload: true, webpackChunkName: "date-picker" */
    ),
);

interface DatePickerProps {
  placeholder: string;
  date: number | string | Date | undefined;
  minDate?: number | string | Date;
  maxDate?: number | string | Date;
  onDateChange: (date: string | Date) => void;
}

const Size = 50;

const useStyles = makeStyles((theme: Theme) =>
  StyleSheet.create({
    dateText: { marginLeft: 40 },
    dateTouchBody: {
      height: Size,
      backgroundColor: theme.colors.mainBackground,
    },
    dateIcon: {
      height: Size,
      borderLeftWidth: 0,
      alignItems: "center",
      justifyContent: "center",
      borderRadius: theme.spacing.xxs,
      borderColor: theme.colors.borderGrey,
      backgroundColor: theme.colors.mainBackground,
      borderTopWidth: StyleSheet.hairlineWidth,
      borderRightWidth: StyleSheet.hairlineWidth,
      borderBottomWidth: StyleSheet.hairlineWidth,
    },
    dateInput: {
      height: Size,
      borderRightWidth: 0,
      borderRadius: theme.spacing.xxs,
      borderTopColor: theme.colors.borderGrey,
      borderLeftColor: theme.colors.borderGrey,
      borderBottomColor: theme.colors.borderGrey,
      borderRightColor: theme.colors.mainBackground,
      borderTopWidth: StyleSheet.hairlineWidth,
      borderLeftWidth: StyleSheet.hairlineWidth,
      borderBottomWidth: StyleSheet.hairlineWidth,
    },
  }),
);

const DatePickerComponent: React.FC<DatePickerProps> = ({
  placeholder,
  minDate = "01 01 1970",
  maxDate = "01 01 2099",
  onDateChange,
  date,
}) => {
  const styles = useStyles();
  const theme = useAppTheme();
  return (
    <Box marginHorizontal={"m"} position={"relative"} marginBottom={"s"}>
      <Box
        zIndex={2}
        height={50}
        padding={"m"}
        alignItems={"center"}
        position={"absolute"}
        justifyContent={"center"}>
        <Text variant={"xsmallPrimary"} color={"textPrimary"}>
          {placeholder}
        </Text>
      </Box>
      <React.Suspense fallback={null}>
        <AsyncDatePicker
          mode="date"
          is24Hour={false}
          date={date}
          minDate={minDate}
          maxDate={maxDate}
          format="DD MMM YYYY"
          style={{ width: "100%" }}
          cancelBtnText="Cancel"
          confirmBtnText="Confirm"
          customStyles={{
            dateText: styles.dateText,
            dateInput: styles.dateInput,
            dateTouchBody: styles.dateTouchBody,
          }}
          iconComponent={
            <Box flex={0.2} style={styles.dateIcon}>
              <MaterialIcon
                name={"date-range"}
                size={theme.spacing.l}
                color={theme.colors.iconPrimary}
              />
            </Box>
          }
          onDateChange={(date: string) => onDateChange(date)}
        />
      </React.Suspense>
    </Box>
  );
};

export default DatePickerComponent;
