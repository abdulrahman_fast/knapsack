import * as React from "react";
import { StyleSheet } from "react-native";

import { Theme } from "@constant/Base";
import { Box, Text, makeStyles } from "components";

interface NotificationCardProps {
  title: string;
  details: string;
  time?: string | number;
}

const useStyles = makeStyles((theme: Theme) =>
  StyleSheet.create({
    cardEffect: {
      elevation: 3,
      shadowRadius: 4,
      shadowOpacity: 0.1,
      shadowColor: theme.colors.shadow,
      shadowOffset: { width: 4, height: 4 },
    },
    card: {
      borderRadius: theme.spacing.xxs,
      minHeight: theme.spacing.xxl * 2,
      marginHorizontal: theme.spacing.m,
      borderColor: theme.colors.borderGrey,
      borderWidth: StyleSheet.hairlineWidth,
      backgroundColor: theme.colors.mainBackground,
    },
  }),
);

const NotificationCard: React.FC<NotificationCardProps> = ({
  title,
  details,
  time,
}) => {
  const styles = useStyles();

  return (
    <Box
      margin={"s"}
      padding={"m"}
      flexDirection={"column"}
      style={[styles.card, styles.cardEffect]}>
      <Box
        flexDirection={"row"}
        alignItems={"center"}
        justifyContent={"space-between"}>
        <Text variant={"smallPrimary"} color={"textPrimary"}>
          {title}
        </Text>
        <Text variant={"xxsmallPrimaryLight"} color={"textSecondary"}>
          {time}
        </Text>
      </Box>
      <Box marginTop={"s"}>
        <Text
          numberOfLines={3}
          color={"textSecondary"}
          variant={"xsmallPrimaryLight"}>
          {details}
        </Text>
      </Box>
    </Box>
  );
};

export default NotificationCard;
