import * as React from "react";
import { format } from "date-fns";
import { StyleSheet, ScrollView, ImageSourcePropType } from "react-native";

import { Theme } from "@constant/Base";
import {
  Box,
  Text,
  Button,
  makeStyles,
  useAppTheme,
  ReviewAndRatePeople,
} from "components";
import useRequest from "@hooks/useRequest";
import useAuth from "@hooks/useAuth";
import Endpoints from "@constant/Endpoint";
import { DetailsRequest } from "types";

interface RequestDetailsSheetProps {
  item: DetailsRequest;
  closeModal: () => void;
}

const TravellerLabels = {
  placeLabel: "Delivery In",
  dateLabel: "Need Before",
  weightLabel: "Weight (Aprx)",
  detailsLabel: "Details",
};
const OrdererLabels = {
  placeLabel: "Delivery In",
  dateLabel: "Need Before",
  weightLabel: "Weight (Aprx)",
  detailsLabel: "Details",
};

const useStyles = makeStyles((theme: Theme) =>
  StyleSheet.create({
    container: {
      position: "relative",
      marginVertical: theme.spacing.m,
      borderRadius: theme.spacing.s,
    },
    left: {
      textAlign: "left",
      marginTop: theme.spacing.xs,
    },
    mid: {
      textAlign: "justify",
      marginTop: theme.spacing.xs,
    },
    right: {
      textAlign: "right",
      marginTop: theme.spacing.xs,
    },
  }),
);

const RequestDetailsSheet: React.FC<RequestDetailsSheetProps> = ({
  item,
  closeModal,
}) => {
  const styles = useStyles();
  const theme = useAppTheme();
  const { user } = useAuth();
  const request = useRequest();
  const {
    id,
    date,
    type,
    nameORDate,
    note,
    toDestination,
    spaceORweight,
  } = item;

  let labels = TravellerLabels;
  let routeName = "travel-request";
  if (type === "orderer") {
    labels = OrdererLabels;
    routeName = "order-request";
  }

  const MarkRequestAsAcceptOrDecline = (status: boolean) => {
    console.log({ status, requestId: id });
    try {
      request(Endpoints.MarkRequest, {
        method: "POST",
        headers: { Authorization: `Bearer ${user.token}` },
        body: { status, requestId: id },
      })
        .then((res) => {
          console.log({ res });
          closeModal();
        })
        .catch((err) => {
          console.log({ err });
        });
    } catch (e) {
      console.log({ e });
    }
  };

  return (
    <Box style={styles.container}>
      <Box
        marginBottom={"gm"}
        alignItems={"center"}
        marginHorizontal={"m"}
        justifyContent={"center"}>
        <Text variant={"mediumPrimaryMedium"} color={"textBlack"}>
          {nameORDate}
        </Text>
      </Box>

      <ScrollView>
        <Box
          flexDirection={"row"}
          marginHorizontal={"m"}
          justifyContent={"space-between"}>
          <Box>
            <Text variant={"xxsmallPrimary"} color={"textSecondary"}>
              {labels["placeLabel"]}
            </Text>
            <Text
              numberOfLines={1}
              color={"textBlack"}
              style={styles.left}
              variant={"xsmallPrimary"}>
              {toDestination}
            </Text>
          </Box>

          <Box>
            <Text variant={"xxsmallPrimary"} color={"textSecondary"}>
              {labels["dateLabel"]}
            </Text>
            <Text
              numberOfLines={1}
              color={"textBlack"}
              style={styles.mid}
              variant={"xsmallPrimary"}>
              {format(new Date(date), "dd MMM yyyy") || ""}
            </Text>
          </Box>

          <Box>
            <Text variant={"xxsmallPrimary"} color={"textSecondary"}>
              {labels["weightLabel"]}
            </Text>
            <Text
              numberOfLines={1}
              color={"textBlack"}
              style={styles.right}
              variant={"xsmallPrimary"}>
              {spaceORweight}
            </Text>
          </Box>
        </Box>

        <Box marginVertical={"l"} marginHorizontal={"m"}>
          <Text variant={"xxsmallPrimary"} color={"textSecondary"}>
            {labels["detailsLabel"]}
          </Text>
          <Text
            color={"textBlack"}
            variant={"xsmallPrimary"}
            style={{ marginTop: theme.spacing.xs }}>
            {note}
          </Text>
        </Box>

        <ReviewAndRatePeople name="" rating="" enableRateUser={false} />

        <Box
          flexDirection={"row"}
          marginHorizontal={"m"}
          justifyContent={"space-between"}
          marginVertical={"l"}>
          <Button
            {...{
              size: "48%",
              btnText: "ACCEPT",
              onPress: () => MarkRequestAsAcceptOrDecline(true),
            }}
          />
          <Button
            {...{
              size: "48%",
              type: "secondary",
              btnText: "DECLINE",
              onPress: () => MarkRequestAsAcceptOrDecline(false),
            }}
          />
        </Box>
      </ScrollView>
    </Box>
  );
};

export default RequestDetailsSheet;
