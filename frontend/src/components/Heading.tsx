import * as React from "react";
import { Box, Text } from ".";

interface HeadingProps {
  heading: string;
  subHeading: string;
}

const Heading: React.FC<HeadingProps> = ({ heading, subHeading }) => {
  return (
    <Box flex={1}>
      <Text variant="xxlargerPrimaryBold" textAlign="left">
        {heading}
      </Text>
      <Text variant="smallPrimary" textAlign="left" marginTop="m">
        {subHeading}
      </Text>
    </Box>
  );
};

export default Heading;
