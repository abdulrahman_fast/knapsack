import * as React from "react";
import { Platform, StyleSheet, TouchableOpacity } from "react-native";
import { BlurView } from "@react-native-community/blur";
import { useNavigation } from "@react-navigation/native";

import { Theme } from "@constant/Base";
import {
  Box,
  Text,
  CustomIcon,
  Button,
  makeStyles,
  useAppTheme,
} from "../components";

const iOS = Platform.OS === "ios";

interface BottomButtonBarProps {
  status?: "review" | "reject" | undefined;
  buttonTitle?: string;
  loading?: boolean;
  btnAction?: () => void;
  backBtnAction?: () => void;
  style?: React.CSSProperties;
}

const STATUS_TEXTS = {
  review: {
    title: "REQUEST IN REVIEW",
    description:
      "You will be able to view contact details once the traveller approve your request.",
  },
  reject: {
    title: "REQUEST REJECTED",
    description:
      "Your request to view contact detail is declined with reason : Bag is already full.",
  },
};

const useStyles = makeStyles((theme: Theme) =>
  StyleSheet.create({
    bar: {
      bottom: 0,
      borderTopColor: theme.colors.shadow,
      borderTopWidth: StyleSheet.hairlineWidth,
      backgroundColor: iOS ? "transparent" : theme.colors.mainBackground,
    },
    backBtn: {
      shadowOpacity: 0.2,
      alignItems: "center",
      justifyContent: "center",
      marginLeft: theme.spacing.xs,
      elevation: theme.spacing.xxs,
      width: theme.spacing.xxl + 10,
      height: theme.spacing.xxl + 10,
      shadowRadius: theme.spacing.xxs,
      shadowColor: theme.colors.shadow,
      borderRadius: theme.spacing.xxs - 2,
      shadowOffset: { width: 1, height: 1 },
      backgroundColor: theme.colors.mainBackground,
    },
  }),
);

const BottomButtonBar: React.FC<BottomButtonBarProps> = ({
  status,
  buttonTitle,
  btnAction,
  loading = false,
  backBtnAction,
  ...props
}) => {
  const styles = useStyles();
  const theme = useAppTheme();
  const { goBack } = useNavigation();

  if (!backBtnAction) {
    backBtnAction = goBack;
  }

  return (
    <Box
      width={"100%"}
      overflow={"hidden"}
      shadowOpacity={0.5}
      alignItems={"center"}
      position={"absolute"}
      flexDirection={"row"}
      paddingHorizontal={"m"}
      height={iOS ? 100 : 80}
      elevation={theme.spacing.xxs}
      justifyContent={"space-between"}
      shadowRadius={theme.spacing.xxs}
      // @ts-ignore
      style={[styles.bar, props.style]}
      borderTopWidth={StyleSheet.hairlineWidth}>
      {iOS ? (
        <BlurView
          blurAmount={20}
          blurType="light"
          style={{
            borderTopLeftRadius: theme.spacing.m,
            borderTopRightRadius: theme.spacing.m,
            ...StyleSheet.absoluteFillObject,
          }}
          reducedTransparencyFallbackColor={"white"}
        />
      ) : null}
      <TouchableOpacity style={styles.backBtn} onPress={backBtnAction}>
        <CustomIcon
          name="back"
          size={theme.spacing.l}
          color={theme.colors.iconPrimary}
        />
      </TouchableOpacity>
      {status ? (
        <Box width={"80%"} height={"65%"}>
          <Text
            color={"bookmarkGolden"}
            variant={"xxsmallPrimaryMedium"}
            style={{ marginBottom: theme.spacing.xxs }}>
            {STATUS_TEXTS[status]?.title}
          </Text>
          <Text variant={"xxsmallPrimaryLight"} color={"textSecondary"}>
            {STATUS_TEXTS[status]?.description}
          </Text>
        </Box>
      ) : null}
      {buttonTitle && btnAction && status === undefined ? (
        <Button
          {...{
            size: "60%",
            onPress: btnAction,
            loading,
            btnText: buttonTitle,
          }}
        />
      ) : null}
    </Box>
  );
};

export default BottomButtonBar;
