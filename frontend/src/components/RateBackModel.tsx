import * as React from "react";
import { Text as RNText, StyleSheet } from "react-native";

import Modal from "@components/AsyncModal";
import { Theme } from "@constant/Base";
import {
  Box,
  Text,
  makeStyles,
  useAppTheme,
  ReviewAndRatePeople,
} from "components";

interface RateBackModelProps {}

const useStyles = makeStyles((theme: Theme) =>
  StyleSheet.create({
    baseText: {
      color: theme.colors.textBlack,
    },
    dynamicText: {
      fontWeight: "bold",
      color: theme.colors.textBlack,
    },
  }),
);

const RateBackModel: React.FC<RateBackModelProps> = () => {
  const theme = useAppTheme();
  const styles = useStyles();

  return (
    <Modal isVisible={false}>
      <Box
        padding={"s"}
        backgroundColor={"white"}
        borderRadius={theme.spacing.xxs}>
        <Box
          marginTop={"s"}
          marginBottom={"m"}
          flexDirection={"row"}
          justifyContent={"center"}>
          <Text variant={"mediumPrimaryBold"} color={"textBlack"}>
            Rate Traveller
          </Text>
        </Box>
        <Box
          marginVertical={"m"}
          flexDirection={"row"}
          justifyContent={"center"}>
          <RNText style={styles.baseText}>
            <RNText style={styles.dynamicText}>"Ahmed Sardar"</RNText> have
            closed the deal on{" "}
            <RNText style={styles.dynamicText}>"MacBook Pro"</RNText> brought
            from <RNText style={styles.dynamicText}>"New York"</RNText> to{" "}
            <RNText style={styles.dynamicText}>"Karachi"</RNText> and have rated
            you. Rate them back and help to build a safe community.
          </RNText>
        </Box>
        <Box marginBottom={"s"}>
          <ReviewAndRatePeople enableRateUser />
        </Box>
      </Box>
    </Modal>
  );
};

export default RateBackModel;
