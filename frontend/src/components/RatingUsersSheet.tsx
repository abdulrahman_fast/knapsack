import * as React from "react";
import { StyleSheet, FlatList } from "react-native";

import { Theme } from "@constant/Base";
import { Box, Text, makeStyles, ReviewAndRatePeople } from "components";

interface RatingUsersSheetProps {
  sheetTitle: string;
  sheetSubTitle: string;
  users?: Array<any>;
}

const useStyles = makeStyles((theme: Theme) =>
  StyleSheet.create({
    container: {
      position: "relative",
      marginVertical: theme.spacing.m,
      borderRadius: theme.spacing.s,
    },
  }),
);

const RatingUsersSheet: React.FC<RatingUsersSheetProps> = ({
  sheetTitle,
  sheetSubTitle,
  users,
}) => {
  const styles = useStyles();

  return (
    <Box style={styles.container}>
      <Box
        marginBottom={"gm"}
        alignItems={"center"}
        marginHorizontal={"m"}
        justifyContent={"center"}>
        <Text variant={"mediumPrimaryMedium"} color={"textBlack"}>
          {sheetTitle}
        </Text>
      </Box>
      <Box marginHorizontal={"m"} marginBottom={"m"}>
        <Text variant={"xsmallPrimary"} color={"textBlack"}>
          {sheetSubTitle}
        </Text>
      </Box>

      <FlatList
        data={users}
        showsVerticalScrollIndicator={false}
        keyExtractor={(_item, idx) => idx.toString()}
        renderItem={({ item, index }) => {
          return (
            <ReviewAndRatePeople
              name="Ahmed Sardar"
              rating="4.5"
              enableRateUser={true}
            />
          );
        }}
      />
    </Box>
  );
};

export default RatingUsersSheet;
