import * as React from "react";
import {
  Platform,
  UIManager,
  LayoutAnimation,
  TouchableOpacity,
} from "react-native";

import { Box, CustomIcon, IconButton, useAppTheme } from "components";

const RatingComponent: React.FC<any> = React.forwardRef((_props, ref) => {
  const [expanded, setExpanded] = React.useState(false);
  const [rating, setRating] = React.useState<Array<number>>([]);
  const theme = useAppTheme();

  React.useImperativeHandle(ref, () => ({
    expanded: expanded,
    closeRatingComponent: changeLayout,
  }));

  React.useEffect(() => {
    if (Platform.OS === "android") {
      UIManager.setLayoutAnimationEnabledExperimental(true);
    }
  }, []);

  const changeLayout = () => {
    LayoutAnimation.configureNext(LayoutAnimation.Presets.easeInEaseOut);
    setExpanded(!expanded);
  };

  const computeRating = (rate: number) => {
    const rating = [];
    for (let i = 1; i <= rate; i++) {
      rating.push(i);
    }
    setRating(rating);
  };

  return (
    <TouchableOpacity
      onPress={changeLayout}
      style={{ position: "absolute", right: 0 }}>
      <Box
        flexDirection={"row"}
        alignItems={"center"}
        height={theme.spacing.xl}
        justifyContent={"space-evenly"}
        backgroundColor={expanded ? "textWhite" : "appGreyBg"}
        style={{
          width: expanded ? theme.spacing.xxl * 4 : theme.spacing.xxl,
          overflow: "hidden",
        }}>
        {!expanded ? (
          <CustomIcon
            name="star"
            size={theme.spacing.gm}
            color={
              rating.length
                ? theme.colors.bookmarkGolden
                : theme.colors.textSecondary
            }
            style={{ marginRight: theme.spacing.xxs }}
          />
        ) : (
          <>
            {[1, 2, 3, 4, 5].map((item, idx) => {
              return (
                <IconButton
                  key={idx}
                  icon="star"
                  custom={true}
                  size={theme.spacing.gm}
                  action={() => computeRating(item)}
                  color={
                    rating.includes(item)
                      ? theme.colors.bookmarkGolden
                      : theme.colors.textSecondary
                  }
                />
              );
            })}
          </>
        )}
      </Box>
    </TouchableOpacity>
  );
});

export default RatingComponent;
