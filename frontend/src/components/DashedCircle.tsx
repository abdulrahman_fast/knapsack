import React from "react";
import { Box } from ".";
interface DashedCircleProps {
  size: number;
  top?: number;
  left?: number;
  right?: number;
}
export default function DashedCircle({
  size,
  top,
  left,
  right,
}: DashedCircleProps) {
  return (
    <Box
      position="absolute"
      width={size}
      height={size}
      borderRadius={size / 2}
      borderStyle="dashed"
      borderWidth={1}
      borderColor="borderGreen"
      {...{ top, left, right }}
    />
  );
}
