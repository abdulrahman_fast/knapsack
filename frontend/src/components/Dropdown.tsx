import * as React from "react";
import {
  FlatList,
  Dimensions,
  StyleSheet,
  TouchableOpacity,
} from "react-native";

import RBSheet from "@components/AsyncRBSheet";
import {
  Box,
  Icon,
  Text,
  TextInput,
  CustomIcon,
  useAppTheme,
} from "components";

const windowHeight = Dimensions.get("window").height;
interface DropdownProps {
  icon?: string;
  value: string;
  selectCity: any;
  dropdownLabel: string;
  dropdownData?: Array<any>;
  requireSearch?: boolean;
  dopdownSheetlabel: string;
  searchMethod?: (text: string) => void;
}

interface DropdownSheetProps {
  requireSearch: boolean;
  dropdownData?: Array<any>;
  dopdownSheetlabel: string;
  onPressItem: any;
  searchMethod?: (text: string) => void;
}

interface DropdownItemProps {
  label: string;
}

const Size = 50;

const Dropdown: React.FC<DropdownProps> = ({
  icon,
  value = "",
  dropdownLabel,
  dopdownSheetlabel,
  requireSearch = false,
  dropdownData,
  selectCity,
}) => {
  const theme = useAppTheme();
  const sheetRef = React.useRef(null);

  const onPress = () => {
    // @ts-ignore
    sheetRef?.current?.open();
  };

  const onPressItem = (item: { id: string; name: string }) => {
    selectCity(item);
    // @ts-ignore
    sheetRef?.current?.close();
  };

  return (
    <>
      <TouchableOpacity
        onPress={onPress}
        style={{
          height: Size,
          marginBottom: theme.spacing.m,
          marginHorizontal: theme.spacing.m,
          borderColor: theme.colors.borderGrey,
          borderWidth: StyleSheet.hairlineWidth,
          borderTopLeftRadius: theme.spacing.xxs,
          borderTopRightRadius: theme.spacing.xxs,
          backgroundColor: theme.colors.mainBackground,
        }}>
        <Box flex={1} marginHorizontal={"xs"}>
          <Box flex={1} flexDirection={"row"} alignItems={"center"}>
            {icon ? (
              <Box
                width={"8%"}
                alignItems={"flex-start"}
                justifyContent={"center"}>
                <CustomIcon
                  name={icon}
                  size={theme.spacing.m}
                  color={theme.colors.borderGreen}
                />
              </Box>
            ) : null}
            <Box width={icon ? "82%" : "90%"}>
              <Text
                numberOfLines={1}
                color={"textPrimary"}
                variant={"xsmallPrimary"}>
                {dropdownLabel} {value}
              </Text>
            </Box>
            <Box
              alignItems={"flex-end"}
              justifyContent={"flex-end"}
              width={"10%"}>
              <Icon
                name={"caret-down"}
                size={theme.spacing.m}
                color={theme.colors.textBlack}
              />
            </Box>
          </Box>
        </Box>
      </TouchableOpacity>

      <RBSheet
        ref={sheetRef}
        closeOnDragDown
        dragFromTopOnly
        closeOnPressBack
        openDuration={250}
        height={windowHeight - 170}
        customStyles={{
          container: { borderRadius: theme.spacing.l },
          draggableIcon: { backgroundColor: theme.colors.textBlack },
        }}>
        <DropdownSheet
          {...{ dopdownSheetlabel, requireSearch, dropdownData, onPressItem }}
        />
      </RBSheet>
    </>
  );
};

const DropdownSheet: React.FC<DropdownSheetProps> = ({
  dopdownSheetlabel,
  requireSearch,
  dropdownData,
  onPressItem,
}) => {
  const [search, setSearch] = React.useState("");
  const [values, setValues] = React.useState(dropdownData);
  React.useEffect(() => {
    if (search) {
      setValues(
        dropdownData?.filter((x) => {
          return x.name.toLowerCase().includes(search.toLowerCase());
        }),
      );
    }
  }, [search, dropdownData]);

  React.useEffect(() => {
    setValues(dropdownData);
  }, [dropdownData]);

  return (
    <Box height={"100%"} padding={"m"} backgroundColor={"mainBackground"}>
      <Box justifyContent={"center"} alignItems={"center"}>
        <Text variant={"mediumPrimaryMedium"} color={"textBlack"}>
          {dopdownSheetlabel}
        </Text>
      </Box>

      {requireSearch ? (
        <Box marginVertical={"m"}>
          <TextInput
            value={search}
            placeholder={"Search"}
            onChangeText={(text) => setSearch(text)}
          />
        </Box>
      ) : null}

      <Box
        flex={1}
        marginTop={"m"}
        justifyContent={"flex-start"}
        style={{ marginBottom: "15%" }}>
        <FlatList
          data={values}
          keyExtractor={(_item, idx) => idx.toString()}
          renderItem={({ item, index }) => (
            <DropdownItem key={index.toString()} {...{ item, onPressItem }} />
          )}
        />
      </Box>
    </Box>
  );
};

const DropdownItem: React.FC<any> = ({ item, onPressItem }) => (
  <TouchableOpacity onPress={() => onPressItem(item)}>
    <Box
      height={50}
      justifyContent={"center"}
      borderBottomColor={"borderGrey"}
      borderBottomWidth={StyleSheet.hairlineWidth}>
      <Text variant={"xsmallPrimaryBold"} color={"textPrimary"}>
        {item.name}
      </Text>
    </Box>
  </TouchableOpacity>
);

export default Dropdown;
