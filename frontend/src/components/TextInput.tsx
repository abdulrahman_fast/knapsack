import * as React from "react";
import { KeyboardTypeOptions, StyleSheet, TextInput } from "react-native";
import { Theme } from "@constant/Base";
import { makeStyles, useAppTheme } from ".";

interface TextInputProps {
  value: string | undefined;
  noBorder?: boolean;
  placeholder?: string;
  size?: string | number;
  style?: React.CSSProperties;
  secureTextEntry?: boolean;
  keyboardType?: KeyboardTypeOptions;
  editable?: boolean;
  onChangeText: (text: string) => void;
}
const height = 50;

const useStyles = makeStyles((theme: Theme) =>
  StyleSheet.create({
    input: {
      height,
      borderRadius: theme.spacing.xxs,
      borderWidth: StyleSheet.hairlineWidth,
    },
    border: {
      borderColor: theme.colors.borderGrey,
    },
    noBorder: {
      borderWidth: 0,
      borderBottomWidth: StyleSheet.hairlineWidth,
      borderBottomColor: theme.colors.textInputBorder,
    },
  }),
);

const Input: React.FC<TextInputProps> = ({
  value = "",
  size = "100%",
  noBorder = false,
  placeholder = "",
  onChangeText,
  secureTextEntry = false,
  keyboardType = "default",
  editable = true,
  ...props
}) => {
  const theme = useAppTheme();
  const styles = useStyles();
  return (
    <TextInput
      {...props}
      style={[
        styles.input,
        // @ts-ignore
        props.style,
        {
          width: size,
          padding: theme.spacing.s,
          backgroundColor: theme.colors.white,
        },
        noBorder ? styles.noBorder : styles.border,
      ]}
      secureTextEntry={secureTextEntry}
      value={value}
      editable={editable}
      keyboardType={keyboardType}
      placeholder={placeholder}
      placeholderTextColor={"grey"}
      onChangeText={(text: string) => onChangeText(text)}
    />
  );
};

export default Input;
