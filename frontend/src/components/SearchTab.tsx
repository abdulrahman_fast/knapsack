import * as React from "react";
import { StyleSheet, TouchableWithoutFeedback } from "react-native";

import { TabValues } from "types";
import { Box, Text, Svg, makeStyles, useAppTheme } from "components";
import { Theme } from "@constant/Base";

interface SearchTabProps {
  label: string;
  secondaryUsage?: boolean;
  type: "traveller" | "orderer";
  active: TabValues;
  setTabActive: (name: TabValues) => void;
}

const useStyles = makeStyles((theme: Theme) =>
  StyleSheet.create({
    card: {
      display: "flex",
      shadowOpacity: 0.1,
      position: "relative",
      shadowRadius: theme.spacing.xxs,
      elevation: theme.spacing.xxs - 1,
      shadowColor: theme.colors.shadow,
      shadowOffset: { width: 4, height: 4 },
    },
    normal: {
      bottom: 0,
      right: -30,
      zIndex: -5,
      position: "absolute",
      width: theme.spacing.xxl * 2 + 20,
      height: theme.spacing.xxl * 2 + 20,
    },
    expanded: {
      height: theme.spacing.xxl * 2 - 10,
    },
  }),
);

const SearchTab: React.FC<SearchTabProps> = ({
  type,
  active,
  label,
  secondaryUsage = false,
  setTabActive,
}) => {
  const styles = useStyles();
  const theme = useAppTheme();
  React.useEffect(() => {
    if (secondaryUsage) {
      setTabActive("traveller");
    }
  }, [secondaryUsage, setTabActive]);

  const determineTabIcon = () => {
    if (active === "traveller" && type === "traveller") {
      return "plane";
    } else if (active === "orderer" && type === "orderer") {
      return "order";
    } else {
      if (type === "traveller") {
        return "plane-disable";
      } else {
        return "order-disable";
      }
    }
  };

  return (
    <Box flex={1}>
      <TouchableWithoutFeedback
        onPress={() =>
          setTabActive(active === type && !secondaryUsage ? "" : type)
        }>
        <Box
          padding={"m"}
          marginTop={"m"}
          style={[
            styles.card,
            active && !secondaryUsage ? { elevation: 0 } : {},
          ]}
          position={"relative"}
          backgroundColor={"mainBackground"}
          height={active ? 80 : secondaryUsage ? 80 : 120}
          borderBottomColor={
            secondaryUsage && active === type ? "borderGreen" : "mainBackground"
          }
          borderBottomWidth={theme.spacing.xxs}
          borderRightWidth={StyleSheet.hairlineWidth}
          borderRightColor={
            type === "traveller" ? "borderGrey" : "mainBackground"
          }
          borderTopRightRadius={type === "orderer" ? theme.spacing.s : 0}
          borderTopLeftRadius={type === "traveller" ? theme.spacing.s : 0}
          borderBottomRightRadius={
            active !== type && type === "orderer" ? theme.spacing.s : 0
          }
          borderBottomLeftRadius={
            active !== type && type === "traveller" ? theme.spacing.s : 0
          }>
          <Text
            variant={"xsmallPrimaryBold"}
            color={active === type ? "textBlack" : "textSecondary"}>
            {label}
          </Text>
          <Box style={[styles.normal, active ? styles.expanded : {}]}>
            <Svg name={determineTabIcon()} />
          </Box>
        </Box>
      </TouchableWithoutFeedback>
    </Box>
  );
};

export default SearchTab;
