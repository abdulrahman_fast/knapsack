import * as React from "react";
import { FlatList, TouchableOpacity } from "react-native";

import { Box, Text, ListCard, useAppTheme, EmptyAreaLabel } from "components";
import { ItemProps } from "types";

interface HomeListProps {
  data: ItemProps[];
  title?: string;
  secondaryUsage?: boolean;
  viewAllAction?: () => void;
}

const HomeList: React.FC<HomeListProps> = ({
  data,
  title,
  secondaryUsage = false,
  viewAllAction,
}) => {
  const theme = useAppTheme();

  return (
    <>
      <Box
        flexDirection={"row"}
        marginHorizontal={"m"}
        justifyContent={"space-between"}>
        {title ? (
          <Text
            variant={secondaryUsage ? "xsmallPrimaryBold" : "mediumPrimaryBold"}
            color={secondaryUsage ? "textBlack" : "textPrimary"}>
            {title}
          </Text>
        ) : null}
        {viewAllAction ? (
          <TouchableOpacity onPress={viewAllAction}>
            <Text variant={"smallPrimary"} color={"textBlack"}>
              View All
            </Text>
          </TouchableOpacity>
        ) : null}
      </Box>
      <FlatList
        data={data}
        showsVerticalScrollIndicator={false}
        showsHorizontalScrollIndicator={false}
        ListEmptyComponent={<EmptyAreaLabel label={"No Travel Request!"} />}
        horizontal={secondaryUsage ? false : true}
        keyExtractor={(_item, idx) => idx.toString()}
        renderItem={({ item, index }) => {
          const isFirst = index === 0;
          const isLast = index === data.length - 1;
          return (
            <>
              <Box
                height={theme.spacing.xxl * 5}
                marginRight={secondaryUsage ? "m" : "xxs"}
                marginVertical={!secondaryUsage ? "m" : "xxs"}
                marginLeft={secondaryUsage || isFirst ? "m" : "xxs"}>
                {secondaryUsage ? <Box height={10} /> : null}
                <ListCard
                  key={index.toString()}
                  {...{
                    item,
                    propagation: secondaryUsage ? "vertical" : "horizontal",
                  }}
                />
              </Box>
              {secondaryUsage && isLast ? <Box height={20} /> : null}
            </>
          );
        }}
      />
    </>
  );
};

export default HomeList;
