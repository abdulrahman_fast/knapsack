import Endpoints from "@constant/Endpoint";
import useAuth from "@hooks/useAuth";
import useRequest from "@hooks/useRequest";
import { useEffect, useState } from "react";
import Config from "react-native-config";
import { ItemProps, Order, Plan } from "types";

const useExplore = (
  type: "orderer" | "traveller",
): [
  data: ItemProps[],
  loading: boolean,
  refreshing: boolean,
  setRefreshing: () => void,
] => {
  const [data, setData] = useState<ItemProps[]>([]);
  const [loading, setLoading] = useState(false);
  const [refreshing, setRefreshing] = useState(false);
  const request = useRequest();
  const { user } = useAuth();

  useEffect(() => {
    setRefreshing(true);
  }, []);

  useEffect(() => {
    if (refreshing) {
      let url = "";
      if (type === "orderer") {
        url = Endpoints.GetOrder;
      } else {
        url = Endpoints.GetPlan;
      }
      setLoading(true);
      request(url, {
        method: "GET",
        headers: { Authorization: `Bearer ${user.token}` },
      })
        .then((res: { Data: Order[] & Plan[] }) => {
          let updatedData = [];
          if (type === "orderer") {
            updatedData = res.Data.map((plan: Order) => {
              return {
                date: plan.date,
                toDestination: plan.to.city,
                fromDestination: plan.from.city,
                id: plan._id,
                rateORitem: plan.itemName,
                type,
                userImage: plan.profileId.avatarUrl
                  ? {
                      uri: `${Config.STATIC_URL}/${plan.profileId.avatarUrl}`,
                    }
                  : undefined,
                spaceORweight: plan.weight,
              };
            });
          } else {
            updatedData = res.Data.map((plan: Plan) => {
              return {
                date: plan.date,
                toDestination: plan.to.city,
                fromDestination: plan.from.city,
                id: plan._id,
                rateORitem: plan.price,
                type,
                userImage: plan.profileId.avatarUrl
                  ? {
                      uri: `${Config.STATIC_URL}/${plan.profileId.avatarUrl}`,
                    }
                  : undefined,
                spaceORweight: plan.space,
              };
            });
          }
          setLoading(false);
          setRefreshing(false);
          setData(updatedData);
        })
        .catch((err) => {
          setLoading(false);
          setRefreshing(false);
          console.log({ err });
        });
    }
  }, [request, user?.token, type, refreshing]);

  const onRefresh = () => {
    setRefreshing(true);
  };

  return [data, loading, refreshing, onRefresh];
};

export default useExplore;
