import Endpoints from "@constant/Endpoint";
import useAuth from "@hooks/useAuth";
import useRequest from "@hooks/useRequest";
import { useEffect, useState } from "react";
import Config from "react-native-config";
import { ItemProps, Order } from "types";

const useGetRecentOrder = (): [orders: ItemProps[], loading: boolean] => {
  const [orders, setOrders] = useState<ItemProps[]>([]);
  const [loading, setLoading] = useState<boolean>(false);
  const request = useRequest();
  const { user } = useAuth();

  useEffect(() => {
    setLoading(true);
    request(Endpoints.GetRecentOrders, {
      method: "GET",
      headers: { Authorization: `Bearer ${user.token}` },
    })
      .then((res: { Orders: Order[] }) => {
        setOrders(
          res.Orders.map((order: Order) => {
            return {
              date: order.date,
              toDestination: order.to.city,
              fromDestination: order.from.city,
              rateORitem: order.itemName,
              id: order._id,
              type: "orderer",
              userImage: order.profileId.avatarUrl
                ? {
                    uri: `${Config.STATIC_URL}/${order.profileId.avatarUrl}`,
                  }
                : undefined,
              spaceORweight: order.weight,
            };
          }),
        );
        setLoading(false);
      })
      .catch((err) => {
        setLoading(false);
        console.log({ err });
      });
  }, [request, user?.token]);

  return [orders, loading];
};

export default useGetRecentOrder;
