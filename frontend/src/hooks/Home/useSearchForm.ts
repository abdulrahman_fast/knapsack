import Endpoints from "@constant/Endpoint";
import useAuth from "@hooks/useAuth";
import useRequest from "@hooks/useRequest";
import { useNavigation } from "@react-navigation/core";
import * as React from "react";
import Config from "react-native-config";
import SimpleToast from "react-native-simple-toast";
import { Order, Plan, TabValues } from "types";
import useGetCities from "./useCities";
export type SeachFormParams = {
  to: { id: string; name: string };
  from: { id: string; name: string };
  date?: string | Date;
  space?: number;
};

const useSearchForm = (
  type: TabValues,
): [
  form: SeachFormParams,
  updateForm: (
    key: string,
    value: Date | number | string | { id: string; name: string },
  ) => void,
  Submit: () => void,
  cities: { id: string; name: string }[],
] => {
  const [cities] = useGetCities();
  const request = useRequest();
  const { user } = useAuth();
  const { navigate } = useNavigation();
  const [form, setForm] = React.useState<SeachFormParams>({
    to: cities[0],
    from: cities[0],
  });

  React.useEffect(() => {
    () => {
      setForm({ to: cities[0], from: cities[0], date: new Date(), space: 0 });
    };
  }, [cities]);

  const Submit = (isSearch = false) => {
    if (!validate(form)) {
      return;
    }

    let url = "";
    if (type === "orderer") {
      url = Endpoints.SearchPlans;
    } else {
      url = Endpoints.SearchOrder;
    }

    const { to, from, date, space } = form;
    try {
      request(url, {
        method: "POST",
        body: {
          to: to.id,
          from: from.id,
          date,
          space,
        },
        headers: { Authorization: `Bearer ${user.token}` },
      })
        .then((res: { Data: Order[] & Plan[] }) => {
          let updatedData = [];
          if (type === "orderer") {
            updatedData = res.Data.map((plan: Order) => {
              return {
                date: plan.date,
                toDestination: plan.to.city,
                fromDestination: plan.from.city,
                id: plan._id,
                rateORitem: plan.itemName,
                type,
                userImage: plan.profileId.avatarUrl
                  ? {
                      uri: `${Config.STATIC_URL}/${plan.profileId.avatarUrl}`,
                    }
                  : undefined,
                spaceORweight: plan.weight,
              };
            });
          } else {
            updatedData = res.Data.map((plan: Plan) => {
              return {
                date: plan.date,
                toDestination: plan.to.city,
                fromDestination: plan.from.city,
                id: plan._id,
                rateORitem: plan.price,
                type,
                userImage: plan.profileId.avatarUrl
                  ? {
                      uri: `${Config.STATIC_URL}/${plan.profileId.avatarUrl}`,
                    }
                  : undefined,
                spaceORweight: plan.space,
              };
            });
          }
          if (isSearch) {
            navigate("search-list", { data: updatedData });
          } else {
            navigate("search", {
              screen: "search-list",
              params: {
                data: updatedData,
              },
            });
          }
        })
        .catch((err) => {
          console.log({ err });
        });
    } catch (e) {
      console.log({ e });
    }
  };

  const validate = (_form: SeachFormParams) => {
    const { to, from } = _form;
    if (!to) {
      SimpleToast.show("To is required!", SimpleToast.LONG);
      return false;
    }
    if (!from) {
      SimpleToast.show("From is required!", SimpleToast.LONG);
      return false;
    }

    return true;
  };

  const updateForm = (
    key: string,
    value: Date | number | string | { id: string; name: string },
  ) => {
    setForm((prevValues: SeachFormParams) => {
      return {
        ...prevValues,
        [key]: value,
      };
    });
  };

  return [form, updateForm, Submit, cities];
};

export default useSearchForm;
