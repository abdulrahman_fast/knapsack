import Endpoints from "@constant/Endpoint";
import useAuth from "@hooks/useAuth";
import useRequest from "@hooks/useRequest";
import { useState, useCallback } from "react";
import Config from "react-native-config";
import Snackbar from "react-native-snackbar";
import { Order, OrderDetails, Plan, Requests } from "types";

const DEFAULTS: OrderDetails = {
  date: new Date(),
  StatusOrderRequest: "",
  isUserHaveRequested: false,
  relatedTravellers: [],
  requests: [],
  toDestination: "",
  fromDestination: "",
  nameORDate: "",
  booked: false,
  note: "",
  isFav: false,
  phoneNumber: "",
  email: "",
  id: "",
  myOrder: false,
  type: "",
  userImage: undefined,
  sapceORWeight: "",
};
const useGetOrderDetails = () => {
  const request = useRequest();
  const { user } = useAuth();
  const [orderDetails, setOrderDetails] = useState<OrderDetails>(DEFAULTS);
  const [loading, setLoading] = useState(false);
  const [favouriteLoading, setFavouriteLoading] = useState(false);
  const [bookedLoading, setBookedLoading] = useState(false);

  const getOrderDetails = useCallback(
    (id: string) => {
      setLoading(true);
      request(`${Endpoints.GetOrderById}/${id}`, {
        method: "GET",
        headers: { Authorization: `Bearer ${user.token}` },
      })
        .then(
          (res: {
            order: Order;
            isUserHaveRequested: boolean;
            RelatedTravellers: { Plans: Plan[] };
            isFav: boolean;
            StatusOrderRequest: "pending" | "approved" | "rejected";
            requests: Requests[];
          }) => {
            const {
              order,
              isUserHaveRequested,
              StatusOrderRequest,
              isFav,
              RelatedTravellers,
              requests,
            } = res;
            setOrderDetails({
              date: order.date,
              isUserHaveRequested,
              StatusOrderRequest,
              isFav,
              booked: order.booked,
              relatedTravellers:
                RelatedTravellers?.Plans && RelatedTravellers.Plans.length > 0
                  ? RelatedTravellers.Plans.map((plan: Plan) => {
                      return {
                        date: plan.date,
                        toDestination: `${plan.to.city}`,
                        fromDestination: `${plan.from.city}`,
                        id: plan._id,
                        rateORitem: plan.price,
                        type: "traveller",
                        userImage: plan.profileId.avatarUrl
                          ? {
                              uri: `${Config.STATIC_URL}/${plan.profileId.avatarUrl}`,
                            }
                          : undefined,
                        spaceORweight: plan.space,
                      };
                    })
                  : [],
              requests: requests.map((request: Requests) => {
                return {
                  date: request.date,
                  type: "orderer",
                  id: request._id,
                  rateORitem: request.price,
                  toDestination: `${order.to.city}`,
                  fromDestination: `${order.from.city}`,
                  spaceORweight: request.weight,
                  note: request.note,
                  name: `${request.fromUserId.firstName} ${request.fromUserId.lastName}`,

                  userImage: request.fromUserId.avatarUrl
                    ? {
                        uri: `${Config.STATIC_URL}/${request.fromUserId.avatarUrl}`,
                      }
                    : undefined,
                  nameORDate: order.itemName,
                };
              }),
              toDestination: `${order.to.city}`,
              fromDestination: `${order.from.city}`,
              nameORDate: order.itemName,
              note: order.note,
              phoneNumber: order.profileId.contactPhone,
              email: order.profileId.contactEmail,
              id: order._id,
              myOrder: order.profileId._id === user.user._id,
              type: "orderer",
              userImage: order.profileId.avatarUrl
                ? {
                    uri: `${Config.STATIC_URL}/${order.profileId.avatarUrl}`,
                  }
                : undefined,
              sapceORWeight: order.weight,
            });
            setLoading(false);
          },
        )
        .catch((err) => {
          setLoading(false);
          console.log({ err });
        });
    },
    [request, user.user._id, user.token],
  );

  const removeRequestFromOrderDetails = (item: any) => {
    setOrderDetails((prevValue) => {
      return {
        ...prevValue,
        requests: prevValue.requests.filter((x) => x.id !== item.id),
      };
    });
  };

  const markRequestPending = () => {
    setOrderDetails((prevValue) => {
      return {
        ...prevValue,
        isUserHaveRequested: true,
        StatusOrderRequest: "pending",
      };
    });
  };
  const markRequestAsBooked = useCallback(
    (id: string) => {
      setBookedLoading(true);
      request(`${Endpoints.MarkOrderFull}/${id}`, {
        method: "PUT",
        headers: { Authorization: `Bearer ${user.token}` },
      })
        .then((_) => {
          setOrderDetails((prevValue) => {
            return {
              ...prevValue,
              booked: true,
            };
          });
          Snackbar.show({
            text: "Order Mark as Booked",
            duration: Snackbar.LENGTH_LONG,
          });
          setBookedLoading(false);
        })
        .catch((err) => {
          setBookedLoading(false);
          console.log({ err });
        });
    },
    [request, user.token],
  );
  const markRequestAsFavourite = useCallback(
    (id: string, onFav?: () => void) => {
      setFavouriteLoading(true);
      request(`${Endpoints.FavouriteOrder}`, {
        method: "POST",
        body: {
          id,
        },
        headers: { Authorization: `Bearer ${user.token}` },
      })
        .then((res: { Update: boolean }) => {
          setOrderDetails((prevValue) => {
            return {
              ...prevValue,
              isFav: res?.Update,
            };
          });

          Snackbar.show({
            text: res?.Update
              ? "Order Marked as favourite"
              : "Order Removed from favourite",
            duration: Snackbar.LENGTH_LONG,
            action: {
              text: "UNDO",
              textColor: "green",
              onPress: () => {
                markRequestAsFavourite(id, onFav);
              },
            },
          });
          setFavouriteLoading(false);
          onFav && onFav();
        })
        .catch((err) => {
          setFavouriteLoading(false);
          console.log({ err });
        });
    },
    [request, user.token],
  );
  return [
    orderDetails,
    removeRequestFromOrderDetails,
    markRequestPending,
    getOrderDetails,
    markRequestAsBooked,
    markRequestAsFavourite,
    loading,
    favouriteLoading,
    bookedLoading,
  ];
};

export default useGetOrderDetails;
