import Endpoints from "@constant/Endpoint";
import useAuth from "@hooks/useAuth";
import useRequest from "@hooks/useRequest";
import moment from "moment";
import { useCallback, useState } from "react";
import Config from "react-native-config";
import Snackbar from "react-native-snackbar";
import { Order, Plan, PlanDetails, Requests, Review } from "types";

const DEFAULTS: PlanDetails = {
  date: new Date(),
  StatusPlanRequest: "",
  isUserHaveRequested: false,
  orders: [],
  requests: [],
  toDestination: "",
  fromDestination: "",
  nameORDate: "",
  booked: false,
  note: "",
  isFav: false,
  price: -1,
  phoneNumber: "",
  email: "",
  id: "",
  reviews: [],
  myPlan: false,
  type: "",
  userImage: undefined,
  sapceORWeight: "",
};

const useGetPlanDetails = () => {
  const request = useRequest();
  const { user } = useAuth();
  const [planDetails, setPlanDetails] = useState<PlanDetails>(DEFAULTS);
  const [loading, setLoading] = useState(false);
  const [favouriteLoading, setFavouriteLoading] = useState(false);
  const [bookedLoading, setBookedLoading] = useState(false);
  const getPlanDetails = useCallback(
    (id: string) => {
      setLoading(true);
      request(`${Endpoints.GetPlanById}/${id}`, {
        method: "GET",
        headers: { Authorization: `Bearer ${user.token}` },
      })
        .then(
          (res: {
            plan: Plan;
            requests: Requests[];
            isFav: boolean;
            RelatedOrders: { Orders: Order[] };
            StatusPlanRequest: string;
            reviews: Review[],
            isUserHaveRequested: boolean;
          }) => {
            const {
              plan,
              requests,
              RelatedOrders,
              isFav,
              reviews,
              StatusPlanRequest,
              isUserHaveRequested,
            } = res;

            console.log({
              reviews,
            })
            setPlanDetails({
              date: plan.date,
              StatusPlanRequest,
              isFav,
              isUserHaveRequested,
              booked: plan.booked,
              reviews,
              orders:
                RelatedOrders?.Orders && RelatedOrders.Orders.length > 0
                  ? RelatedOrders.Orders.map((order: Order) => {
                    return {
                      date: order.date,
                      toDestination: `${order.to.city}`,
                      fromDestination: `${order.from.city}`,
                      id: order._id,
                      rateORitem: order.price,
                      type: "orderer",
                      userImage: order.profileId.avatarUrl
                        ? {
                          uri: `${Config.STATIC_URL}/${order.profileId.avatarUrl}`,
                        }
                        : undefined,
                      spaceORweight: order.weight,
                    };
                  })
                  : [],
              requests: requests.map((request: Requests) => {
                return {
                  date: request.date,
                  type: "traveller",
                  id: request._id,
                  rateORitem: request.price,
                  toDestination: `${plan.to.city}`,
                  fromDestination: `${plan.from.city}`,
                  spaceORweight: request.weight,
                  note: request.note,
                  name: `${request.fromUserId.firstName} ${request.fromUserId.lastName}`,
                  userImage: request.fromUserId.avatarUrl
                    ? {
                      uri: `${Config.STATIC_URL}/${request.fromUserId.avatarUrl}`,
                    }
                    : undefined,
                  nameORDate: plan.itemName,
                };
              }),
              toDestination: `${plan.to.city}`,
              fromDestination: `${plan.from.city}`,
              nameORDate: moment(plan.date).format("LL"),
              note: plan.note,
              price: plan.price,
              phoneNumber: plan.profileId.contactPhone,
              email: plan.profileId.contactEmail,
              id: plan._id,
              myPlan: plan.profileId._id === user.user._id,
              type: "traveller",
              userImage: plan.profileId.avatarUrl
                ? {
                  uri: `${Config.STATIC_URL}/${plan.profileId.avatarUrl}`,
                }
                : undefined,
              sapceORWeight: plan.space,
            });
            setLoading(false);
          },
        )
        .catch((err) => {
          setLoading(false);
          console.log({ err });
        });
    },
    [request, user.user._id, user.token],
  );

  const removeRequestPlanOrderDetails = (item: any) => {
    setPlanDetails((prevValue) => {
      return {
        ...prevValue,
        requests: prevValue.requests.filter((x) => x.id !== item.id),
      };
    });
  };

  const markRequestPending = () => {
    setPlanDetails((prevValue) => {
      return {
        ...prevValue,
        isUserHaveRequested: true,
        StatusPlanRequest: "pending",
      };
    });
  };

  const markRequestAsBooked = useCallback(
    (id: string) => {
      setBookedLoading(true);
      request(`${Endpoints.MarkPlanFull}/${id}`, {
        method: "PUT",
        headers: { Authorization: `Bearer ${user.token}` },
      })
        .then((_) => {
          setPlanDetails((prevValue) => {
            return {
              ...prevValue,
              booked: true,
            };
          });
          Snackbar.show({
            text: "Trip Mark as Booked",
            duration: Snackbar.LENGTH_LONG,
          });
          setBookedLoading(false);
        })
        .catch((err) => {
          setBookedLoading(false);
          console.log({ err });
        });
    },
    [request, user.token],
  );
  const markRequestAsFavourite = useCallback(
    (id: string, onFav?: () => void) => {
      setFavouriteLoading(true);
      request(`${Endpoints.FavouritePlan}`, {
        method: "POST",
        body: {
          id,
        },
        headers: { Authorization: `Bearer ${user.token}` },
      })
        .then((res: { Update: boolean }) => {
          setPlanDetails((prevValue) => {
            return {
              ...prevValue,
              isFav: res?.Update,
            };
          });
          Snackbar.show({
            text: res?.Update
              ? "Trip Marked as favourite"
              : "Trip Removed from favourite",
            duration: Snackbar.LENGTH_LONG,
            action: {
              text: "UNDO",
              textColor: "green",
              onPress: () => {
                markRequestAsFavourite(id, onFav);
              },
            },
          });
          setFavouriteLoading(false);
          onFav && onFav();
        })
        .catch((err) => {
          setFavouriteLoading(false);
          console.log({ err });
        });
    },
    [request, user.token],
  );
  return [
    planDetails,
    removeRequestPlanOrderDetails,
    markRequestPending,
    getPlanDetails,
    markRequestAsBooked,
    markRequestAsFavourite,
    loading,
    favouriteLoading,
    bookedLoading,
  ];
};

export default useGetPlanDetails;
