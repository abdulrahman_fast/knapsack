import Endpoints from "@constant/Endpoint";
import useAuth from "@hooks/useAuth";
import useRequest from "@hooks/useRequest";
import { useEffect, useState } from "react";
import Config from "react-native-config";
import { ItemProps, Plan } from "types";

const useGetRecentPlan = (): [plans: ItemProps[], loading: boolean] => {
  const [plans, setPlans] = useState<ItemProps[]>([]);
  const [loading, setLoading] = useState<boolean>(false);
  const request = useRequest();
  const { user } = useAuth();

  useEffect(() => {
    setLoading(true);
    request(Endpoints.GetRecentPlans, {
      method: "GET",
      headers: { Authorization: `Bearer ${user.token}` },
    })
      .then((res: { Plans: Plan[] }) => {
        setPlans(
          res.Plans.map((plan: Plan) => {
            return {
              date: plan.date,
              toDestination: plan.to.city,
              fromDestination: plan.from.city,
              id: plan._id,
              rateORitem: plan.price,
              type: "traveller",
              userImage: plan.profileId.avatarUrl
                ? {
                    uri: `${Config.STATIC_URL}/${plan.profileId.avatarUrl}`,
                  }
                : undefined,
              spaceORweight: plan.space,
            };
          }),
        );
        setLoading(false);
      })
      .catch((err) => {
        setLoading(false);
        console.log({ err });
      });
  }, [request, user?.token]);

  return [plans, loading];
};

export default useGetRecentPlan;
