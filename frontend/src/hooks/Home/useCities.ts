import Endpoints from "@constant/Endpoint";
import useRequest from "@hooks/useRequest";
import { useEffect, useState } from "react";
const useGetCities = () => {
  const request = useRequest();
  const [cities, setCities] = useState<any>({});

  useEffect(() => {
    request(`${Endpoints.GetCities}`, {
      method: "GET",
    })
      .then((res: { cities: { id: string; name: string }[] }) => {
        setCities(res.cities);
      })
      .catch((err) => {
        console.log({ err });
      });
  }, [request]);

  return [cities];
};

export default useGetCities;
