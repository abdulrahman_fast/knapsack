import React, { useState } from "react";

const useSheetTitleAndStatus = (
  details: any,
): [title: string, status: "review" | "reject" | undefined] => {
  const [title, setTitle] = useState<string>("");
  const [status, setStatus] = useState<"review" | "reject" | undefined>(
    undefined,
  );

  React.useEffect(() => {
    const {
      myOrder: isOwnRequest,
      isUserHaveRequested,
      StatusOrderRequest,
      booked,
    } = details;

    setTitle(
      isOwnRequest
        ? !booked
          ? "MARK ORDER AS BOOKED"
          : "Review Orders"
        : isUserHaveRequested
        ? StatusOrderRequest === "pending"
          ? "REQUESTED FOR APPROVAL"
          : StatusOrderRequest === "approved"
          ? "View Contact Details"
          : "REJECTED"
        : "REQUEST FOR ORDER",
    );

    console.log({
      isOwnRequest,
      isUserHaveRequested,
      StatusOrderRequest,
      booked,
      details,
    });

    setStatus(
      !isOwnRequest && isUserHaveRequested && StatusOrderRequest === "pending"
        ? "review"
        : StatusOrderRequest === "rejected"
        ? "reject"
        : undefined,
    );
  }, [details]);

  return [title, status];
};

export default useSheetTitleAndStatus;
