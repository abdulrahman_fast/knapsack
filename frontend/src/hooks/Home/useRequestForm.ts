import Endpoints from "@constant/Endpoint";
import useAuth from "@hooks/useAuth";
import useRequest from "@hooks/useRequest";
import useOrderDetailProvider from "@providers/Home/OrderDetailProvider";
import usePlanDetailProvider from "@providers/Home/PlanDetailProvider";
import { RouteProp, useNavigation, useRoute } from "@react-navigation/core";
import * as React from "react";
import SimpleToast from "react-native-simple-toast";
import Snackbar from "react-native-snackbar";
import { TabValues } from "types";
import useGetCities from "./useCities";
type ParamList = {
  RequestForOrder: { type: TabValues; id: string };
};

export type RequestFromParams = {
  place: { id: string; name: string };
  date: Date | string;
  note: string;
  itemName: string;
  weight: string;
  price: string;
};

const DEFAULTS = {
  place: {},
  date: new Date(),
  note: "",
  itemName: "",
  weight: "",
  price: "",
};

const useRequestForm = (): [
  form: RequestFromParams,
  updateForm: (
    key: string,
    value: Date | string | { id: string; name: string },
  ) => void,
  Submit: () => void,
  type: TabValues,
  cities: { id: string; name: string }[],
  loading: boolean,
] => {
  const { goBack } = useNavigation();
  const [cities] = useGetCities();
  const request = useRequest();
  const { user } = useAuth();
  const [_, __, markPlanPending] = usePlanDetailProvider();
  const [orderDetails, ___, markOrderPending] = useOrderDetailProvider();
  const {
    params: { type, id },
  } = useRoute<RouteProp<ParamList, "RequestForOrder">>();
  const [loading, setLoading] = React.useState(false);

  const [form, setForm] = React.useState<RequestFromParams>({
    ...DEFAULTS,
    place: cities[0],
    itemName: type === "orderer" ? orderDetails?.nameORDate : "",
  });

  const Submit = () => {
    setLoading(true);
    if (!validate(form)) {
      setLoading(false);
      return;
    }
    const { note, place, itemName, date, weight, price } = form;
    try {
      request(Endpoints.Request, {
        method: "POST",
        body: {
          fromUserId: user.user._id,
          status: "pending",
          note,
          place: place.id,
          itemName,
          date: new Date(date),
          weight: parseInt(weight, 10),
          price: parseInt(price, 10),
          orderId: type === "orderer" ? id : null,
          planId: type === "traveller" ? id : null,
        },
        headers: { Authorization: `Bearer ${user.token}` },
      })
        .then((res) => {
          if (res) {
            console.log({ res });
            type === "orderer" ? markOrderPending() : markPlanPending();
            goBack();
            Snackbar.show({
              text: "Request Created",
              duration: Snackbar.LENGTH_LONG,
            });
          }
        })
        .catch((err) => {
          console.log({ err });
        });
    } catch (e) {
      console.log({ e });
    }
  };

  const validate = (_form: RequestFromParams) => {
    const { place, date, note, weight, price, itemName } = _form;
    if (!place) {
      Snackbar.show({
        text: "Place is required!",
        duration: Snackbar.LENGTH_LONG,
      });
      return false;
    }
    if (!date) {
      Snackbar.show({
        text: "Date is required!",
        duration: Snackbar.LENGTH_LONG,
      });
      return false;
    }
    if (!note) {
      Snackbar.show({
        text: "Note is required!",
        duration: Snackbar.LENGTH_LONG,
      });
      return false;
    }
    if (!weight) {
      Snackbar.show({
        text: "Weight is required!",
        duration: Snackbar.LENGTH_LONG,
      });
      return false;
    }
    if (!price) {
      Snackbar.show({
        text: "Price is required!",
        duration: Snackbar.LENGTH_LONG,
      });
      return false;
    }

    if (!itemName) {
      Snackbar.show({
        text: "ItemName is required!",
        duration: Snackbar.LENGTH_LONG,
      });
      return false;
    }

    return true;
  };

  const updateForm = (
    key: string,
    value: Date | string | { id: string; name: string },
  ) => {
    setForm((prevValues: RequestFromParams) => {
      return {
        ...prevValues,
        [key]: value,
      };
    });
  };

  return [form, updateForm, Submit, type, cities, loading];
};

export default useRequestForm;
