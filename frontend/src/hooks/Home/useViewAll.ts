import Endpoints from "@constant/Endpoint";
import useAuth from "@hooks/useAuth";
import useRequest from "@hooks/useRequest";
import { useEffect, useState } from "react";
import Config from "react-native-config";
import { ItemProps, Order, Plan } from "types";

const useViewAll = (type: "orderer" | "traveller") => {
  const [data, setData] = useState<ItemProps[]>([]);
  const request = useRequest();
  const { user } = useAuth();

  useEffect(() => {
    let url = "";
    if (type === "orderer") {
      url = Endpoints.GetOrder;
    } else {
      url = Endpoints.GetPlan;
    }

    request(url, {
      method: "GET",
      headers: { Authorization: `Bearer ${user.token}` },
    })
      .then((res: { Data: Order[] & Plan[] }) => {
        let updatedData = [];
        if (type === "orderer") {
          updatedData = res.Data.map((plan: Order) => {
            return {
              date: plan.date,
              toDestination: plan.to.city,
              fromDestination: plan.from.city,
              id: plan._id,
              rateORitem: plan.itemName,
              type,
              userImage: plan.profileId.avatarUrl
                ? {
                  uri: `${Config.STATIC_URL}/${plan.profileId.avatarUrl}`,
                }
                : undefined,
              spaceORweight: plan.weight,
            };
          });
        } else {
          updatedData = res.Data.map((plan: Plan) => {
            return {
              date: plan.date,
              toDestination: plan.to.city,
              fromDestination: plan.from.city,
              id: plan._id,
              rateORitem: plan.price,
              type,
              userImage: plan.profileId.avatarUrl
                ? {
                  uri: `${Config.STATIC_URL}/${plan.profileId.avatarUrl}`,
                }
                : undefined,
              spaceORweight: plan.space,
            };
          });
        }

        setData(updatedData);
      })
      .catch((err) => {
        console.log({ err });
      });
  }, [request, user?.token, type]);

  return [data];
};

export default useViewAll;
