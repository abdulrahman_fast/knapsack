import Endpoints from "@constant/Endpoint";
import useAuth from "@hooks/useAuth";
import useRequest from "@hooks/useRequest";
import { useEffect, useState } from "react";

const useStats = () => {
  const [stats, setStats] = useState({ plans: 0, orders: 0 });
  const [refresh, setRefresh] = useState(true);
  const request = useRequest();
  const { user } = useAuth();

  useEffect(() => {
    if (refresh) {
      request(Endpoints.GetStats, {
        method: "GET",
        headers: { Authorization: `Bearer ${user.token}` },
      })
        .then((res) => {
          setStats(res);
          setRefresh(false);
        })
        .catch((err) => {
          console.log({ err });
          setRefresh(false);
        });
    }
  }, [request, user?.token, refresh]);

  return [stats, setRefresh];
};

export default useStats;
