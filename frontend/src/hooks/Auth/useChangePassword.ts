import Endpoints from "@constant/Endpoint";
import useRequest from "@hooks/useRequest";
import { useNavigation } from "@react-navigation/core";
import React from "react";
import Snackbar from "react-native-snackbar";

const useChangePassword = ({
  email,
  loginId,
  token,
}: {
  email: string;
  loginId: string;
  token: string;
}): [
  password: string,
  changePassword: React.Dispatch<React.SetStateAction<string>>,
  cPassword: string,
  changeCPassword: React.Dispatch<React.SetStateAction<string>>,
  setPassword: () => void,
  loading: boolean,
] => {
  const [password, changePassword] = React.useState("Ahsansam");
  const [cPassword, changeCPassword] = React.useState("Ahsansam");
  const [loading, setLoading] = React.useState(false);
  const { navigate } = useNavigation();
  const request = useRequest();

  const setPassword = () => {
    setLoading(true);
    if (password && password === "") {
      Snackbar.show({
        text: "Password is required",
        duration: Snackbar.LENGTH_LONG,
      });
      setLoading(false);

      return;
    }

    if (cPassword && cPassword === "") {
      Snackbar.show({
        text: "Confirm Password is required",
        duration: Snackbar.LENGTH_LONG,
      });
      setLoading(false);

      return;
    }

    if (password !== cPassword) {
      Snackbar.show({
        text: "Password doesnot match",
        duration: Snackbar.LENGTH_LONG,
      });
      setLoading(false);

      return;
    }

    try {
      request(Endpoints.SetPassword, {
        method: "POST",
        body: { email, password },
      })
        //.then((res) => res.json())
        .then((res) => {
          console.log({ res });
          setLoading(false);
          navigate("editprofile", { email, loginId, token });
        })
        .catch((err) => {
          setLoading(false);
          console.log({ err });
        });
    } catch (err) {
      setLoading(false);
      console.log({ err });
    }
  };

  return [
    password,
    changePassword,
    cPassword,
    changeCPassword,
    setPassword,
    loading,
  ];
};

export default useChangePassword;
