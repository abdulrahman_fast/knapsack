import Endpoints from "@constant/Endpoint";
import useAuth from "@hooks/useAuth";
import useRequest from "@hooks/useRequest";
import React, { useState } from "react";
import Snackbar from "react-native-snackbar";
const useSignIn = (): [
  email: string,
  setEmail: React.Dispatch<React.SetStateAction<string>>,
  password: string,
  setPassword: React.Dispatch<React.SetStateAction<string>>,
  Login: () => Promise<void>,
  loading: boolean,
] => {
  const request = useRequest();
  const [email, setEmail] = useState("ahsan.kai@gmail.com");
  const [password, setPassword] = useState("Ahsansam");
  const [loading, setLoading] = useState(false);
  const { setLoggedUser } = useAuth();

  React.useEffect(() => {
    return () => {
      setEmail("");
      setPassword("");
      setLoading(false);
    };
  }, []);

  const Login = async () => {
    setLoading(true);
    if (email && email === "") {
      Snackbar.show({
        text: "Email is required",
        duration: Snackbar.LENGTH_LONG,
      });
      setLoading(false);
      return;
    }

    if (password && password === "") {
      Snackbar.show({
        text: "Password is required",
        duration: Snackbar.LENGTH_LONG,
      });
      setLoading(false);
      return;
    }

    try {
      const res = await request(Endpoints.SignIn, {
        method: "POST",
        body: { email, password },
      });
      if (res) {
        const obj = {
          token: res.token,
          user: {
            ...res.user,
            _id: res.user._id,
          },
          loginId: res.user.loginId,
        };
        setLoading(false);

        setLoggedUser(obj);
      }
    } catch (err) {
      setLoading(false);
      console.log({ err });
    }
  };
  return [email, setEmail, password, setPassword, Login, loading];
};

export default useSignIn;
