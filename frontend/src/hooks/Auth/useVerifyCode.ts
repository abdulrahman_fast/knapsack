import Endpoints from "@constant/Endpoint";
import useRequest from "@hooks/useRequest";
import { useNavigation } from "@react-navigation/core";
import React from "react";
import Snackbar from "react-native-snackbar";
const useVerifyCode = ({
  email,
}: {
  email: string;
}): [
  code: string,
  setCode: React.Dispatch<React.SetStateAction<string>>,
  Send: () => void,
  loading: boolean,
] => {
  const [code, setCode] = React.useState("");
  const [loading, setLoading] = React.useState(false);
  const request = useRequest();
  const { navigate } = useNavigation();
  React.useEffect(() => {
    return () => {
      setCode("");
      setLoading(false);
    };
  }, []);

  const Send = () => {
    setLoading(true);
    try {
      request(Endpoints.VerifyCode, {
        method: "POST",
        body: { code, email },
      })
        //.then((res) => res.json())
        .then((res) => {
          console.log({ res });
          if (res.codeVerify) {
            navigate("setpassword", {
              email,
              loginId: res.user._id,
              token: res.token,
            });
            Snackbar.show({
              text: "Token Verify",
              duration: Snackbar.LENGTH_LONG,
            });
          } else {
            Snackbar.show({
              text: "Invalid Token",
              duration: Snackbar.LENGTH_LONG,
            });
          }
          setLoading(false);
        })
        .catch((err) => {
          setLoading(false);
          console.log({ err });
        });
    } catch (err) {
      setLoading(false);
      console.log({ err });
    }
  };
  return [code, setCode, Send, loading];
};

export default useVerifyCode;
