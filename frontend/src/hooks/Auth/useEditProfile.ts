import Endpoints from "@constant/Endpoint";
import useAuth from "@hooks/useAuth";
import useRequest from "@hooks/useRequest";
import { useNavigation } from "@react-navigation/core";
import React from "react";
import { ImagePickerResponse } from "react-native-image-picker";
import Snackbar from "react-native-snackbar";

const useEditProfile = ({
  email,
  loginId,
  token,
}: {
  email: string;
  loginId: string;
  token: string;
}): [
  firstName: string,
  lastName: string,
  country: string,
  phoneNumber: string,
  image: ImagePickerResponse | undefined,
  setFirstName: React.Dispatch<React.SetStateAction<string>>,
  setLastName: React.Dispatch<React.SetStateAction<string>>,
  setCountry: React.Dispatch<React.SetStateAction<string>>,
  setPhoneNumber: React.Dispatch<React.SetStateAction<string>>,
  setImage: React.Dispatch<
    React.SetStateAction<ImagePickerResponse | undefined>
  >,
  SetProfile: () => void,
  loading: boolean,
] => {
  const [firstName, setFirstName] = React.useState("Ahsan");
  const [lastName, setLastName] = React.useState("Sohail");
  const [country, setCountry] = React.useState("Pakistan");
  const [phoneNumber, setPhoneNumber] = React.useState("+923328287820");
  const [image, setImage] = React.useState<ImagePickerResponse | undefined>(
    undefined,
  );
  const [loading, setLoading] = React.useState(false);
  const request = useRequest();
  const { setLoggedUser } = useAuth();
  const { navigate } = useNavigation();

  const SetProfile = () => {
    setLoading(true);
    if (firstName && firstName === "") {
      Snackbar.show({
        text: "Fist Name is required",
        duration: Snackbar.LENGTH_LONG,
      });
      setLoading(false);
      return;
    }

    if (lastName && lastName === "") {
      Snackbar.show({
        text: "Last Name is required",
        duration: Snackbar.LENGTH_LONG,
      });
      setLoading(false);
      return;
    }

    if (country && country === "") {
      Snackbar.show({
        text: "Country doesnot match",
        duration: Snackbar.LENGTH_LONG,
      });
      setLoading(false);
      return;
    }

    if (phoneNumber && phoneNumber === "") {
      Snackbar.show({
        text: "Phone Number doesnot match",
        duration: Snackbar.LENGTH_LONG,
      });
      setLoading(false);
      return;
    }

    if (email && email === "") {
      Snackbar.show({
        text: "Email doesnot match",
        duration: Snackbar.LENGTH_LONG,
      });
      setLoading(false);
      return;
    }

    try {
      const formData = new FormData();
      formData.append("firstName", firstName);
      formData.append("lastName", lastName);
      formData.append("contactEmail", email);
      formData.append("contactPhone", phoneNumber);
      formData.append("country", country);
      formData.append("loginId", loginId);
      if (image) {
        formData.append("file", {
          uri: image?.uri,
          type: image?.type,
          name: `image_${new Date().toISOString()}_${image?.fileName}`,
        });
      }

      request(
        Endpoints.SetProfile,
        {
          method: "POST",
          body: formData,
          headers: {
            Authorization: `Bearer ${token}`,
          },
        },
        true,
      )
        .then((res) => {
          setLoggedUser({
            user: {
              ...res.profile,
              _id: res.profile._id,
            },
            token,
            loginId,
          });
          navigate("Home");
          setLoading(false);
        })
        .catch((err) => {
          setLoading(false);

          console.log({ err });
        });
    } catch (err) {
      setLoading(false);

      console.log({ err });
    }
  };

  return [
    firstName,
    lastName,
    country,
    phoneNumber,
    image,
    setFirstName,
    setLastName,
    setCountry,
    setPhoneNumber,
    setImage,
    SetProfile,
    loading,
  ];
};

export default useEditProfile;
