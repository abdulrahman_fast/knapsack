import Endpoints from "@constant/Endpoint";
import useRequest from "@hooks/useRequest";
import { useNavigation } from "@react-navigation/core";
import React from "react";
const useSignUp = (): [
  email: string,
  setEmail: React.Dispatch<React.SetStateAction<string>>,
  Send: () => void,
  loading: boolean,
] => {
  const [email, setEmail] = React.useState("ahsan.kai@gmail.com");
  const [loading, setLoading] = React.useState(false);
  const request = useRequest();
  const { navigate } = useNavigation();
  React.useEffect(() => {
    return () => {
      setEmail("");
      setLoading(false);
    };
  }, []);

  const Send = () => {
    setLoading(true);
    try {
      request(Endpoints.SignUp, {
        method: "POST",
        body: { email },
      })
        //.then((res) => res.json())
        .then((res) => {
          console.log({ res });
          if (res) {
            navigate("verifyCode", {
              email,
            });
          }
          setLoading(false);
        })
        .catch((err) => {
          setLoading(false);
          console.log({ err });
        });
    } catch (err) {
      setLoading(false);
      console.log({ err });
    }
  };
  return [email, setEmail, Send, loading];
};

export default useSignUp;
