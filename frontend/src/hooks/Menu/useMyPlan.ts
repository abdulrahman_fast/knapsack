import Endpoints from "@constant/Endpoint";
import useAuth from "@hooks/useAuth";
import useRequest from "@hooks/useRequest";
import React from "react";
import Config from "react-native-config";
import { ItemProps, Plan } from "types";

const useMyPlans = (): [
  loading: boolean,
  plans: ItemProps[],
  setRefresh: React.Dispatch<React.SetStateAction<boolean>>,
] => {
  const request = useRequest();
  const { user } = useAuth();
  const [loading, setLoading] = React.useState<boolean>(false);
  const [refresh, setRefresh] = React.useState<boolean>(true);
  const [plans, setPlans] = React.useState<ItemProps[]>([]);

  React.useEffect(() => {
    if (refresh === true) {
      setLoading(true);
      try {
        request(Endpoints.MyPlans, {
          method: "GET",
          headers: { Authorization: `Bearer ${user.token}` },
        })
          .then((res: { Plans: Plan[] }) => {
            const mappedPlans: ItemProps[] = res.Plans.map((plan: Plan) => {
              return {
                date: plan.date,
                toDestination: `${plan.to.city}`,
                fromDestination: `${plan.from.city}`,
                id: plan._id,
                rateORitem: plan.price,
                type: "traveller",
                userImage: plan.profileId.avatarUrl
                  ? {
                      uri: `${Config.STATIC_URL}/${plan.profileId.avatarUrl}`,
                    }
                  : undefined,
                spaceORweight: plan.space,
              };
            });
            setLoading(false);
            setRefresh(false);
            setPlans(mappedPlans);
          })
          .catch((err) => {
            setLoading(false);
            setRefresh(false);
            console.log({ err });
          });
      } catch (e) {
        setLoading(false);
        setRefresh(false);
        console.log({ e });
      }
    }
  }, [request, user.token, refresh]);

  return [loading, plans, setRefresh];
};

export default useMyPlans;
