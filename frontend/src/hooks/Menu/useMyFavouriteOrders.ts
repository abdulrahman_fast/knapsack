import Endpoints from "@constant/Endpoint";
import useAuth from "@hooks/useAuth";
import useRequest from "@hooks/useRequest";
import React from "react";
import Config from "react-native-config";
import { ItemProps, Order } from "types";

const useMyFavouriteOrders = (): [
  loading: boolean,
  plans: ItemProps[],
  setRefresh: React.Dispatch<React.SetStateAction<boolean>>,
] => {
  const request = useRequest();
  const { user } = useAuth();
  const [loading, setLoading] = React.useState<boolean>(false);
  const [refresh, setRefresh] = React.useState<boolean>(true);
  const [orders, setOrders] = React.useState<ItemProps[]>([]);
  React.useEffect(() => {
    if (refresh) {
      setLoading(true);
      try {
        request(Endpoints.FavouriteOrder, {
          method: "GET",
          headers: { Authorization: `Bearer ${user.token}` },
        })
          .then((res: { Orders: Order[] }) => {
            setLoading(false);
            const mappedOrders: ItemProps[] = res.Orders.map((order: Order) => {
              return {
                date: order.date,
                toDestination: `${order.to.city}`,
                fromDestination: `${order.from.city}`,
                id: order._id,
                rateORitem: order.price,
                type: "orderer",
                userImage: order.profileId.avatarUrl
                  ? {
                      uri: `${Config.STATIC_URL}/${order.profileId.avatarUrl}`,
                    }
                  : undefined,
                spaceORweight: order.weight,
              };
            });
            setRefresh(false);
            setOrders(mappedOrders);
          })
          .catch((err) => {
            setLoading(false);
            setRefresh(false);
            console.log({ err });
          });
      } catch (e) {
        setLoading(false);
        setRefresh(false);
        console.log({ e });
      }
    }
  }, [request, user.token, refresh]);

  return [loading, orders, setRefresh];
};

export default useMyFavouriteOrders;
