import Endpoints from "@constant/Endpoint";
import useAuth from "@hooks/useAuth";
import useRequest from "@hooks/useRequest";
import React from "react";
import Config from "react-native-config";
import { ItemProps, RequestsExtended } from "types";

const useMyPendingRequest = (): [
  loading: boolean,
  requests: ItemProps[],
  setRefresh: React.Dispatch<React.SetStateAction<boolean>>,
] => {
  const request = useRequest();
  const { user } = useAuth();
  const [loading, setLoading] = React.useState<boolean>(false);
  const [refresh, setRefresh] = React.useState<boolean>(true);
  const [requests, setRequests] = React.useState<ItemProps[]>([]);
  React.useEffect(() => {
    if (refresh) {
      setLoading(true);
      try {
        request(Endpoints.MyPendingRequests, {
          method: "GET",
          headers: { Authorization: `Bearer ${user.token}` },
        })
          .then((res: { Requests: RequestsExtended[] }) => {
            setLoading(false);
            console.log({ res });
            const mappedRequests: ItemProps[] = res.Requests.map(
              (r: RequestsExtended) => {
                return {
                  date: r.date,
                  type: r.planId ? "traveller" : "orderer",
                  id: r._id,
                  rateORitem: r.price,
                  toDestination: r.planId
                    ? `${r.planId?.to.city}`
                    : `${r.orderId?.to.city}`,
                  fromDestination: r.planId
                    ? `${r.planId?.from.city}`
                    : `${r.orderId?.from.city}`,
                  spaceORweight: r.weight,
                  note: r.note,
                  name: `${r.fromUserId.firstName} ${r.fromUserId.lastName}`,
                  userImage: r.fromUserId.avatarUrl
                    ? {
                        uri: `${Config.STATIC_URL}/${r.fromUserId.avatarUrl}`,
                      }
                    : undefined,
                  nameORDate: r.planId ? r.planId?.itemName : r.orderId?.date,
                };
              },
            );
            setRefresh(false);
            setRequests(mappedRequests);
          })
          .catch((err) => {
            setLoading(false);
            setRefresh(false);
            console.log({ err });
          });
      } catch (e) {
        setLoading(false);
        setRefresh(false);
        console.log({ e });
      }
    }
  }, [request, user.token, refresh]);

  return [loading, requests, setRefresh];
};

export default useMyPendingRequest;
