import Endpoints from "@constant/Endpoint";
import useAuth from "@hooks/useAuth";
import useRequest from "@hooks/useRequest";
import useStatsProvider from "@providers/Home/StatsProvider";
import useMyOrderProvider from "@providers/Menu/MyOrderProvider";
import useMyPlanProvider from "@providers/Menu/MyPlanProvider";
import { useState } from "react";
import Snackbar from "react-native-snackbar";
import { TabValues } from "types";

type NewRequestForm = {
  fromDestination: { id: string; name: string };
  toDestination: { id: string; name: string };
  date: Date | string;
  itemName: string;
  weight: string;
  price: string;
  note: string;
};

const Defaults: NewRequestForm = {
  date: new Date(),
  itemName: "",
  toDestination: { id: "", name: "" },
  weight: "",
  note: "",
  price: "",
  fromDestination: { id: "", name: "" },
};

const useNewPlanForm = (
  searchActive: TabValues,
): [
  form: NewRequestForm,
  UpdateForm: (
    key: string,
    value: string | Date | number | { id: string; name: string },
  ) => void,
  Submit: () => void,
  loading: boolean,
] => {
  const request = useRequest();
  const { user } = useAuth();
  const [_, setRefresh] = useStatsProvider();
  const [__, ___, RefreshMyOrders] = useMyOrderProvider();
  const [____, _____, RefreshMyPlans] = useMyPlanProvider();
  const [form, setForm] = useState<NewRequestForm>(Defaults);
  const [loading, setLoading] = useState(false);

  const Submit = () => {
    setLoading(true);
    if (validate()) {
      const {
        fromDestination,
        toDestination,
        date,
        itemName,
        price,
        weight,
        note,
      } = form;
      try {
        if (searchActive === "traveller") {
          request(Endpoints.Plan, {
            method: "POST",
            body: {
              from: fromDestination.id,
              to: toDestination.id,
              date: date,
              itemName,
              space: weight,
              price,
              note,
            },
            headers: {
              Authorization: `Bearer ${user.token}`,
            },
          })
            .then((res) => {
              console.log({ res });
              if (res) {
                setRefresh(true);
                setLoading(false);
                RefreshMyPlans(true);
                Snackbar.show({
                  text: "Travel Trip Created",
                  duration: Snackbar.LENGTH_LONG,
                });
              }
            })
            .catch((err) => {
              setLoading(false);

              console.log({ err });
            });
        } else {
          request(Endpoints.Order, {
            method: "POST",
            body: {
              from: fromDestination.id,
              to: toDestination.id,
              date,
              itemName,
              weight,
              price,
              note,
            },
            headers: {
              Authorization: `Bearer ${user.token}`,
            },
          })
            .then((res) => {
              if (res) {
                setLoading(false);
                setRefresh(true);
                RefreshMyOrders(true);
                Snackbar.show({
                  text: "Order Request Created",
                  duration: Snackbar.LENGTH_LONG,
                });
              }
            })
            .catch((err) => {
              setLoading(false);
              console.log({ err });
            });
        }
      } catch (e) {
        setLoading(false);
        console.log({ e });
      }
    } else {
      setLoading(false);
    }
  };

  const validate = () => {
    const {
      date,
      itemName,
      toDestination,
      price,
      note,
      weight,
      fromDestination,
    } = form;

    if (!date) {
      Snackbar.show({
        text: "Date is required",
        duration: Snackbar.LENGTH_LONG,
      });
      return false;
    }
    if (searchActive === "orderer" && !itemName) {
      Snackbar.show({
        text: "Item Name is required",
        duration: Snackbar.LENGTH_LONG,
      });
      return false;
    }
    if (!toDestination) {
      Snackbar.show({
        text: "To Destination is required is required",
        duration: Snackbar.LENGTH_LONG,
      });
      return false;
    }
    if (!weight) {
      Snackbar.show({
        text: "Weight is required",
        duration: Snackbar.LENGTH_LONG,
      });
      return false;
    }
    if (!note) {
      Snackbar.show({
        text: "Note is required",
        duration: Snackbar.LENGTH_LONG,
      });
      return false;
    }
    if (!price) {
      Snackbar.show({
        text: "Price is required",
        duration: Snackbar.LENGTH_LONG,
      });
      return false;
    }
    if (!fromDestination) {
      Snackbar.show({
        text: "From Destination is required",
        duration: Snackbar.LENGTH_LONG,
      });
      return false;
    }

    return true;
  };

  const UpdateForm = (
    key: string,
    value: Date | string | number | { id: string; name: string },
  ) => {
    setForm((prevValues: NewRequestForm) => {
      return {
        ...prevValues,
        [key]: value,
      };
    });
  };

  return [form, UpdateForm, Submit, loading];
};

export default useNewPlanForm;
