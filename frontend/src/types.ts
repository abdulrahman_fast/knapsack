import moment from "moment";
import { ImageSourcePropType } from "react-native";

export type TabValues = "" | "traveller" | "orderer";
export type Airport = {
  name: string;
  city: string;
  country: string;
  iata_code: string;
  _geoloc: {
    lat: number;
    lng: number;
  };
  links_count: number;
  objectID: string;
};
export type Profile = {
  _id: string;
  avatarUrl: string;
  firstName: string;
  lastName: string;
  contactEmail: string;
  contactPhone: string;
  country: string;
  loginId: string;
};
export type Plan = {
  from: Airport;
  _id: string;
  to: Airport;
  date: Date;
  profileId: Profile;
  itemName: string;
  space: number;
  price: number;
  booked: boolean; 
  note: string;
  loginId: string;
};
export type Review = {
  _id: string;
  fromUserId: Profile;
  toUserId: Profile;
  rating: number;
  note: string;
}

export type Order = {
  _id: string;
  from: Airport;
  to: Airport;
  date: Date;
  itemName: string;
  profileId: Profile;
  weight: number;
  booked: boolean; 
  price: number;
  note: string;
  loginId: string;
};

export type OrderByIdResponse = {
  _id: string;
  from: Airport;
  to: Airport;
  date: Date;
  itemName: string;
  profileId: Profile;
  weight: number;
  price: number;
  note: string;
  loginId: string;
  isUserHaveRequested: boolean;
};

export type Requests = {
  _id: string;
  fromUserId: Profile;
  status: string;
  note: string;
  place: string;
  date: Date;
  weight: number;
  price: number;
  orderId: string;
  planId: string;
};

export type RequestsExtended = {
  _id: string;
  fromUserId: Profile;
  status: string;
  note: string;
  place: string;
  date: Date;
  weight: number;
  price: number;
  orderId?: Order;
  planId?: Plan;
};

export type DetailsRequest = {
  date: Date | string;
  type: "traveller" | "orderer" | "";
  id: string;
  rateORitem: string | number;
  toDestination: string;
  spaceORweight: string | number;
  note: string;
  name: string;
  userImage?: ImageSourcePropType;
  nameORDate: string;
  fromDestination: string;
};

export interface ItemProps {
  userImage?: ImageSourcePropType;
  date: Date | number;
  toDestination: string;
  id: string;
  fromDestination: string;
  rateORitem: string | number;
  type: "traveller" | "orderer";
  spaceORweight: string | number;
}

export type PlanDetails = {
  date: Date;
  StatusPlanRequest: string | undefined;
  isUserHaveRequested: boolean;
  orders: ItemProps[];
  requests: DetailsRequest[];
  toDestination: string;
  fromDestination: string;
  nameORDate: string | Date | moment.Moment;
  note: string;
  reviews: Review[],
  price: number;
  isFav: boolean;
  phoneNumber: string;
  email: string;
  id: string;
  myPlan: boolean;
  booked: boolean;
  type: "traveller" | "orderer" | "";
  userImage?: ImageSourcePropType;
  sapceORWeight: string | number;
};

export type OrderDetails = {
  date: Date;
  isFav: boolean;
  booked: boolean;
  StatusOrderRequest: string | undefined;
  isUserHaveRequested: boolean;
  relatedTravellers: ItemProps[];
  requests: DetailsRequest[];
  toDestination: string;
  fromDestination: string;
  nameORDate: string | Date | moment.Moment;
  note: string;
  phoneNumber: string;
  email: string;
  id: string;
  myOrder: boolean;
  type: "traveller" | "orderer" | "";
  userImage?: ImageSourcePropType;
  sapceORWeight: string | number;
};
