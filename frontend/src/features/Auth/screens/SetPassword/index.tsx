import * as React from "react";
import { RouteProp, useRoute } from "@react-navigation/native";

import { Box, Button, TextInput } from "components";
import Layout from "@features/Auth/components/Layout";
import useChangePassword from "@hooks/Auth/useChangePassword";

const SetPassword: React.FC = () => {
  const {
    params: { email, loginId, token },
  } = useRoute<
    RouteProp<
      { SetPassword: { email: string; loginId: string; token: string } },
      "SetPassword"
    >
  >();
  const [
    password,
    changePassword,
    cPassword,
    changeCPassword,
    setPassword,
    loading,
  ] = useChangePassword({ email, loginId, token });
  return (
    <Layout
      {...{
        requireBackBtn: true,
        header: "Set a secure password",
        description: "You will use it for your next sign in",
      }}>
      <Box
        flex={1.5}
        zIndex={2}
        marginBottom={"xxl"}
        justifyContent="space-evenly">
        <TextInput
          value={password}
          secureTextEntry={true}
          onChangeText={(text) => changePassword(text)}
          placeholder={"Password"}
        />
        <TextInput
          value={cPassword}
          secureTextEntry={true}
          onChangeText={(text) => changeCPassword(text)}
          placeholder={"Retype password"}
        />
      </Box>
      <Box flex={3} alignItems="center" justifyContent="flex-start" zIndex={2}>
        <Button
          {...{
            size: "100%",
            btnText: "NEXT",
            loading,
            onPress: () => setPassword(),
          }}
        />
      </Box>
    </Layout>
  );
};

export default SetPassword;
