import * as React from "react";

import { Box, TextInput, useAppTheme } from "components";

interface DetailsFormProps {
  firstName: string;
  lastName: string;
  country: string;
  phoneNumber: string;
  email: string;
  setFirstName: React.Dispatch<React.SetStateAction<string>>;
  setLastName: React.Dispatch<React.SetStateAction<string>>;
  setCountry: React.Dispatch<React.SetStateAction<string>>;
  setPhoneNumber: React.Dispatch<React.SetStateAction<string>>;
}

const DetailsForm: React.FC<DetailsFormProps> = ({
  firstName,
  lastName,
  country,
  phoneNumber,
  email,
  setFirstName,
  setLastName,
  setCountry,
  setPhoneNumber,
}: DetailsFormProps) => {
  const theme = useAppTheme();

  return (
    <Box flex={1} marginVertical={"xxl"}>
      <TextInput
        value={firstName}
        size={"100%"}
        onChangeText={(text) => setFirstName(text)}
        placeholder={"First Name"}
        style={{ marginBottom: theme.spacing.s }}
      />
      <TextInput
        value={lastName}
        size={"100%"}
        onChangeText={(text) => setLastName(text)}
        placeholder={"Last Name"}
        style={{ marginBottom: theme.spacing.s }}
      />
      <TextInput
        value={country}
        size={"100%"}
        placeholder={"Country"}
        onChangeText={(text) => setCountry(text)}
        style={{ marginBottom: theme.spacing.s }}
      />
      <TextInput
        value={phoneNumber}
        size={"100%"}
        onChangeText={(text) => setPhoneNumber(text)}
        placeholder={"Contact Phone Number"}
        style={{ marginBottom: theme.spacing.s }}
      />
      <TextInput
        value={email}
        size={"100%"}
        onChangeText={(text: string) => {
          console.log({ text });
        }}
        editable={false}
        placeholder={"Contact Email"}
        style={{ marginBottom: theme.spacing.s }}
      />
    </Box>
  );
};

export default DetailsForm;
