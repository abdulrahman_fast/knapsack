import useEditProfile from "@hooks/Auth/useEditProfile";
import { RouteProp, useRoute } from "@react-navigation/core";
import { BottomButtonBar, Box, useAppTheme } from "components";
import * as React from "react";
import { Image, SafeAreaView, ScrollView, StyleSheet } from "react-native";
import { launchImageLibrary } from "react-native-image-picker";
import CameraIconButton from "./CameraIconButton";
import DetailsForm from "./DetailsForm";

const styles = StyleSheet.create({
  userImage: {
    width: "100%",
    height: "100%",
  },
});

const EditProfile: React.FC = () => {
  const theme = useAppTheme();
  const {
    params: { email, loginId, token },
  } = useRoute<
    RouteProp<
      { EditProfile: { email: string; loginId: string; token: string } },
      "EditProfile"
    >
  >();
  const [
    firstName,
    lastName,
    country,
    phoneNumber,
    image,
    setFirstName,
    setLastName,
    setCountry,
    setPhoneNumber,
    setImage,
    SetProfile,
    loading,
  ] = useEditProfile({ email, loginId, token });
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Box flex={0.5}>
        <Image
          style={styles.userImage}
          resizeMode={"cover"}
          source={
            image
              ? image
              : {
                  uri:
                    "https://jacksimonvineyards.com/wp-content/uploads/2016/05/mplh.jpg",
                }
          }
        />
        <CameraIconButton
          btnAction={() => {
            launchImageLibrary(
              {
                mediaType: "photo",
                includeBase64: false,

                maxHeight: 200,
                maxWidth: 200,
              },
              (response) => {
                setImage(response);
              },
            );
          }}
        />
      </Box>
      <Box
        flex={1}
        borderTopWidth={1}
        paddingHorizontal={"m"}
        borderColor={"borderGrey"}
        justifyContent={"space-between"}
        backgroundColor={"mainBackground"}
        borderTopLeftRadius={theme.spacing.xl}
        borderTopRightRadius={theme.spacing.xl}>
        <ScrollView style={{ flex: 1 }} showsVerticalScrollIndicator={false}>
          <DetailsForm
            {...{
              firstName,
              lastName,
              email,
              country,
              phoneNumber,
              setCountry,
              setLastName,
              setFirstName,
              setPhoneNumber,
            }}
          />
          <Box height={50} />
        </ScrollView>
      </Box>
      <BottomButtonBar
        {...{
          buttonTitle: "SAVE PROFILE",
          btnAction: () => SetProfile(),
          loading,
        }}
      />
    </SafeAreaView>
  );
};

export default EditProfile;
