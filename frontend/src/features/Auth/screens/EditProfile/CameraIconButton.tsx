import * as React from "react";
import { StyleSheet, TouchableOpacity } from "react-native";

import { CustomIcon, makeStyles, useAppTheme } from "components";
import { Theme } from "@constant/Base";

interface CameraIconButtonProps {
  btnAction?: () => void;
}

const useStyles = makeStyles((theme: Theme) =>
  StyleSheet.create({
    btn: {
      width: 40,
      right: 50,
      height: 40,
      bottom: 20,
      elevation: 5,
      borderRadius: 6,
      shadowRadius: 3,
      shadowOpacity: 0.2,
      position: "absolute",
      alignItems: "center",
      backgroundColor: "grey",
      justifyContent: "center",
      shadowColor: theme.colors.shadow,
      shadowOffset: { width: 1, height: 1 },
    },
  }),
);

const CameraIconButton: React.FC<CameraIconButtonProps> = ({ btnAction }) => {
  const theme = useAppTheme();
  const styles = useStyles();
  return (
    <TouchableOpacity style={styles.btn} onPress={btnAction}>
      <CustomIcon
        name="camera"
        size={theme.spacing.m}
        color={theme.colors.mainBackground}
      />
    </TouchableOpacity>
  );
};

export default CameraIconButton;
