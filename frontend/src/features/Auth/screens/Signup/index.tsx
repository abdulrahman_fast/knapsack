import * as React from "react";
import { Box, Button, TextInput } from "components";
import Layout from "@features/Auth/components/Layout";
import useSignUp from "@hooks/Auth/useSignUp";

const Signup: React.FC = () => {
  const [email, setEmail, Send, loading] = useSignUp();

  return (
    <Layout
      {...{
        requireBackBtn: true,
        header: "Enter your email ID",
        description: "We will send you verfication code",
      }}>
      <Box
        flex={1.5}
        flexDirection="row"
        alignItems="center"
        justifyContent="center"
        zIndex={2}>
        <TextInput
          value={email}
          onChangeText={(text) => setEmail(text)}
          placeholder={"Email ID"}
        />
      </Box>
      <Box flex={3} alignItems="center" justifyContent="flex-start" zIndex={2}>
        <Button
          {...{
            size: "100%",
            btnText: "NEXT",
            loading,
            onPress: () => Send(),
          }}
        />
      </Box>
    </Layout>
  );
};

export default Signup;
