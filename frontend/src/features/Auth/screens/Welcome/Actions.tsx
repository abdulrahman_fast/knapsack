import React from "react";
import { useNavigation } from "@react-navigation/native";

import { Box, Button } from "components";

const Actions: React.FC = () => {
  const { navigate } = useNavigation();
  return (
    <Box
      flex={1}
      flexDirection="row"
      justifyContent="center"
      alignItems="center"
      zIndex={2}>
      <Button
        {...{
          size: "40%",
          btnText: "SIGN UP",
          onPress: () => navigate("signup"),
        }}
      />
      <Box width="10%" />
      <Button
        {...{
          size: "40%",
          btnText: "SIGN IN",
          onPress: () => navigate("signin"),
        }}
      />
    </Box>
  );
};

export default Actions;
