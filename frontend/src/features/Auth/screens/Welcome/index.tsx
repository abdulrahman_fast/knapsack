import * as React from "react";
import Actions from "./Actions";
import ConnectPeople from "./ConnectPeople";
import Layout from "@features/Auth/components/Layout";
const Welcome: React.FC<any> = () => {
  return (
    <Layout
      {...{
        header: "Connect with travellers or clients",
        description: "Search in the directory of thousands of peoples",
      }}>
      <ConnectPeople />
      <Actions />
    </Layout>
  );
};

export default Welcome;
