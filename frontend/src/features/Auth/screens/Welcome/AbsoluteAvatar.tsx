import React from 'react';
import {Avatar, Box} from 'components';
import {ImageSourcePropType} from 'react-native';

interface AbsoluteAvatarProps {
  src: ImageSourcePropType;
  size: number;
  top?: number;
  left?: number;
  right?: number;
}

export default function AbsoluteAvatar({
  src,
  size,
  top,
  left,
  right,
}: AbsoluteAvatarProps) {
  return (
    <Box position="absolute" {...{top, left, right}}>
      <Avatar {...{size, src}} />
    </Box>
  );
}
