import React from "react";
import { Box, DashedCircle, Heading } from "components";
import AbsoluteAvatar from "./AbsoluteAvatar";

const TopOffset = 20;

export default function ConnectPeople() {
  return (
    <Box flex={4}>
      <DashedCircle {...{ size: 250, left: -150, top: TopOffset }} />
      <DashedCircle {...{ size: 300, right: -150, top: TopOffset + 20 }} />
      <DashedCircle {...{ size: 600, left: -150, top: TopOffset + 300 }} />
      <AbsoluteAvatar
        {...{
          size: 60,
          left: 30,
          top: TopOffset,
          src: { uri: "https://randomuser.me/api/portraits/women/45.jpg" },
        }}
      />
      <AbsoluteAvatar
        {...{
          size: 60,
          left: 30,
          top: TopOffset + 180,
          src: { uri: "https://randomuser.me/api/portraits/women/55.jpg" },
        }}
      />
      <AbsoluteAvatar
        {...{
          size: 60,
          right: 80,
          top: TopOffset + 20,
          src: { uri: "https://randomuser.me/api/portraits/women/46.jpg" },
        }}
      />
      <AbsoluteAvatar
        {...{
          size: 60,
          right: 0,
          top: TopOffset + 270,
          src: { uri: "https://randomuser.me/api/portraits/women/56.jpg" },
        }}
      />
      <AbsoluteAvatar
        {...{
          size: 60,
          left: 0,
          top: TopOffset + 280,
          src: { uri: "https://randomuser.me/api/portraits/women/26.jpg" },
        }}
      />
      <AbsoluteAvatar
        {...{
          size: 60,
          right: 0,
          top: TopOffset + 340,
          src: { uri: "https://randomuser.me/api/portraits/women/11.jpg" },
        }}
      />
    </Box>
  );
}
