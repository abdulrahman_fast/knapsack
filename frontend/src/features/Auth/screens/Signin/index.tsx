import * as React from "react";
import { SafeAreaView, StyleSheet, TouchableOpacity } from "react-native";
import { useNavigation } from "@react-navigation/native";

import { Theme } from "@constant/Base";
import {
  Box,
  Text,
  Button,
  TextInput,
  IconButton,
  Svg,
  makeStyles,
  useAppTheme,
} from "components";
import SigninHeader from "../../components/SigninHeader";
import SocialSignInBtn from "../../components/SocialSignInBtn";
import useSignIn from "@hooks/Auth/useSignIn";

const useStyles = makeStyles((theme: Theme) =>
  StyleSheet.create({
    backBtn: {
      alignItems: "center",
      justifyContent: "center",
      width: theme.spacing.xxl + 10,
      height: theme.spacing.xxl + 10,
      borderRadius: theme.spacing.xxl + 10 / 2,
      backgroundColor: theme.colors.placeIconBg,
    },
  }),
);

const Signin: React.FC = () => {
  const styles = useStyles();
  const theme = useAppTheme();
  const { navigate } = useNavigation();
  const [email, setEmail, password, setPassword, Login, loading] = useSignIn();

  return (
    <SafeAreaView
      style={{
        flex: 1,
        position: "relative",
        backgroundColor: theme.colors.mainBackground,
      }}>
      <Box
        top={-130}
        left={-210}
        width={500}
        height={500}
        position={"absolute"}>
        <Svg name={"circle"} />
      </Box>
      <Box flex={1} marginHorizontal={"m"}>
        <Box alignItems="center" justifyContent="center" zIndex={2}>
          <SigninHeader />
          <TextInput
            value={email}
            onChangeText={(text) => setEmail(text)}
            placeholder={"Email ID"}
            style={{ marginBottom: theme.spacing.l }}
          />
          <TextInput
            value={password}
            onChangeText={(text) => setPassword(text)}
            secureTextEntry={true}
            placeholder={"Password"}
          />
        </Box>
        <Box flexDirection={"row"} alignSelf={"flex-end"} marginVertical={"l"}>
          <TouchableOpacity>
            <Text variant={"xsmallPrimaryRegular"} color={"textBlack"}>
              Forgot Password?
            </Text>
          </TouchableOpacity>
        </Box>
        <Box alignItems={"center"} justifyContent={"flex-start"}>
          <Button
            {...{
              size: "100%",
              btnText: "SIGN IN",
              loading,
              onPress: () => Login(),
            }}
          />
        </Box>
        <Box marginTop={"xxl"} alignItems={"center"}>
          <Text variant={"xsmallPrimaryRegular"} color={"textPrimary"}>
            Or sign in with you social account
          </Text>
        </Box>

        <Box
          marginVertical={"xl"}
          marginHorizontal={"xl"}
          alignItems={"center"}
          justifyContent={"space-between"}
          flexDirection={"row"}>
          <SocialSignInBtn icon={"fb"} action={() => {}} />
          <SocialSignInBtn icon={"gmail"} action={() => {}} />
        </Box>

        <Box flex={1} bottom={20} position={"absolute"} alignSelf={"center"}>
          <IconButton
            icon={"back"}
            custom={true}
            // @ts-ignore
            style={styles.backBtn}
            size={theme.spacing.m}
            color={theme.colors.iconFill}
            action={() => navigate("welcome")}
          />
        </Box>
      </Box>
      <Box
        zIndex={-1}
        left={-210}
        width={500}
        height={500}
        bottom={-180}
        position={"absolute"}>
        <Svg name={"logo-illustrated"} />
      </Box>
    </SafeAreaView>
  );
};

export default Signin;
