import * as React from "react";
import { Box, Button, TextInput } from "components";
import Layout from "@features/Auth/components/Layout";
import useVerifyCode from "@hooks/Auth/useVerifyCode";
import { RouteProp, useRoute } from "@react-navigation/core";

const VerifyCode: React.FC = () => {
  const {
    params: { email },
  } = useRoute<RouteProp<{ VerifyCode: { email: string } }, "VerifyCode">>();
  const [code, setCode, Send, loading] = useVerifyCode({ email });
  return (
    <Layout
      {...{
        requireBackBtn: true,
        header: "Enter your email ID",
        description: "We will send you verfication code",
      }}>
      <Box
        flex={1.5}
        flexDirection="row"
        alignItems="center"
        justifyContent="center"
        zIndex={2}>
        <TextInput
          value={code}
          onChangeText={(text) => setCode(text)}
          placeholder={"Code"}
        />
      </Box>
      <Box flex={3} alignItems="center" justifyContent="flex-start" zIndex={2}>
        <Button
          {...{
            size: "100%",
            btnText: "Verify",
            loading,
            onPress: () => Send(),
          }}
        />
      </Box>
    </Layout>
  );
};

export default VerifyCode;
