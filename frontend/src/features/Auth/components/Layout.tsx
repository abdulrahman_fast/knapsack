import React from "react";
import { SafeAreaView } from "react-native";

import { Heading, useAppTheme, Box } from "components";
import LogoArea from "@components/LogoArea";

interface LayoutProps {
  children: any;
  header: string;
  description: string;
  requireBackBtn?: boolean;
}

export default function Layout({
  header,
  children,
  description,
  requireBackBtn = false,
}: LayoutProps) {
  const theme = useAppTheme();
  return (
    <SafeAreaView
      style={{
        flex: 1,
        backgroundColor: theme.colors.mainBackground,
      }}>
      <Box flex={1} marginHorizontal={"m"}>
        <LogoArea backBtn={requireBackBtn} />
        <Heading {...{ heading: header, subHeading: description }} />
        {children}
      </Box>
    </SafeAreaView>
  );
}
