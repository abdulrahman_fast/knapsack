import * as React from "react";
import { StyleSheet, TouchableOpacity } from "react-native";

import { CustomIcon, makeStyles, useAppTheme } from "components";
import { Theme } from "@constant/Base";

interface SocialSignInBtnProps {
  icon: string;
  action: () => void;
}

const useStyles = makeStyles((theme: Theme) =>
  StyleSheet.create({
    btn: {
      alignItems: "center",
      justifyContent: "center",
      borderRadius: theme.spacing.xs,
      height: theme.spacing.xxl * 1.5,
      width: theme.spacing.xxl * 3.3,
      backgroundColor: theme.colors.socialBtnLightBlue,
    },
  }),
);

const SocialSignInBtn: React.FC<SocialSignInBtnProps> = ({ icon, action }) => {
  const styles = useStyles();
  const theme = useAppTheme();
  return (
    <TouchableOpacity style={styles.btn} onPress={action}>
      <CustomIcon
        name={icon}
        size={theme.spacing.m}
        color={theme.colors.iconFill}
      />
    </TouchableOpacity>
  );
};

export default SocialSignInBtn;
