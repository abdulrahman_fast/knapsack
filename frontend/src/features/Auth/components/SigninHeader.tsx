import * as React from "react";

import { Box, Text } from "components";
import LogoArea from "@components/LogoArea";

const SigninHeader: React.FC = () => {
  return (
    <Box height={170} marginVertical={"l"} alignItems="center">
      <LogoArea />
      <Text
        marginTop={"xl"}
        marginBottom={"m"}
        color={"textBlack"}
        variant={"mediumPrimaryRegular"}>
        Sign in
      </Text>
      <Text variant={"xsmallPrimaryRegular"} color={"textSecondary"}>
        Sing In with your Email
      </Text>
    </Box>
  );
};

export default SigninHeader;
