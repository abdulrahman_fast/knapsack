import * as React from "react";

import { SafeAreaView } from "react-native";

import { dummyTravels } from "@constant/DummyData";
import SearchListingResult from "@components/SearchListingResult";
import { RouteProp, useRoute } from "@react-navigation/core";
import { ItemProps } from "types";

const SearchList: React.FC = () => {
  const {
    params: { data },
  } = useRoute<
    RouteProp<{ SearchList: { data: ItemProps[] } }, "SearchList">
  >();
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <SearchListingResult {...{ data }} />
    </SafeAreaView>
  );
};
export default SearchList;
