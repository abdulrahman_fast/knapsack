import * as React from "react";
import { useNavigation } from "@react-navigation/native";

import { TabValues } from "types";
import {
  Box,
  Button,
  Dropdown,
  TextInput,
  DatePicker,
  useAppTheme,
} from "components";
import useSearchForm from "@hooks/Home/useSearchForm";

const TravellersLabels = {
  dropdown1: "Travelling From : ",
  dropdown2: "Travelling To : ",
  dropdown3: "Travelling Before : ",
  field1: "Minimum Space Available",
};

const OrderersLabels = {
  dropdown1: "Ordering From : ",
  dropdown2: "Ordering To : ",
  dropdown3: "Travelling Date : ",
  field1: "Maximum Space Available",
};

const SearchForm: React.FC<{
  searchActive: TabValues;
}> = ({ searchActive }) => {
  const theme = useAppTheme();
  const [form, updateForm, Submit, cities] = useSearchForm(searchActive);
  const labels =
    searchActive === "traveller" ? TravellersLabels : OrderersLabels;
  return (
    <>
      <Dropdown
        value={form?.from?.name}
        selectCity={(place_: { id: string; name: string }) =>
          updateForm("from", place_)
        }
        icon={"from"}
        dropdownData={cities}
        requireSearch={true}
        dropdownLabel={labels.dropdown1}
        dopdownSheetlabel={"Select City"}
      />
      <Dropdown
        value={form?.to?.name}
        icon={"to"}
        selectCity={(place_: { id: string; name: string }) =>
          updateForm("to", place_)
        }
        dropdownData={cities}
        requireSearch={true}
        dropdownLabel={labels.dropdown2}
        dopdownSheetlabel={"Select City"}
      />
      <DatePicker
        date={form.date}
        onDateChange={(date) => updateForm("date", date)}
        placeholder={labels.dropdown3}
      />
      <Box marginHorizontal={"m"} marginBottom={"l"}>
        <TextInput
          value={form?.space?.toString()}
          onChangeText={(text: string) =>
            updateForm("space", parseInt(text, 10))
          }
          placeholder={labels.field1}
          style={{ marginBottom: theme.spacing.m }}
        />
        <Button btnText={"SEARCH"} onPress={() => Submit(true)} />
      </Box>
    </>
  );
};
export default SearchForm;
