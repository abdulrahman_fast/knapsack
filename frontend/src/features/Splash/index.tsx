import React from "react";
import { SafeAreaView, Text } from "react-native";

const Splash: React.FC = () => {
  return (
    <SafeAreaView
      style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
      <Text>Splash</Text>
    </SafeAreaView>
  );
};

export default Splash;
