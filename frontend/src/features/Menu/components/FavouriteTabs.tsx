import * as React from "react";
import { StyleSheet, TouchableOpacity } from "react-native";

import { Theme } from "@constant/Base";
import {
  Box,
  Text,
  GradientBox,
  NotificationBadge,
  makeStyles,
  useAppTheme,
} from "components";

interface FavouriteTabsProps {
  active: "orders" | "plans";
  orderCounts: number;
  planCounts: number;
  setActiveTab: React.Dispatch<React.SetStateAction<"orders" | "plans">>;
}

const useStyles = makeStyles((theme: Theme) =>
  StyleSheet.create({
    btn: {
      width: "50%",
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "center",
      height: theme.spacing.xxl,
      backgroundColor: theme.colors.mainBackground,
    },
    btn1: {
      borderTopLeftRadius: theme.spacing.xs,
      borderBottomLeftRadius: theme.spacing.xs,
    },
    btn2: {
      borderTopRightRadius: theme.spacing.xs,
      borderBottomRightRadius: theme.spacing.xs,
    },
    active: {
      backgroundColor: theme.colors.buttonGreen,
    },
  }),
);

const FavouriteTabs: React.FC<FavouriteTabsProps> = ({
  active,
  setActiveTab,
  planCounts,
  orderCounts,
}) => {
  const styles = useStyles();
  const theme = useAppTheme();
  return (
    <GradientBox height={theme.spacing.xxl + 25} opacity={"20"}>
      <Box
        flex={1}
        flexDirection={"row"}
        alignItems={"center"}
        marginHorizontal={"m"}
        justifyContent={"center"}>
        <TouchableOpacity
          style={[
            styles.btn,
            styles.btn1,
            active === "plans" ? styles.active : {},
          ]}
          onPress={() => setActiveTab("plans")}>
          <Text
            marginRight={"xxs"}
            variant={"smallPrimary"}
            color={active === "plans" ? "textWhite" : "textSecondary"}>
            Plans
          </Text>
          <NotificationBadge
            count={planCounts}
            position={"relative"}
            active={active === "plans"}
            size={{ width: theme.spacing.l, height: theme.spacing.gm }}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={[
            styles.btn,
            styles.btn2,
            active === "orders" ? styles.active : {},
          ]}
          onPress={() => setActiveTab("orders")}>
          <Text
            variant={"smallPrimary"}
            color={active === "orders" ? "textWhite" : "textSecondary"}>
            Orders
          </Text>
          <NotificationBadge
            count={orderCounts}
            position={"relative"}
            active={active === "orders"}
            size={{ width: theme.spacing.l, height: theme.spacing.gm }}
          />
        </TouchableOpacity>
      </Box>
    </GradientBox>
  );
};

export default FavouriteTabs;
