import * as React from "react";
import { StyleSheet, TouchableOpacity } from "react-native";
import { useNavigation } from "@react-navigation/native";

import {
  Box,
  Text,
  CustomIcon,
  NotificationBadge,
  makeStyles,
  useAppTheme,
} from "components";
import { Theme } from "@constant/Base";
import useAuth from "@hooks/useAuth";

interface MenuItem {
  label: string;
  toPath?: string;
  justBtn?: boolean;
  requireBadge?: boolean;
}

interface MenuItemProps {
  item: MenuItem;
  last: boolean;
}

const useStyles = makeStyles((theme: Theme) =>
  StyleSheet.create({
    item: {
      justifyContent: "center",
      height: theme.spacing.xxl * 1.35,
      borderBottomColor: theme.colors.borderGrey,
    },
    label: {
      marginRight: theme.spacing.m,
    },
    icon: { transform: [{ rotateY: "180deg" }] },
  }),
);

const MenuItem: React.FC<MenuItemProps> = ({ item, last }) => {
  const { navigate } = useNavigation();
  const theme = useAppTheme();
  const styles = useStyles();
  const { setUserLoggedOut } = useAuth();
  const {
    label = "",
    toPath = "",
    justBtn = false,
    requireBadge = false,
  } = item;

  const navigateTo = () => {
    if (toPath && !justBtn) {
      navigate(toPath);
    } else if (justBtn) {
      // logout
      setUserLoggedOut();
      navigate("Auth");
    }
  };

  return (
    <TouchableOpacity
      style={[
        styles.item,
        { borderBottomWidth: !last ? StyleSheet.hairlineWidth : 0 },
      ]}
      onPress={navigateTo}>
      <Box
        flexDirection={"row"}
        alignItems={"center"}
        marginHorizontal={"gm"}
        justifyContent={"space-between"}>
        <Box flexDirection={"row"} alignItems={"center"}>
          <Text
            style={styles.label}
            variant={"xsmallPrimaryRegular"}
            color={justBtn ? "redTextColor" : "textBlack"}>
            {label}
          </Text>
          {requireBadge ? (
            <NotificationBadge count={"3"} position={"relative"} />
          ) : null}
        </Box>

        {!justBtn ? (
          <CustomIcon
            name="back"
            style={styles.icon}
            size={theme.spacing.s}
            color={theme.colors.black}
          />
        ) : null}
      </Box>
    </TouchableOpacity>
  );
};

export default MenuItem;
