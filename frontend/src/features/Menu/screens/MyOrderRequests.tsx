import * as React from "react";
import { SafeAreaView, StyleSheet } from "react-native";

import theme from "@constant/Base";
import { Box, MenuHeader, HomeList } from "components";
import useMyOrderProvider from "@providers/Menu/MyOrderProvider";
import Loader from "@components/Loader";

const MyOrderRequests: React.FC = () => {
  const [loading, orders] = useMyOrderProvider();
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Box flex={1} backgroundColor={"mainBackground"}>
        <MenuHeader />
        {loading && <Loader />}
        <Box
          flex={1}
          borderColor={"borderGrey"}
          backgroundColor={"appGreyBg"}
          borderTopLeftRadius={theme.spacing.l}
          borderTopRightRadius={theme.spacing.l}
          borderWidth={StyleSheet.hairlineWidth}>
          <HomeList data={orders} secondaryUsage={true} />
        </Box>
      </Box>
    </SafeAreaView>
  );
};

export default MyOrderRequests;
