import * as React from "react";
import { SafeAreaView, StyleSheet } from "react-native";

import theme from "@constant/Base";
import { Box, MenuHeader, HomeList } from "components";
import useMyPlanProvider from "@providers/Menu/MyPlanProvider";
import Loader from "@components/Loader";

const MyTravelPlan: React.FC = () => {
  const [loading, plans] = useMyPlanProvider();

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Box flex={1} backgroundColor={"mainBackground"}>
        <MenuHeader />
        {loading && <Loader />}
        <Box
          flex={1}
          borderColor={"borderGrey"}
          backgroundColor={"appGreyBg"}
          borderTopLeftRadius={theme.spacing.l}
          borderTopRightRadius={theme.spacing.l}
          borderWidth={StyleSheet.hairlineWidth}>
          <HomeList data={plans} secondaryUsage={true} />
        </Box>
      </Box>
    </SafeAreaView>
  );
};

export default MyTravelPlan;
