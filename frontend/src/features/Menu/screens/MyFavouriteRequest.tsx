import * as React from "react";
import { SafeAreaView, StyleSheet } from "react-native";
import { Box, HomeList, MenuHeader, useAppTheme } from "components";
import useMyFavouriteRequestProvider from "@providers/Menu/FavouriteRequestProvider";
import FavouriteTabs from "../components/FavouriteTabs";
import Loader from "@components/Loader";

const MyFavouriteRequest: React.FC = () => {
  const theme = useAppTheme();
  const [
    orderLoading,
    orders,
    refreshOrders,
    planLoading,
    plans,
    refreshPlans,
  ] = useMyFavouriteRequestProvider();

  const [activeTab, setActiveTab] = React.useState<"orders" | "plans">("plans");

  React.useEffect(() => {
    refreshOrders(true);
    refreshPlans(true);
  }, [refreshOrders, refreshPlans]);

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Box flex={1} backgroundColor={"mainBackground"}>
        <MenuHeader />
        {orderLoading || (planLoading && <Loader />)}
        {!orderLoading && !planLoading && (
          <Box
            flex={3}
            borderColor={"borderGrey"}
            backgroundColor={"appGreyBg"}
            borderTopLeftRadius={theme.spacing.l}
            borderTopRightRadius={theme.spacing.l}
            borderWidth={StyleSheet.hairlineWidth}>
            <FavouriteTabs
              orderCounts={orders?.length}
              planCounts={plans?.length}
              active={activeTab}
              setActiveTab={setActiveTab}
            />
            {activeTab === "plans" && (
              <HomeList data={plans} secondaryUsage={true} />
            )}
            {activeTab === "orders" && (
              <HomeList data={orders} secondaryUsage={true} />
            )}
          </Box>
        )}
      </Box>
    </SafeAreaView>
  );
};

export default MyFavouriteRequest;
