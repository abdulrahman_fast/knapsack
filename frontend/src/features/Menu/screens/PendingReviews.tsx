import * as React from "react";
import { SafeAreaView, StyleSheet } from "react-native";

import { dummyTravels } from "@constant/DummyData";
import { Box, MenuHeader, RequestList, useAppTheme } from "components";

const PendingReviews: React.FC = () => {
  const theme = useAppTheme();
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Box flex={1} backgroundColor={"mainBackground"}>
        <MenuHeader />

        <Box
          flex={3}
          borderColor={"borderGrey"}
          backgroundColor={"appGreyBg"}
          borderTopLeftRadius={theme.spacing.l}
          borderTopRightRadius={theme.spacing.l}
          borderWidth={StyleSheet.hairlineWidth}>
          <RequestList data={dummyTravels} />
        </Box>
      </Box>
    </SafeAreaView>
  );
};

export default PendingReviews;
