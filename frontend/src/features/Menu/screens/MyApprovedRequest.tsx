import * as React from "react";
import { SafeAreaView, StyleSheet } from "react-native";
import { Box, MenuHeader, useAppTheme } from "components";
import useMyApprovedRequestProvider from "@providers/Menu/MyApprovedRequestProvider";
import ApprovedList from "@components/ApprovedList";
import Loader from "@components/Loader";

const MyApprovedRequest: React.FC = () => {
  const theme = useAppTheme();
  const [
    loading,
    requests,
    refreshApprovedRequest,
  ] = useMyApprovedRequestProvider();

  React.useEffect(() => {
    refreshApprovedRequest(true);
  }, [refreshApprovedRequest]);
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Box flex={1} backgroundColor={"mainBackground"}>
        <MenuHeader />
        {loading && <Loader />}
        <Box
          flex={3}
          borderColor={"borderGrey"}
          backgroundColor={"appGreyBg"}
          borderTopLeftRadius={theme.spacing.l}
          borderTopRightRadius={theme.spacing.l}
          borderWidth={StyleSheet.hairlineWidth}>
          <ApprovedList data={requests} />
        </Box>
      </Box>
    </SafeAreaView>
  );
};

export default MyApprovedRequest;
