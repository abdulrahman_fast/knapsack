import * as React from "react";
import { SafeAreaView, StyleSheet } from "react-native";
import { Box, MenuHeader, RequestList, useAppTheme } from "components";
import useMyPendingsRequestProvider from "@providers/Menu/MyPendingRequestProvider";
import Loader from "@components/Loader";

const MyPendingRequests: React.FC = () => {
  const theme = useAppTheme();
  const [
    loading,
    requests,
    refreshApprovedRequest,
  ] = useMyPendingsRequestProvider();

  React.useEffect(() => {
    refreshApprovedRequest(true);
  }, [refreshApprovedRequest]);
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Box flex={1} backgroundColor={"mainBackground"}>
        <MenuHeader />
        {loading && <Loader />}
        <Box
          flex={3}
          borderColor={"borderGrey"}
          backgroundColor={"appGreyBg"}
          borderTopLeftRadius={theme.spacing.l}
          borderTopRightRadius={theme.spacing.l}
          borderWidth={StyleSheet.hairlineWidth}>
          <RequestList
            data={requests}
            removeFromRequest={() => refreshApprovedRequest(true)}
          />
        </Box>
      </Box>
    </SafeAreaView>
  );
};

export default MyPendingRequests;
