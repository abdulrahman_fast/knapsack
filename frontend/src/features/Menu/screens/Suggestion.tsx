import * as React from "react";
import { SafeAreaView, StyleSheet } from "react-native";

import { SuggestionTypes } from "@constant/SuggestionTypes";
import {
  Box,
  Button,
  Dropdown,
  TextInput,
  MenuHeader,
  useAppTheme,
} from "components";

const Suggestion: React.FC = () => {
  const theme = useAppTheme();

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Box flex={1} backgroundColor={"mainBackground"}>
        <MenuHeader />

        <Box
          flex={3}
          borderColor={"borderGrey"}
          backgroundColor={"appGreyBg"}
          borderTopLeftRadius={theme.spacing.l}
          borderTopRightRadius={theme.spacing.l}
          borderWidth={StyleSheet.hairlineWidth}>
          <Box marginTop={"m"}>
            <Dropdown
              value={""}
              selectCity={() => {}}
              dropdownData={SuggestionTypes}
              dropdownLabel={"Suggetion Type : "}
              dopdownSheetlabel={"Select Suggestion Type"}
            />
          </Box>
          <Box marginHorizontal={"m"}>
            <TextInput
              value={""}
              onChangeText={() => {}}
              placeholder={"Subject"}
              style={{ marginBottom: theme.spacing.m }}
            />
            <TextInput
              value={""}
              // @ts-ignore
              multiline={true}
              style={{
                height: 150,
                marginBottom: theme.spacing.l,
              }}
              placeholder={"Comment"}
              onChangeText={() => {}}
            />
          </Box>

          <Box
            bottom={40}
            width={"90%"}
            position={"absolute"}
            marginHorizontal={"m"}>
            <Button
              {...{
                onPress: () => {},
                btnText: "SUBMIT",
              }}
            />
          </Box>
        </Box>
      </Box>
    </SafeAreaView>
  );
};

export default Suggestion;
