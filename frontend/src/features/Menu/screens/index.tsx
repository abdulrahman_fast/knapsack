import * as React from "react";
import { SafeAreaView, ScrollView, StyleSheet, FlatList } from "react-native";

import { Box, HomeHeader, makeStyles } from "components";
import MenuItem from "../components/MenuItem";
import { Theme } from "@constant/Base";

const MenuItems = [
  {
    label: "My Travel Plans",
    toPath: "my-travel-plans",
    requireBadge: false,
    justBtn: false,
  },
  {
    label: "My Order Requests",
    toPath: "my-order-requests",
    requireBadge: false,
    justBtn: false,
  },
  {
    label: "Pending Reviews",
    toPath: "pending-reviews",
    requireBadge: true,
    justBtn: false,
  },
  {
    label: "My Approved Requests",
    toPath: "my-approved-requests",
    requireBadge: true,
    justBtn: false,
  },
  {
    label: "Pending Requests",
    toPath: "my-pending-requests",
    requireBadge: true,
    justBtn: false,
  },
  {
    label: "My Favourite Requests",
    toPath: "my-favourite-requests",
    requireBadge: false,
    justBtn: false,
  },
  {
    label: "Settings",
    toPath: "settings",
    requireBadge: false,
    justBtn: false,
  },
  { label: "About", toPath: "about", requireBadge: false, justBtn: false },
  {
    label: "Suggestion",
    toPath: "suggestion",
    requireBadge: false,
    justBtn: false,
  },
  { label: "Logout", toPath: "", requireBadge: false, justBtn: true },
];

const useStyles = makeStyles((theme: Theme) =>
  StyleSheet.create({
    scrollItemsView: {
      marginVertical: theme.spacing.xxl,
    },
  }),
);

const Menu: React.FC = () => {
  const styles = useStyles();
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Box flex={1} backgroundColor={"mainBackground"}>
        <HomeHeader />

        <ScrollView style={styles.scrollItemsView}>
          <FlatList
            data={MenuItems}
            showsVerticalScrollIndicator={false}
            keyExtractor={(_item, idx) => idx.toString()}
            renderItem={({ item, index }) => {
              const isFirst = index === 0;
              return (
                <MenuItem {...{ item, last: index === MenuItems.length - 1 }} />
              );
            }}
          />
        </ScrollView>
      </Box>
    </SafeAreaView>
  );
};

export default Menu;
