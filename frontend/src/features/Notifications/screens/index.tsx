import * as React from "react";
import { SafeAreaView, StyleSheet, FlatList } from "react-native";

import {
  Box,
  MenuHeader,
  useAppTheme,
  EmptyAreaLabel,
  NotificationCard,
} from "components";
import NotificationTypeLabel from "../components/NotificationTypeLabel";

const notifications = [1, 2, 3];

const Notifications: React.FC = () => {
  const theme = useAppTheme();

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Box flex={1} backgroundColor={"mainBackground"}>
        <MenuHeader />
        <Box
          flex={3}
          borderColor={"borderGrey"}
          backgroundColor={"appGreyBg"}
          borderTopLeftRadius={theme.spacing.l}
          borderTopRightRadius={theme.spacing.l}
          borderWidth={StyleSheet.hairlineWidth}>
          <NotificationTypeLabel label={"new"} />
          <FlatList
            data={notifications}
            showsVerticalScrollIndicator={false}
            showsHorizontalScrollIndicator={false}
            ListEmptyComponent={<EmptyAreaLabel label={"No notifications!"} />}
            keyExtractor={(_item, idx) => idx.toString()}
            renderItem={({ item, index }) => {
              return (
                <NotificationCard
                  title="This is title"
                  time="5 Mins ago"
                  details="Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s."
                />
              );
            }}
          />
        </Box>
      </Box>
    </SafeAreaView>
  );
};

export default Notifications;
