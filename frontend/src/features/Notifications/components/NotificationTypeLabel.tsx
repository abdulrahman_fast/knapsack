import * as React from "react";

import { Box, Text } from "components";

const NotificationTypeLabel: React.FC<{ label: "new" | "previous" }> = ({
  label,
}) => {
  return (
    <Box marginHorizontal="m" marginTop="m">
      <Text variant={"xsmallPrimary"} color={"textSecondary"}>
        {label ? label.toLocaleUpperCase() : ""}
      </Text>
    </Box>
  );
};

export default NotificationTypeLabel;
