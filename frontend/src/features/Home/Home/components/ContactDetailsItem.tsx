import * as React from "react";
import { StyleSheet, TouchableOpacity } from "react-native";
import Clipboard from "@react-native-clipboard/clipboard";

import { Box, Text, CustomIcon, useAppTheme } from "components";

interface ContactDetailsItemProps {
  type: "phone" | "email";
  value: string;
}

const ContactDetailsItem: React.FC<ContactDetailsItemProps> = ({
  type,
  value,
}) => {
  const theme = useAppTheme();
  return (
    <Box
      height={100}
      flexDirection={"row"}
      alignItems={"center"}
      justifyContent={"space-between"}
      borderBottomColor={"borderGrey"}
      borderBottomWidth={StyleSheet.hairlineWidth}>
      <Box
        width={50}
        height={50}
        borderRadius={25}
        alignItems={"center"}
        justifyContent={"center"}
        backgroundColor={"borderGrey"}>
        <CustomIcon
          name={type}
          color={theme.colors.buttonGreen}
          size={type === "phone" ? theme.spacing.l : theme.spacing.m}
        />
      </Box>
      <Box width={"60%"}>
        <Text
          color={"textBlack"}
          marginBottom={"xs"}
          textTransform={"capitalize"}
          variant={"smallPrimaryBold"}>
          {type}
        </Text>
        <Text variant={"xsmallPrimaryBold"} color={"textSecondary"}>
          {value || ""}
        </Text>
      </Box>
      <Box width={50} alignItems={"center"} justifyContent={"center"}>
        <TouchableOpacity onPress={() => Clipboard.setString(value)}>
          <CustomIcon
            name="copy"
            size={theme.spacing.l}
            color={theme.colors.iconPrimary}
          />
        </TouchableOpacity>
      </Box>
    </Box>
  );
};

export default ContactDetailsItem;
