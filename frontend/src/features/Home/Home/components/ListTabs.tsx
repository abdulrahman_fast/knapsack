import * as React from "react";
import { StyleSheet, TouchableOpacity } from "react-native";

import { Theme } from "@constant/Base";
import {
  Box,
  Text,
  GradientBox,
  NotificationBadge,
  makeStyles,
  useAppTheme,
} from "components";

interface ListTabsProps {
  active: string;
  requestCounts: number;
  type: "orderer" | "traveller";
  setActiveTab: (name: string) => void;
}

const useStyles = makeStyles((theme: Theme) =>
  StyleSheet.create({
    btn: {
      width: "50%",
      flexDirection: "row",
      alignItems: "center",
      justifyContent: "center",
      height: theme.spacing.xxl,
      backgroundColor: theme.colors.mainBackground,
    },
    btn1: {
      borderTopLeftRadius: theme.spacing.xs,
      borderBottomLeftRadius: theme.spacing.xs,
    },
    btn2: {
      borderTopRightRadius: theme.spacing.xs,
      borderBottomRightRadius: theme.spacing.xs,
    },
    active: {
      backgroundColor: theme.colors.buttonGreen,
    },
  }),
);

const ListTabs: React.FC<ListTabsProps> = ({
  type,
  active,
  setActiveTab,
  requestCounts,
}) => {
  const styles = useStyles();
  const theme = useAppTheme();
  return (
    <GradientBox height={theme.spacing.xxl + 25} opacity={"20"}>
      <Box
        flex={1}
        flexDirection={"row"}
        alignItems={"center"}
        marginHorizontal={"m"}
        justifyContent={"center"}>
        <TouchableOpacity
          style={[
            styles.btn,
            styles.btn1,
            active === "requests" ? styles.active : {},
          ]}
          onPress={() => setActiveTab("requests")}>
          <Text
            marginRight={"xxs"}
            variant={"smallPrimary"}
            color={active === "requests" ? "textWhite" : "textSecondary"}>
            Requests
          </Text>
          <NotificationBadge
            count={requestCounts}
            position={"relative"}
            active={active === "requests"}
            size={{ width: theme.spacing.l, height: theme.spacing.gm }}
          />
        </TouchableOpacity>
        <TouchableOpacity
          style={[
            styles.btn,
            styles.btn2,
            active === "related" ? styles.active : {},
          ]}
          onPress={() => setActiveTab("related")}>
          <Text
            variant={"smallPrimary"}
            color={active === "related" ? "textWhite" : "textSecondary"}>
            Related {type === "traveller" ? "Travel Trips" : "Orders"}
          </Text>
        </TouchableOpacity>
      </Box>
    </GradientBox>
  );
};

export default ListTabs;
