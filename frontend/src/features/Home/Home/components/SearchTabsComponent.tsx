import * as React from "react";

import {
  Box,
  Text,
  Button,
  TextInput,
  SearchTab,
  CustomIcon,
  useAppTheme,
  Dropdown,
} from "components";
import { TabValues } from "types";
import useSearchForm from "@hooks/Home/useSearchForm";

interface SearchTabsComponentProps {
  active: TabValues;
  setSearchActive: (name: TabValues) => void;
}

const SearchTabsComponent: React.FC<SearchTabsComponentProps> = ({
  active,
  setSearchActive,
}) => {
  const theme = useAppTheme();
  const [form, updateForm, Submit, cities] = useSearchForm(active);
  const travllerPlaceholders = {
    from: "DEPARTURE LOCATION",
    to: "ARRIVAL LOCATION",
  };

  const ordererPlaceholders = {
    from: "ORDERING FROM",
    to: "ORDERING TO",
  };

  const labels =
    active === "traveller" ? travllerPlaceholders : ordererPlaceholders;

  return (
    <Box marginHorizontal={"m"} marginBottom={"m"}>
      <Text variant={"mediumPrimaryBold"} color={"textPrimary"}>
        Search
      </Text>
      <Box flexDirection={"row"} justifyContent={"space-between"}>
        <SearchTab
          active={active}
          type={"traveller"}
          setTabActive={setSearchActive}
          label={"I am Traveller\nSearching for Orders"}
        />
        <SearchTab
          active={active}
          type={"orderer"}
          setTabActive={setSearchActive}
          label={"I am Ordering\nSearching for Travellers"}
        />
      </Box>
      {active ? (
        <Box
          width={"100%"}
          borderBottomLeftRadius={theme.spacing.s}
          borderBottomRightRadius={theme.spacing.s}
          backgroundColor={"mainBackground"}>
          <Dropdown
            value={form?.from?.name}
            selectCity={(place_: { id: string; name: string }) =>
              updateForm("from", place_)
            }
            icon={"from"}
            dropdownData={cities}
            requireSearch={true}
            dropdownLabel={labels.from}
            dopdownSheetlabel={"Select City"}
          />
          <Dropdown
            value={form?.to?.name}
            icon={"to"}
            selectCity={(place_: { id: string; name: string }) =>
              updateForm("to", place_)
            }
            dropdownData={cities}
            requireSearch={true}
            dropdownLabel={labels.to}
            dopdownSheetlabel={"Select City"}
          />
          <Box flex={1} alignItems={"center"} marginVertical={"s"}>
            <Button
              size={"80%"}
              btnText={"SEARCH"}
              onPress={() => {
                Submit();
              }}
            />
          </Box>
        </Box>
      ) : null}
    </Box>
  );
};

interface InputRowProps {
  type: "traveller" | "orderer";
  icon: string;
}

const InputRow: React.FC<InputRowProps> = ({ type, icon }) => {
  const theme = useAppTheme();
  const travllerPlaceholders = {
    from: "DEPARTURE LOCATION",
    to: "ARRIVAL LOCATION",
  };

  const ordererPlaceholders = {
    from: "ORDERING FROM",
    to: "ORDERING TO",
  };

  const labels =
    type === "traveller" ? travllerPlaceholders : ordererPlaceholders;

  return (
    <Box alignItems={"flex-end"} flexDirection={"row"} margin={"s"}>
      <Box
        style={{
          width: 40,
          height: 40,
          borderRadius: 2,
          alignItems: "center",
          justifyContent: "center",
          backgroundColor: theme.colors.placeIconBg,
        }}>
        <CustomIcon
          name={icon}
          size={theme.spacing.l}
          color={theme.colors.buttonGreen}
        />
      </Box>
      <Box marginLeft={"xs"} width={"85%"}>
        <TextInput
          value={""}
          noBorder={true}
          onChangeText={() => {}}
          // @ts-ignore
          placeholder={labels[icon]}
        />
      </Box>
    </Box>
  );
};

export default SearchTabsComponent;
