import * as React from "react";

import { TabValues } from "types";
import { Box, Dropdown, TextInput, DatePicker, useAppTheme } from "components";
import { RequestFromParams } from "@hooks/Home/useRequestForm";

const TravellersLabels = {
  dropdown1: "Delivery In : ",
  dropdown2: "Need Before Date : ",
  field1: "Item Name",
  field2: "Approx weight in KG",
  field4: "Buget Per KG",
  field3: "Description",
};

const OrderersLabels = {
  dropdown1: "Travelling From: ",
  dropdown2: "Travel Date: ",
  field1: "",
  field2: "Weight Available",
  field3: "Description",
  field4: "Price/Kg",
};

const RequestOrderForm: React.FC<{
  searchActive: TabValues;
  form: RequestFromParams;
  cities: { id: string; name: string }[];
  updateForm: (
    key: string,
    value: Date | string | { id: string; name: string },
  ) => void;
}> = ({ searchActive, form, cities, updateForm }) => {
  const theme = useAppTheme();
  const labels =
    searchActive === "traveller" ? TravellersLabels : OrderersLabels;

  const { place, price, date, itemName, weight, note } = form;

  return (
    <>
      <Dropdown
        value={place?.name}
        icon={"from"}
        dropdownData={cities}
        requireSearch={true}
        dropdownLabel={labels.dropdown1}
        selectCity={(place_: { id: string; name: string }) =>
          updateForm("place", place_)
        }
        dopdownSheetlabel={"Select City"}
      />
      <DatePicker
        date={date}
        placeholder={labels.dropdown2}
        onDateChange={(date_: Date | string) => {
          updateForm("date", date_);
        }}
      />
      <Box marginHorizontal={"m"} marginBottom={"l"}>
        <TextInput
          value={itemName}
          onChangeText={(text) => {
            updateForm("itemName", text);
          }}
          editable={searchActive === "traveller"}
          placeholder={labels.field1}
          style={{ marginBottom: theme.spacing.m }}
        />
        <Box flex={1} flexDirection={"row"} justifyContent={"space-between"}>
          <TextInput
            value={weight}
            keyboardType="numeric"
            onChangeText={(text) => {
              updateForm("weight", text);
            }}
            placeholder={labels.field2}
            style={{ marginBottom: theme.spacing.m }}
            size={"48%"}
          />
          <TextInput
            value={price}
            size={"48%"}
            keyboardType="numeric"
            onChangeText={(text) => {
              updateForm("price", text);
            }}
            // @ts-ignore
            placeholder={labels?.field4}
            style={{ marginBottom: theme.spacing.m }}
          />
        </Box>
        <TextInput
          value={note}
          // @ts-ignore
          multiline={true}
          style={{
            height: 150,
            marginBottom: theme.spacing.l,
          }}
          placeholder={labels.field3}
          onChangeText={(text) => {
            updateForm("note", text);
          }}
        />
      </Box>
    </>
  );
};
export default RequestOrderForm;
