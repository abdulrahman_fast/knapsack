import * as React from "react";

import { Box, Text } from "components";
import ContactDetailsItem from "./ContactDetailsItem";

interface ContactDetailsSheetProps {
  dopdownSheetlabel: string;
  phone?: string;
  mail?: string;
}

const ContactDetailsSheet: React.FC<ContactDetailsSheetProps> = ({
  dopdownSheetlabel,
  phone,
  mail,
}) => {
  return (
    <Box height={"100%"} padding={"m"} backgroundColor={"mainBackground"}>
      <Box justifyContent={"center"} alignItems={"center"}>
        <Text variant={"mediumPrimaryBold"} color={"textBlack"}>
          {dopdownSheetlabel}
        </Text>
      </Box>

      <Box flex={1} justifyContent={"flex-start"}>
        <ContactDetailsItem type={"phone"} value={phone} />
        <ContactDetailsItem type={"email"} value={mail} />
      </Box>
    </Box>
  );
};

export default ContactDetailsSheet;
