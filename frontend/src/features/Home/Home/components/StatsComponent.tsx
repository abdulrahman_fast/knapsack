import * as React from "react";
import { Box, StatCard } from "components";
import useStatsProvider from "@providers/Home/StatsProvider";
import { useNavigation } from "@react-navigation/core";

const StatsComponent: React.FC = () => {
  const { navigate } = useNavigation();
  const [stats] = useStatsProvider();

  return (
    <Box
      marginVertical={"l"}
      flexDirection={"row"}
      marginHorizontal={"m"}
      justifyContent={"space-between"}>
      <StatCard
        count={stats?.plans}
        type="traveller"
        label="Travellers with space"
        btnAction={() => navigate("explore", { type: "traveller" })}
      />
      <StatCard
        count={stats?.orders}
        type="orderer"
        label="Orders needs to be delivered"
        btnAction={() => navigate("explore", { type: "orderer" })}
      />
    </Box>
  );
};
export default StatsComponent;
