import * as React from "react";

import { SafeAreaView } from "react-native";
import { RouteProp, useRoute } from "@react-navigation/core";
import useExplore from "@hooks/Home/useExplore";
import SearchListingResult from "@components/SearchListingResult";

const Explore: React.FC = () => {
  const {
    params: { type },
  } = useRoute<
    RouteProp<{ Explore: { type: "orderer" | "traveller" } }, "Explore">
  >();
  const [data, loading, refreshing, onRefresh] = useExplore(type);

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <SearchListingResult {...{ data, refreshing, onRefresh, loading }} />
    </SafeAreaView>
  );
};
export default Explore;
