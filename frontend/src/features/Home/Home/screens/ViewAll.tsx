import * as React from "react";
import { SafeAreaView } from "react-native";
import { RouteProp, useRoute } from "@react-navigation/core";
import useViewAll from "@hooks/Home/useViewAll";
import SearchListingResult from "@components/SearchListingResult";

const ViewAll: React.FC = () => {
  const {
    params: { type },
  } = useRoute<
    RouteProp<{ ViewAll: { type: "orderer" | "traveller" } }, "ViewAll">
  >();
  const [data] = useViewAll(type);

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <SearchListingResult {...{ data }} />
    </SafeAreaView>
  );
};

export default ViewAll;
