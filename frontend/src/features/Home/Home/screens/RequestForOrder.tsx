import * as React from "react";
import { SafeAreaView, StyleSheet, ScrollView } from "react-native";

import RequestOrderForm from "../components/RequestOrderForm";
import {
  Box,
  Text,
  HomeHeader,
  BottomButtonBar,
  useAppTheme,
} from "components";
import useRequestForm from "@hooks/Home/useRequestForm";
const RequestForOrder: React.FC = () => {
  const theme = useAppTheme();
  const [form, updateForm, Submit, type, cities, loading] = useRequestForm();

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Box flex={1} backgroundColor={"mainBackground"}>
        <HomeHeader />

        <Box
          flex={3}
          borderColor={"borderGrey"}
          backgroundColor={"appGreyBg"}
          borderTopLeftRadius={theme.spacing.l}
          borderTopRightRadius={theme.spacing.l}
          borderTopWidth={StyleSheet.hairlineWidth}>
          <Box margin={"m"}>
            <Text
              marginTop={"m"}
              marginLeft={"xxs"}
              marginBottom={"xxs"}
              color={"textPrimary"}
              variant={"smallPrimaryBold"}>
              Type of request
            </Text>
          </Box>
          <ScrollView showsVerticalScrollIndicator={false}>
            <RequestOrderForm
              {...{
                searchActive: type,
                form,
                updateForm,
                cities,
              }}
            />
          </ScrollView>
        </Box>
      </Box>
      {/* Added to bring content above bottom bar, Can't give margin or padding */}
      <Box height={60} />
      <BottomButtonBar
        loading={loading}
        buttonTitle={"SEND REQUEST"}
        btnAction={() => {
          Submit();
        }}
      />
    </SafeAreaView>
  );
};

export default RequestForOrder;
