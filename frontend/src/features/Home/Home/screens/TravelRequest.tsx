import * as React from "react";
import { StyleSheet, SafeAreaView, ScrollView } from "react-native";
import { RouteProp, useNavigation, useRoute } from "@react-navigation/native";

import RBSheet from "@components/AsyncRBSheet";
import {
  Box,
  HomeList,
  ReviewList,
  RequestList,
  OrderDetails,
  RequestsHeader,
  BottomButtonBar,
  RatingUsersSheet,
  useAppTheme,
} from "components";
import ListTabs from "../components/ListTabs";
import ContactDetailsSheet from "../components/ContactDetailsSheet";
import usePlanDetailProvider from "@providers/Home/PlanDetailProvider";
import useMyFavouriteRequestProvider from "@providers/Menu/FavouriteRequestProvider";
import Loader from "@components/Loader";
import useSheetTitleAndStatus from "@hooks/Home/useSheetTitleAndStatus";

const TravelRequest: React.FC = () => {
  const { navigate } = useNavigation();
  const theme = useAppTheme();
  const [
    planDetails,
    removeRequestFromPlanDetails,
    _,
    getPlanDetails,
    markRequestAsBooked,
    markRequestAsFavourite,
    loading,
    __,
    bookedLoading,
  ] = usePlanDetailProvider();
  const [
    ___,
    ____,
    refreshOrders,
    _____,
    ______,
    refreshPlans,
  ] = useMyFavouriteRequestProvider();
  const {
    params: { id },
  } = useRoute<RouteProp<{ TravelRequest: { id: string } }, "TravelRequest">>();

  React.useEffect(() => {
    getPlanDetails(id);
  }, [id, getPlanDetails]);

  const [activeTab, setActiveTab] = React.useState("requests");
  const [sheet, setSheet] = React.useState<"contact" | "rate">("contact");
  const sheetRef = React.useRef(null);
  const { myPlan } = planDetails;

  const goToRequestScreen = React.useCallback(() => {
    navigate("request-for-order", { type: "traveller", id: planDetails.id });
  }, [planDetails.id, navigate]);
  const [title, status] = useSheetTitleAndStatus({
    isOwnRequest: myPlan,
    booked: planDetails?.booked,
    isUserHaveRequested: planDetails?.isUserHaveRequested,
    StatusOrderRequest: planDetails?.StatusOrderRequest,
  });

  const SheetAction = () => {
    if (myPlan) {
      if (planDetails.booked) {
        setSheet("rate");
        // @ts-ignore
        sheetRef?.current?.open();
      }
    } else if (
      planDetails.isUserHaveRequested &&
      planDetails.StatusPlanRequest === "approved"
    ) {
      setSheet("contact");
      // @ts-ignore
      sheetRef?.current?.open();
    } else if (!myPlan) {
      goToRequestScreen();
    } else {
      console.log("marked as booked here");
      markRequestAsBooked(planDetails.id);
      setSheet("rate");
    }
  };
  const onFav = () => {
    refreshPlans(true);
    refreshOrders(true);
  };
  return (
    <>
      <Box height={theme.spacing.xxl * 6.25} backgroundColor={"mainBackground"}>
        <RequestsHeader
          type={"traveller"}
          {...{
            myOrder: planDetails.myPlan,
            fromDestination: planDetails?.fromDestination,
            toDestination: planDetails?.toDestination,
            userImage: planDetails?.userImage,
            id: planDetails.id,
            isFav: planDetails.isFav,
            markRequestAsFavourite,
            onFav,
          }}
        />
      </Box>
      <SafeAreaView style={{ flex: 1 }}>
        <Box
          flex={1}
          borderColor={"borderGrey"}
          backgroundColor={"mainBackground"}
          borderTopLeftRadius={theme.spacing.l}
          borderTopRightRadius={theme.spacing.l}
          borderWidth={StyleSheet.hairlineWidth}>
          {loading ? (
            <Loader />
          ) : (
            <ScrollView showsVerticalScrollIndicator={false}>
              <Box flex={1} marginBottom={"xxs"}>
                <Box flex={2}>
                  <OrderDetails
                    type={"traveller"}
                    {...{ orderDetails: planDetails }}
                  />
                </Box>

                <Box flex={3} marginTop={"m"}>
                  {myPlan ? (
                    <>
                      <ListTabs
                        type={"traveller"}
                        active={activeTab}
                        setActiveTab={setActiveTab}
                        requestCounts={planDetails?.requests.length}
                      />
                      {activeTab === "related" ? (
                        <HomeList
                          secondaryUsage={true}
                          data={planDetails?.orders}
                        />
                      ) : (
                        <RequestList
                          data={planDetails?.requests}
                          removeFromRequest={(item) => {
                            removeRequestFromPlanDetails(item);
                          }}
                        />
                      )}
                    </>
                  ) : (
                    <ReviewList data={[1, 2, 3, 4]} title={"Reviews"} />
                  )}
                </Box>
              </Box>

              <Box height={50} />
            </ScrollView>
          )}
        </Box>
        <BottomButtonBar
          btnAction={SheetAction}
          buttonTitle={title}
          loading={bookedLoading}
          status={status}
        />
        <RBSheet
          ref={sheetRef}
          closeOnDragDown
          dragFromTopOnly
          closeOnPressBack
          openDuration={250}
          height={
            sheet === "contact"
              ? theme.spacing.xxl * 7 + 20
              : theme.spacing.xxl * 15 + 20
          }
          customStyles={{
            container: { borderRadius: theme.spacing.l },
            draggableIcon: { backgroundColor: theme.colors.textBlack },
          }}>
          {/* Show contact details of user OR Show rating users dialog */}
          {sheet === "contact" ? (
            <ContactDetailsSheet
              dopdownSheetlabel={"Contact Details"}
              {...{ phone: planDetails.phoneNumber, mail: planDetails.email }}
            />
          ) : (
            <RatingUsersSheet
              {...{
                sheetTitle: "Rate the Traveller",
                sheetSubTitle: "Rate the traveller who brought your order(s).",
                users: [1, 2, 3, 4, 5],
              }}
            />
          )}
        </RBSheet>
      </SafeAreaView>
    </>
  );
};

export default TravelRequest;
