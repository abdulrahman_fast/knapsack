import * as React from "react";
import { SafeAreaView, ScrollView } from "react-native";

import { TabValues } from "types";
import { Box, HomeList, HomeHeader, RateBackModel } from "components";
import StatsComponent from "../components/StatsComponent";
import SearchTabsComponent from "../components/SearchTabsComponent";
import useGetRecentPlan from "@hooks/Home/useGetRecentPlans";
import useGetRecentOrder from "@hooks/Home/useGetRecentOrders";
import { useNavigation } from "@react-navigation/core";
import Loader from "@components/Loader";

const Home: React.FC = () => {
  const scrollViewRef = React.useRef(null);
  const [searchActive, setSearchActive] = React.useState<TabValues>("");
  const [recentPlans, loadingPlans] = useGetRecentPlan();
  const [recentOrders, loadingOrders] = useGetRecentOrder();

  const { navigate } = useNavigation();

  const tabClick = (name: TabValues) => {
    setSearchActive(name);
    setTimeout(() => {
      // @ts-ignore
      scrollViewRef?.current?.scrollToEnd({ animated: true });
    }, 350);
  };

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <ScrollView ref={scrollViewRef} showsVerticalScrollIndicator={false}>
        <HomeHeader screenName={false} />
        <StatsComponent />
        {loadingPlans ? (
          <Loader />
        ) : (
          <HomeList
            data={recentPlans}
            title={"New Travel Requests"}
            viewAllAction={() => navigate("explore", { type: "traveller" })}
          />
        )}

        {loadingOrders ? (
          <Loader />
        ) : (
          <HomeList
            data={recentOrders}
            title={"New Order Requests"}
            viewAllAction={() => navigate("explore", { type: "orderer" })}
          />
        )}

        <SearchTabsComponent active={searchActive} setSearchActive={tabClick} />
        {/* Added to bring content above bottom bar, Can't give margin or padding */}
        <Box height={70} />

        {/* User will only see this dialog when a deal is done and now it's time to rate back */}
        <RateBackModel />
      </ScrollView>
    </SafeAreaView>
  );
};

export default Home;
