import * as React from "react";
import { StyleSheet, SafeAreaView, ScrollView } from "react-native";
import { RouteProp, useNavigation, useRoute } from "@react-navigation/native";

import RBSheet from "@components/AsyncRBSheet";
import {
  Box,
  HomeList,
  ReviewList,
  RequestList,
  OrderDetails,
  RequestsHeader,
  BottomButtonBar,
  useAppTheme,
  RatingUsersSheet,
} from "components";
import ListTabs from "../components/ListTabs";
import ContactDetailsSheet from "../components/ContactDetailsSheet";
import useOrderDetailProvider from "@providers/Home/OrderDetailProvider";
import useMyFavouriteRequestProvider from "@providers/Menu/FavouriteRequestProvider";
import Loader from "@components/Loader";
import useSheetTitleAndStatus from "@hooks/Home/useSheetTitleAndStatus";

const OrderRequest: React.FC = () => {
  const { navigate } = useNavigation();
  const [
    orderDetails,
    removeRequestFromOrderDetails,
    _,
    getOrderDetails,
    markRequestAsBooked,
    markRequestAsFavourite,
    loading,
    __,
    bookedLoading,
  ] = useOrderDetailProvider();

  const {
    params: { id },
  } = useRoute<RouteProp<{ OrderRequest: { id: string } }, "OrderRequest">>();

  React.useEffect(() => {
    getOrderDetails(id);
  }, [id, getOrderDetails]);
  const { myOrder } = orderDetails;

  const [
    ___,
    ____,
    refreshOrders,
    _____,
    ______,
    refreshPlans,
  ] = useMyFavouriteRequestProvider();
  const theme = useAppTheme();
  const [activeTab, setActiveTab] = React.useState("requests");
  const sheetRef = React.useRef(null);
  const [sheet, setSheet] = React.useState<"contact" | "rate">("contact");
  const goToRequestScreen = React.useCallback(() => {
    navigate("request-for-order", { type: "orderer", id: orderDetails.id });
  }, [orderDetails.id, navigate]);

  const [title, status] = useSheetTitleAndStatus(orderDetails);

  const SheetAction = () => {
    if (myOrder) {
      if (orderDetails.booked) {
        setSheet("rate");
        // @ts-ignore
        sheetRef?.current?.open();
      } else {
        console.log("marked as booked here");
        markRequestAsBooked(orderDetails.id);
        setSheet("rate");
      }
    } else if (
      orderDetails.isUserHaveRequested &&
      orderDetails.StatusPlanRequest === "approved"
    ) {
      setSheet("contact");
      // @ts-ignore
      sheetRef?.current?.open();
    } else {
      goToRequestScreen();
    }
  };

  const onFav = () => {
    refreshPlans(true);
    refreshOrders(true);
  };
  return (
    <>
      <Box height={theme.spacing.xxl * 6.25} backgroundColor={"mainBackground"}>
        <RequestsHeader
          type={"orderer"}
          {...{
            myOrder: orderDetails.myOrder,
            fromDestination: orderDetails?.fromDestination,
            toDestination: orderDetails?.toDestination,
            id: orderDetails.id,
            userImage: orderDetails?.userImage,
            isFav: orderDetails.isFav,
            markRequestAsFavourite,
            onFav,
          }}
        />
      </Box>
      <SafeAreaView style={{ flex: 1 }}>
        <Box
          flex={1}
          borderColor={"borderGrey"}
          backgroundColor={"mainBackground"}
          borderTopLeftRadius={theme.spacing.l}
          borderTopRightRadius={theme.spacing.l}
          borderWidth={StyleSheet.hairlineWidth}>
          {loading ? (
            <Loader />
          ) : (
            <ScrollView showsVerticalScrollIndicator={false}>
              <Box flex={1} marginBottom={"xxs"}>
                <Box flex={2}>
                  <OrderDetails type={"orderer"} {...{ orderDetails }} />
                </Box>

                <Box flex={3} marginTop={"m"}>
                  {myOrder === true ? (
                    <>
                      <ListTabs
                        type={"orderer"}
                        requestCounts={orderDetails?.requests.length}
                        active={activeTab}
                        setActiveTab={setActiveTab}
                      />
                      {activeTab === "related" ? (
                        <HomeList
                          data={orderDetails?.relatedTravellers}
                          secondaryUsage={true}
                        />
                      ) : (
                        <RequestList
                          data={orderDetails?.requests}
                          removeFromRequest={(item) => {
                            removeRequestFromOrderDetails(item);
                          }}
                        />
                      )}
                    </>
                  ) : (
                    <ReviewList data={[1, 2, 3, 4]} title={"Reviews"} />
                  )}
                </Box>
              </Box>
              {/* Added to bring content above bottom bar, Can't give margin or padding */}
              <Box height={50} />
            </ScrollView>
          )}
        </Box>

        <BottomButtonBar
          btnAction={SheetAction}
          buttonTitle={title}
          loading={bookedLoading}
          status={status}
        />

        <RBSheet
          ref={sheetRef}
          closeOnDragDown
          dragFromTopOnly
          closeOnPressBack
          openDuration={250}
          height={
            sheet === "contact"
              ? theme.spacing.xxl * 7 + 20
              : theme.spacing.xxl * 15 + 20
          }
          customStyles={{
            container: { borderRadius: theme.spacing.l },
            draggableIcon: { backgroundColor: theme.colors.textBlack },
          }}>
          {sheet === "contact" ? (
            <ContactDetailsSheet
              dopdownSheetlabel={"Contact Details"}
              {...{ phone: orderDetails.phoneNumber, mail: orderDetails.email }}
            />
          ) : (
            <RatingUsersSheet
              {...{
                sheetTitle: "Rate the Traveller",
                sheetSubTitle: "Rate the traveller who brought your order(s).",
                users: [1, 2, 3, 4, 5],
              }}
            />
          )}
        </RBSheet>
      </SafeAreaView>
    </>
  );
};

export default OrderRequest;
