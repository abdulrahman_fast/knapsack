import * as React from "react";

import { TabValues } from "types";
import {
  Box,
  Button,
  Dropdown,
  TextInput,
  DatePicker,
  useAppTheme,
} from "components";
import useGetCities from "@hooks/Home/useCities";
import useNewRequestForm from "@hooks/Request/useNewPlanForm";

const TravellersLabels = {
  dropdown1: "Travelling From : ",
  dropdown2: "Travelling To : ",
  dropdown3: "Travelling Date : ",
  field1: "Item Name",
  field2: "Approx space available in KG",
  field3: "Rate per KG",
  field4: "Details",
};

const OrderersLabels = {
  dropdown1: "Ordering From : ",
  dropdown2: "Ordering To : ",
  dropdown3: "Delivery By : ",
  field1: "Item Name",
  field2: "Approx weight in KG : ",
  field4: "Details",
  field3: "Price in USD : ",
};

const NewPlanForm: React.FC<{
  searchActive: TabValues;
}> = ({ searchActive }) => {
  const theme = useAppTheme();
  const [cities] = useGetCities();
  const [form, UpdateForm, Submit, loading] = useNewRequestForm(searchActive);
  const labels =
    searchActive === "traveller" ? TravellersLabels : OrderersLabels;
  return (
    <>
      <Dropdown
        value={form.fromDestination.name}
        selectCity={(place_: { id: string; name: string }) =>
          UpdateForm("fromDestination", place_)
        }
        icon={"from"}
        dropdownData={cities}
        requireSearch={true}
        dropdownLabel={labels.dropdown1}
        dopdownSheetlabel={"Select City"}
      />
      <Dropdown
        value={form.toDestination.name}
        icon={"to"}
        selectCity={(place_: { id: string; name: string }) =>
          UpdateForm("toDestination", place_)
        }
        dropdownData={cities}
        requireSearch={true}
        dropdownLabel={labels.dropdown2}
        dopdownSheetlabel={"Select City"}
      />
      <DatePicker
        date={form.date}
        placeholder={labels.dropdown3}
        onDateChange={(date_: Date | string) => {
          UpdateForm("date", date_);
        }}
      />

      <Box marginHorizontal={"m"} marginBottom={"l"}>
        {searchActive === "orderer" && (
          <TextInput
            value={form.itemName}
            onChangeText={(text) => {
              UpdateForm("itemName", text);
            }}
            editable={searchActive === "orderer"}
            placeholder={labels.field1}
            style={{ marginBottom: theme.spacing.m }}
          />
        )}
        <Box flex={1} flexDirection={"row"} justifyContent={"space-between"}>
          <TextInput
            value={form.weight}
            keyboardType="numeric"
            onChangeText={(text) => {
              UpdateForm("weight", text);
            }}
            placeholder={labels.field2}
            style={{ marginBottom: theme.spacing.m }}
            size={"48%"}
          />
          <TextInput
            value={form.price}
            size={"48%"}
            keyboardType="numeric"
            onChangeText={(text) => {
              UpdateForm("price", text);
            }}
            // @ts-ignore
            placeholder={labels?.field3}
            style={{ marginBottom: theme.spacing.m }}
          />
        </Box>
        <TextInput
          value={form.note}
          // @ts-ignore
          multiline={true}
          style={{
            height: 150,
            marginBottom: theme.spacing.l,
          }}
          placeholder={labels.field4}
          onChangeText={(text) => {
            UpdateForm("note", text);
          }}
        />
        <Button
          btnText={"SUBMIT"}
          loading={loading}
          onPress={() => {
            Submit();
          }}
        />
      </Box>
    </>
  );
};
export default NewPlanForm;
