import * as React from "react";
import { SafeAreaView, StyleSheet, ScrollView } from "react-native";

import { TabValues } from "types";
import NewRequestForm from "../components/NewPlanForm";
import { Box, Text, SearchTab, HomeHeader, useAppTheme } from "components";

const Request: React.FC = () => {
  const theme = useAppTheme();

  const [tabActive, setTabActive] = React.useState<TabValues>("traveller");

  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Box flex={1} backgroundColor={"mainBackground"}>
        <HomeHeader />

        <Box
          flex={3}
          borderColor={"borderGrey"}
          backgroundColor={"appGreyBg"}
          borderTopLeftRadius={theme.spacing.l}
          borderTopRightRadius={theme.spacing.l}
          borderWidth={StyleSheet.hairlineWidth}>
          <Box margin={"m"}>
            <Text
              marginTop={"m"}
              marginLeft={"xxs"}
              marginBottom={"xxs"}
              color={"textPrimary"}
              variant={"smallPrimaryBold"}>
              Type of request
            </Text>

            <Box flexDirection={"row"}>
              <SearchTab
                type={"traveller"}
                label={"Travelers"}
                active={tabActive}
                secondaryUsage={true}
                setTabActive={setTabActive}
              />
              <SearchTab
                type={"orderer"}
                label={"Orders"}
                active={tabActive}
                secondaryUsage={true}
                setTabActive={setTabActive}
              />
            </Box>
          </Box>
          <ScrollView showsVerticalScrollIndicator={false}>
            <NewRequestForm searchActive={tabActive} />
            {/* Added to bring content above bottom bar, Can't give margin or padding */}
            <Box height={60} />
          </ScrollView>
        </Box>
      </Box>
    </SafeAreaView>
  );
};

export default Request;
