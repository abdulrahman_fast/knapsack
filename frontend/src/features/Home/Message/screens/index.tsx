import * as React from "react";
import { SafeAreaView, StyleSheet } from "react-native";

import { Box, Svg, Text, HomeHeader, useAppTheme } from "components";

const Message: React.FC = () => {
  const theme = useAppTheme();
  return (
    <SafeAreaView style={{ flex: 1 }}>
      <Box flex={1} backgroundColor={"mainBackground"}>
        <HomeHeader />

        <Box
          flex={1}
          alignItems={"center"}
          justifyContent={"center"}
          backgroundColor={"appGreyBg"}
          borderColor={"borderGrey"}
          borderTopLeftRadius={theme.spacing.l}
          borderTopRightRadius={theme.spacing.l}
          borderWidth={StyleSheet.hairlineWidth}>
          <Box height={150} width={150} marginBottom={"xxl"}>
            <Svg name={"message"} />
          </Box>

          <Text
            marginTop={"l"}
            marginBottom={"m"}
            color={"textBlack"}
            variant={"mediumPrimaryBold"}>
            Under Development
          </Text>

          <Text variant={"xsmallPrimaryBold"} color={"textSecondary"}>
            This feature is under development, will be{"\n"} available for your
            use in further releases.
          </Text>
        </Box>
        {/* Added to bring content above bottom bar, Can't give margin or padding */}
        <Box height={60} />
      </Box>
    </SafeAreaView>
  );
};

export default Message;
