import * as React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import { Box } from "components";

import Menu from "@features/Menu/screens";
import Suggestion from "@features/Menu/screens/Suggestion";
import PendingReviews from "@features/Menu/screens/PendingReviews";
import MyTravelPlan from "@features/Menu/screens/MyTravelPlan";
import MyOrderRequests from "@features/Menu/screens/MyOrderRequests";
import MyApprovedRequest from "@features/Menu/screens/MyApprovedRequest";
import MyFavouriteRequest from "@features/Menu/screens/MyFavouriteRequest";
import MyPendingRequests from "@features/Menu/screens/MyPendingRequest";
import TravelRequest from "@features/Home/Home/screens/TravelRequest";
import OrderRequest from "@features/Home/Home/screens/OrderRequest";
import RequestForOrder from "@features/Home/Home/screens/RequestForOrder";

const Stack = createStackNavigator();

const MenuStack: React.FC = () => {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="menu" component={Menu} />
      <Stack.Screen name="my-travel-plans" component={MyTravelPlan} />
      <Stack.Screen name="my-order-requests" component={MyOrderRequests} />
      <Stack.Screen name="travel-request" component={TravelRequest} />
      <Stack.Screen name="order-request" component={OrderRequest} />
      <Stack.Screen name="pending-reviews" component={PendingReviews} />
      <Stack.Screen name="my-approved-requests" component={MyApprovedRequest} />
      <Stack.Screen name="my-pending-requests" component={MyPendingRequests} />
      <Stack.Screen name="request-for-order" component={RequestForOrder} />
      <Stack.Screen
        name="my-favourite-requests"
        component={MyFavouriteRequest}
      />
      <Stack.Screen name="settings" component={Box} />
      <Stack.Screen name="suggestion" component={Suggestion} />
      <Stack.Screen name="about" component={Box} />
    </Stack.Navigator>
  );
};

export default MenuStack;
