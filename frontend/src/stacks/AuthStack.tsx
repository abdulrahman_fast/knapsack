import * as React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import Welcome from "@features/Auth/screens/Welcome";
import Signup from "@features/Auth/screens/Signup";
import Signin from "@features/Auth/screens/Signin";
import SetPassword from "@features/Auth/screens/SetPassword";
import EditProfile from "@features/Auth/screens/EditProfile";
import VerifyCode from "@features/Auth/screens/VerifyCode";

const AuthStack = createStackNavigator();

const AuthStackNavigator: React.FC = () => {
  return (
    <AuthStack.Navigator initialRouteName="welcome" headerMode="none">
      <AuthStack.Screen name="welcome" component={Welcome} />
      <AuthStack.Screen name="signup" component={Signup} />
      <AuthStack.Screen name="signin" component={Signin} />
      <AuthStack.Screen name="verifyCode" component={VerifyCode} />
      <AuthStack.Screen name="setpassword" component={SetPassword} />
      <AuthStack.Screen name="editprofile" component={EditProfile} />
    </AuthStack.Navigator>
  );
};

export default AuthStackNavigator;
