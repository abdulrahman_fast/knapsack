import * as React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import Search from "@features/Search/screens";
import SearchList from "@features/Search/screens/SearchList";

const Stack = createStackNavigator();

const SearchStack: React.FC = () => {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="search" component={Search} />
      <Stack.Screen name="search-list" component={SearchList} />
    </Stack.Navigator>
  );
};

export default SearchStack;
