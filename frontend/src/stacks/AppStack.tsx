import * as React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";

import { BottomTabs } from "components";

// Stacks
import HomeStack from "@stacks/HomeStack";
import Search from "@stacks/SearchStack";
import Menu from "@stacks/MenuStack";

// Screens
import Request from "@features/Home/Request/screens";
import Message from "@features/Home/Message/screens";
import Notifications from "@features/Notifications/screens";
import { StatsProvider } from "@providers/Home/StatsProvider";
import { MyOrderProvider } from "@providers/Menu/MyOrderProvider";
import { MyPlansProvider } from "@providers/Menu/MyPlanProvider";
import { MyApprovedRequestProvider } from "@providers/Menu/MyApprovedRequestProvider";
import { MyPendingsRequestProvider } from "@providers/Menu/MyPendingRequestProvider";
import { OrderDetailProvider } from "@providers/Home/OrderDetailProvider";
import { PlanDetailProvider } from "@providers/Home/PlanDetailProvider";
import { MyFavouriteRequestProvider } from "@providers/Menu/FavouriteRequestProvider";

const Tab = createBottomTabNavigator();

const ApplicationStackNavigator: React.FC = () => {
  return (
    <StatsProvider>
      <MyPlansProvider>
        <MyOrderProvider>
          <OrderDetailProvider>
            <PlanDetailProvider>
              <MyApprovedRequestProvider>
                <MyPendingsRequestProvider>
                  <MyFavouriteRequestProvider>
                    <Tab.Navigator
                      tabBar={(props) => <BottomTabs {...props} />}>
                      <Tab.Screen name="home" component={HomeStack} />
                      <Tab.Screen name="search" component={Search} />
                      <Tab.Screen name="new-request" component={Request} />
                      <Tab.Screen name="messages" component={Message} />
                      <Tab.Screen name="menu" component={Menu} />
                      <Tab.Screen
                        name="notifications"
                        component={Notifications}
                      />
                    </Tab.Navigator>
                  </MyFavouriteRequestProvider>
                </MyPendingsRequestProvider>
              </MyApprovedRequestProvider>
            </PlanDetailProvider>
          </OrderDetailProvider>
        </MyOrderProvider>
      </MyPlansProvider>
    </StatsProvider>
  );
};

export default ApplicationStackNavigator;
