import * as React from "react";
import { createStackNavigator } from "@react-navigation/stack";

import Home from "@features/Home/Home/screens";
import TravelRequest from "@features/Home/Home/screens/TravelRequest";
import OrderRequest from "@features/Home/Home/screens/OrderRequest";
import RequestForOrder from "@features/Home/Home/screens/RequestForOrder";
import Explore from "@features/Home/Home/screens/Explore";
import ViewAll from "@features/Home/Home/screens/ViewAll";

const Stack = createStackNavigator();

const HomeStack: React.FC = () => {
  return (
    <Stack.Navigator screenOptions={{ headerShown: false }}>
      <Stack.Screen name="home" component={Home} />
      <Stack.Screen name="travel-request" component={TravelRequest} />
      <Stack.Screen name="order-request" component={OrderRequest} />
      <Stack.Screen name="request-for-order" component={RequestForOrder} />
      <Stack.Screen name="explore" component={Explore} />
      <Stack.Screen name="view-all" component={ViewAll} />
    </Stack.Navigator>
  );
};

export default HomeStack;
