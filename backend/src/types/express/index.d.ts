import { Document, Model } from 'mongoose';
import { IFile } from '../../interfaces/IFile';
import { IOrder } from '../../interfaces/IOrder';
import { IProfile } from '../../interfaces/IProfile';
import { IPlan } from '../../interfaces/IPlan';
import { IToken } from '../../interfaces/IToken';
import { IUser } from '../../interfaces/IUser';
import { IRequest } from '../../interfaces/IRequest';
import { IAirport } from '../../interfaces/IAirport';
import { IOffer } from '../../interfaces/IOffer';
import { IReview } from '../../interfaces/IReview';
declare global {
  namespace Express {
    export interface Request {
      currentUser: { user: IUser & Document; profile: IProfile & Document };
    }
  }

  namespace Models {
    export type UserModel = Model<IUser & Document>;
    export type TokenModel = Model<IToken & Document>;
    export type ProfileModel = Model<IProfile & Document>;
    export type PlanModel = Model<IPlan & Document>;
    export type OrderModel = Model<IOrder & Document>;
    export type FileModel = Model<IFile & Document>;
    export type RequestModel = Model<IRequest & Document>;
    export type AirportModel = Model<IAirport & Document>;
    export type OfferModel = Model<IOffer & Document>;
    export type ReviewModel = Model<IReview & Document>;
  }
}
