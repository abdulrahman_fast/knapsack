import { Router } from 'express';
import auth from './routes/auth';
import user from './routes/user';
import agendash from './routes/agendash';
import profile from './routes/user';
import plan from './routes/plan';
import order from './routes/order';
import dashboard from './routes/dashboard';
import airport from './routes/airport';
import request from './routes/request';
import review from './routes/reviews';
import offer from './routes/offer';

// guaranteed to get dependencies
export default () => {
  const app = Router();
  auth(app);
  user(app);
  agendash(app);
  profile(app);
  plan(app);
  order(app);
  airport(app);
  dashboard(app);
  request(app);
  review(app);
  offer(app);
  return app;
};
