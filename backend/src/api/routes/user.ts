import { celebrate, Joi } from 'celebrate';
import { Router, Request, Response, NextFunction } from 'express';
import { Logger } from 'winston';
import { Container } from 'typedi';
import ProfileService from '../../services/profile';
import middlewares from '../middlewares';
import { IProfileInputDTO } from '../../interfaces/IProfile';
import multer from 'multer';
import path from 'path';

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, 'src/uploads/');
  },
  filename: (req, file, cb) => {
    cb(null, Date.now() + path.extname(file.originalname)); // Appending extension
  },
});

const upload = multer({ storage });
const route = Router();

export default (app: Router) => {
  app.use('/users', route);

  route.get('/me', middlewares.isAuth, middlewares.attachCurrentUser, (req: Request, res: Response) => {
    return res.json({ user: req.currentUser }).status(200);
  });

  route.post(
    '/profile',
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    upload.single('file'),
    celebrate({
      body: Joi.object({
        firstName: Joi.string().required(),
        lastName: Joi.string().required(),
        contactEmail: Joi.string().required(),
        contactPhone: Joi.string().required(),
        country: Joi.string().required(),
        loginId: Joi.string().required(),
      }),
    }),
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get('logger');
      logger.debug('Calling Sign-Up endpoint with body: %o', req.body);
      try {
        const prorfileServiceInstance = Container.get(ProfileService);
        const { profile } = await prorfileServiceInstance.SetProfile(req.body as IProfileInputDTO, req.file);
        return res.status(201).json({ profile });
      } catch (e) {
        logger.error('🔥 error: %o', e);
        return next(e);
      }
    },
  );
};
