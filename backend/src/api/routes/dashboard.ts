import { Router, Request, Response, NextFunction } from 'express';
import { Container } from 'typedi';
import middlewares from '../middlewares';
import { Logger } from 'winston';
import DashboardService from '../../services/dashboard';
const route = Router();

export default (app: Router) => {
  app.use('/dashboard', route);

  route.get(
    '/stats',
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get('logger');
      logger.debug('Calling Stats endpoint with body: %o', req.body);
      try {
        const dashboardServiceInstance = Container.get(DashboardService);
        const {
          stats: { plans, orders },
        } = await dashboardServiceInstance.GetStats();
        return res.status(200).json({ plans, orders });
      } catch (e) {
        logger.error('🔥 error: %o', e);
        return next(e);
      }
    },
  );
};
