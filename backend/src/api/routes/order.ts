import { celebrate, Joi } from 'celebrate';
import { Router, Request, Response, NextFunction } from 'express';
import { Logger } from 'winston';
import { Container } from 'typedi';
import middlewares from '../middlewares';
import OrderService from '../../services/order';
import { IOrderInputDTO } from '../../interfaces/IOrder';
import RequestService from '../../services/request';
import PlanService from '../../services/plan';
import ProfileService from '../../services/profile';
import ReviewService from '../../services/review';
const route = Router();

export default (app: Router) => {
  app.use('/order', route);
  route.post(
    '/create',
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    celebrate({
      body: Joi.object({
        from: Joi.string().required(),
        to: Joi.string().required(),
        date: Joi.date().required(),
        itemName: Joi.string().required(),
        weight: Joi.number().required(),
        price: Joi.number().required(),
        note: Joi.string().required(),
      }),
    }),
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get('logger');
      logger.debug('Calling Sign-Up endpoint with body: %o', req.body);
      try {
        const orderServiceInstance = Container.get(OrderService);
        const { order } = await orderServiceInstance.CreateOrder({
          ...req.body,
          profileId: req.currentUser.profile._id,
        } as IOrderInputDTO);
        return res.status(201).json({ order });
      } catch (e) {
        logger.error('🔥 error: %o', e);
        return next(e);
      }
    },
  );

  route.get(
    '/',
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get('logger');
      logger.debug('Calling Sign-Up endpoint with body: %o', req.body);
      try {
        const orderServiceInstance = Container.get(OrderService);
        const { Data } = await orderServiceInstance.GetOrder();
        return res.status(200).json({ Data });
      } catch (e) {
        logger.error('🔥 error: %o', e);
        return next(e);
      }
    },
  );

  route.get(
    '/byId/:id',
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get('logger');
      logger.debug('Calling Sign-Up endpoint with body: %o', req.body);
      try {
        const requestServiceInstance = Container.get(RequestService);
        const orderServiceInstance = Container.get(OrderService);
        const planServiceInstance = Container.get(PlanService);
        const profileServiceInstance = Container.get(ProfileService);


        const { order } = await orderServiceInstance.GetOrderById(req.params.id);
        const isUserHaveRequested = await requestServiceInstance.isUserHaveRequested(
          order.id,
          req.currentUser.profile._id,
        );
        let StatusOrderRequest = null;
        if (isUserHaveRequested) {
          StatusOrderRequest = await requestServiceInstance.getStatusOrderRequest(
            order.id,
            req.currentUser.profile._id,
          );
        }
        const { Requests } =
          order.profileId !== req.currentUser.profile._id
            ? await requestServiceInstance.GetRequestByOrderId(order.id)
            : { Requests: [] };

        const RelatedTravellers = await planServiceInstance.getRelatedTravellers(order);
        const { UserIds } = await requestServiceInstance.GetApprovedUserByOrderId(req.params.id);
        const {Users} = await profileServiceInstance.getUsersByIds(UserIds);
        const {Order} = await orderServiceInstance.GetReviewsByOrderId(req.params.id);
        const reviews = Users.map(x => {
          const review = Order.reviews.find(y => y.toUserId === x._id);
          return {
            ...x,
            review
          }
        })
        console.log({reviews});
        
        return res.status(200).json({
          order,
          isUserHaveRequested,
          StatusOrderRequest,
          requests: Requests,
          RelatedTravellers,
          reviews,
          isFav: req.currentUser.profile.favouriteOrders.includes(req.params.id),
        });
      } catch (e) {
        logger.error('🔥 error: %o', e);
        return next(e);
      }
    },
  );

  route.get(
    '/myorders',
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get('logger');
      logger.debug('Calling Sign-Up endpoint with body: %o', req.body);
      try {
        const orderServiceInstance = Container.get(OrderService);
        const { Orders } = await orderServiceInstance.GetMyOrders(req.currentUser.profile._id);
        return res.status(201).json({ Orders });
      } catch (e) {
        logger.error('🔥 error: %o', e);
        return next(e);
      }
    },
  );

  route.get(
    '/recent',
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get('logger');
      logger.debug('Calling Recent ORder endpoint with body: %o', req.body);
      try {
        const orderServiceInstance = Container.get(OrderService);
        const { Orders } = await orderServiceInstance.GetRecentOrders(req.currentUser.profile._id);
        return res.status(200).json({ Orders });
      } catch (e) {
        logger.error('🔥 error: %o', e);
        return next(e);
      }
    },
  );
  route.post(
    '/search',
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get('logger');
      logger.debug('Calling Recent ORder endpoint with body: %o', req.body);
      try {
        const orderServiceInstance = Container.get(OrderService);
        const { Orders } = await orderServiceInstance.Search({ ...req.body, id: req.currentUser.profile._id });
        console.log({ Orders });
        return res.status(200).json({ Data: Orders });
      } catch (e) {
        logger.error('🔥 error: %o', e);
        return next(e);
      }
    },
  );

  route.post(
    '/favourite',
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get('logger');
      logger.debug('Calling Recent Plans endpoint with body: %o', req.body);
      try {
        const profileServiceInstance = Container.get(ProfileService);
        if (req.currentUser.profile.favouriteOrders.includes(req.body.id)) {
          const { Update } = await profileServiceInstance.RemoveFavouriteOrder({
            id: req.body.id,
            userId: req.currentUser.profile._id,
          });
          return res.status(201).json({ Update: false });
        } else {
          const { Update } = await profileServiceInstance.AddFavouriteOrder({
            id: req.body.id,
            userId: req.currentUser.profile._id,
          });
          return res.status(201).json({ Update: true });
        }
      } catch (e) {
        logger.error('🔥 error: %o', e);
        return next(e);
      }
    },
  );

  route.get(
    '/favourite',
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get('logger');
      logger.debug('Calling Recent Plans endpoint with body: %o', req.body);
      try {
        const orderServiceInstance = Container.get(OrderService);
        const { Orders } = await orderServiceInstance.GetFavouriteOrders(req.currentUser.profile.favouriteOrders);
        return res.status(201).json({ Orders });
      } catch (e) {
        logger.error('🔥 error: %o', e);
        return next(e);
      }
    },
  );

  // route.get(
  //   '/userToReview/:id',
  //   middlewares.isAuth,
  //   middlewares.attachCurrentUser,
  //   async (req: Request, res: Response, next: NextFunction) => {
  //     const logger: Logger = Container.get('logger');
  //     logger.debug('Calling Recent Plans endpoint with body: %o', req.body);
  //     try {
  //       const requestServiceInstance = Container.get(RequestService);
  //       const orderServiceInstance = Container.get(OrderService);
  //       const profileServiceInstance = Container.get(ProfileService);
  //       const { UserIds } = await requestServiceInstance.GetApprovedUserByOrderId(req.params.id);
  //       const {Users} = await profileServiceInstance.getUsersByIds(UserIds);
  //       const {Orders} = await orderServiceInstance.GetReviewsByOrderId(req.params.id);
  //       return res.status(201).json({ Users, Orders });
  //     } catch (e) {
  //       logger.error('🔥 error: %o', e);
  //       return next(e);
  //     }
  //   },
  // );

  route.put(
    '/markfull/:orderId',
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get('logger');
      logger.debug('Calling Marked Booked endpoint with body: %o', req.body);
      try {
        const orderServiceInstance = Container.get(OrderService);
        const { Update } = await orderServiceInstance.MarkedBook(req.params.orderId);
        return res.status(201).json({ Update });
      } catch (e) {
        logger.error('🔥 error: %o', e);
        return next(e);
      }
    },
  );
};
