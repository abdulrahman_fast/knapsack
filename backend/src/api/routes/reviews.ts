import { Router, Request, Response, NextFunction } from 'express';
import { Logger } from 'winston';
import { Container } from 'typedi';
import middlewares from '../middlewares';
import ReviewService from '../../services/review';
import OrderService from '../../services/order';
import PlanService from '../../services/plan';
const route = Router();

export default (app: Router) => {
  app.use('/reviews', route);

  route.get(
    '/',
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get('logger');
      logger.debug('Calling Sign-Up endpoint with body: %o', req.body);
      try {
        const reviewServiceInstance = Container.get(ReviewService);
        const { reviews } = await reviewServiceInstance.GetReviews();
        return res.status(200).json({ reviews });
      } catch (e) {
        logger.error('🔥 error: %o', e);
        return next(e);
      }
    },
  );

  route.post(
    '/orderReview/:id',
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get('logger');
      logger.debug('Calling Sign-Up endpoint with body: %o', req.body);
      try {
        const reviewServiceInstance = Container.get(ReviewService);
        const orderServiceInstance = Container.get(OrderService);
        const { review } = await reviewServiceInstance.RateUser(req.body);
        const UpdateRecord = await orderServiceInstance.AddReview(req.params.id, review._id);
        return res.status(200).json({ review });
      } catch (e) {
        logger.error('🔥 error: %o', e);
        return next(e);
      }
    },
  );
  route.post(
    '/planReview/:id',
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get('logger');
      logger.debug('Calling Sign-Up endpoint with body: %o', req.body);
      try {
        const reviewServiceInstance = Container.get(ReviewService);
        const planServiceInstance = Container.get(PlanService);
        const { review } = await reviewServiceInstance.RateUser(req.body);
        const UpdateRecord = await planServiceInstance.AddReview(req.params.id, review._id);
        return res.status(200).json({ review });
      } catch (e) {
        logger.error('🔥 error: %o', e);
        return next(e);
      }
    },
  );
};
