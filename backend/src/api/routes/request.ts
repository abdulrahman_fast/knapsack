import { celebrate, Joi } from 'celebrate';
import { Router, Request, Response, NextFunction } from 'express';
import { Logger } from 'winston';
import { Container } from 'typedi';
import middlewares from '../middlewares';
import { IRequestInputDTO } from '../../interfaces/IRequest';
import RequestService from '../../services/request';
import PlanService from '../../services/plan';
import OrderService from '../../services/order';
const route = Router();

export default (app: Router) => {
  app.use('/request', route);
  route.post(
    '/create',
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    celebrate({
      body: Joi.object({
        fromUserId: Joi.string().required(),
        status: Joi.string().required(),
        note: Joi.string().required(),
        place: Joi.string().required(),
        date: Joi.string().required(),
        weight: Joi.number().required(),
        price: Joi.number().required(),
        itemName: Joi.string(),
        orderId: Joi.any(),
        planId: Joi.any(),
      }),
    }),
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get('logger');
      logger.debug('Calling Sign-Up endpoint with body: %o', req.body);
      try {
        const requestServiceInstance = Container.get(RequestService);
        const { Request } = await requestServiceInstance.CreateRequest(req.body as IRequestInputDTO);
        return res.status(200).json({ Request });
      } catch (e) {
        logger.error('🔥 error: %o', e);
        return next(e);
      }
    },
  );

  route.post(
    '/mark',
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    celebrate({
      body: Joi.object({
        status: Joi.boolean().required(),
        requestId: Joi.string().required(),
      }),
    }),
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get('logger');
      logger.debug('Calling Sign-Up endpoint with body: %o', req.body);
      try {
        const requestServiceInstance = Container.get(RequestService);
        const { Request } = await requestServiceInstance.MarkRequest(
          req.body as { status: boolean; requestId: string },
        );
        return res.status(201).json({ Request });
      } catch (e) {
        logger.error('🔥 error: %o', e);
        return next(e);
      }
    },
  );

  route.get(
    '/',
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get('logger');
      logger.debug('Calling Sign-Up endpoint with body: %o', req.body);
      try {
        const requestServiceInstance = Container.get(RequestService);
        const { Requests } = await requestServiceInstance.GetRequests();
        return res.status(201).json({ Requests });
      } catch (e) {
        logger.error('🔥 error: %o', e);
        return next(e);
      }
    },
  );

  route.get(
    '/mypendingrequest',
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get('logger');
      logger.debug('Calling My Pending Request endpoint with body: %o', req.body);
      try {
        const requestServiceInstance = Container.get(RequestService);
        const planServiceInstance = Container.get(PlanService);
        const orderServiceInstance = Container.get(OrderService);
        const planIds = await planServiceInstance.getPlanIdsOfUser(req.currentUser.profile._id);
        const orderIds = await orderServiceInstance.getOrderIdsOfUser(req.currentUser.profile._id);
        const { Requests } = await requestServiceInstance.GetMyPendingRequest(planIds, orderIds);
        return res.status(201).json({ Requests });
      } catch (e) {
        logger.error('🔥 error: %o', e);
        return next(e);
      }
    },
  );
  route.get(
    '/myapprovedrequest',
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get('logger');
      logger.debug('Calling Sign-Up endpoint with body: %o', req.body);
      try {
        const requestServiceInstance = Container.get(RequestService);
        // const planServiceInstance = Container.get(PlanService);
        // const orderServiceInstance = Container.get(OrderService);
        const { Requests } = await requestServiceInstance.GetMyApprovedRequest(req.currentUser.profile._id);
        return res.status(201).json({ Requests });
      } catch (e) {
        logger.error('🔥 error: %o', e);
        return next(e);
      }
    },
  );
};
