import { celebrate, Joi } from 'celebrate';
import { Router, Request, Response, NextFunction } from 'express';
import { Logger } from 'winston';
import { Container } from 'typedi';
import middlewares from '../middlewares';
import { IPlanInputDTO, IPlanResponseDTO } from '../../interfaces/IPlan';
import PlanService from '../../services/plan';
import RequestService from '../../services/request';
import OrderService from '../../services/order';
import ProfileService from '../../services/profile';
const route = Router();

export default (app: Router) => {
  app.use('/plan', route);
  route.post(
    '/create',
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    celebrate({
      body: Joi.object({
        from: Joi.string().required(),
        to: Joi.string().required(),
        date: Joi.date().required(),
        itemName: Joi.any(),
        space: Joi.number().required(),
        price: Joi.number().required(),
        note: Joi.string().required(),
      }),
    }),
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get('logger');
      logger.debug('Calling Sign-Up endpoint with body: %o', req.body);
      try {
        const planServiceInstance = Container.get(PlanService);
        const { Plan } = await planServiceInstance.CreatePlan({
          ...req.body,
          profileId: req.currentUser.profile._id,
        } as IPlanInputDTO);
        return res.status(201).json({ Plan });
      } catch (e) {
        logger.error('🔥 error: %o', e);
        return next(e);
      }
    },
  );

  route.get(
    '/',
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get('logger');
      logger.debug('Calling Sign-Up endpoint with body: %o', req.body);
      try {
        const planServiceInstance = Container.get(PlanService);
        const { Data } = await planServiceInstance.GetPlan();
        return res.status(201).json({ Data });
      } catch (e) {
        logger.error('🔥 error: %o', e);
        return next(e);
      }
    },
  );

  route.get(
    '/byId/:id',
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get('logger');
      logger.debug('Calling Sign-Up endpoint with body: %o', req.body);
      try {
        const planServiceInstance = Container.get(PlanService);
        const requestServiceInstance = Container.get(RequestService);
        const orderServiceInstance = Container.get(OrderService);
        const profileServiceInstance = Container.get(ProfileService)
        const { plan } = await planServiceInstance.GetPlanById(req.params.id);
        const isUserHaveRequested = await requestServiceInstance.isUserHavePlanRequested(
          plan.id,
          req.currentUser.profile._id,
        );
        let StatusPlanRequest = null;
        if (isUserHaveRequested) {
          StatusPlanRequest = await requestServiceInstance.getStatusPlanRequest(plan.id, req.currentUser.profile._id);
        }
        const { Requests } =
          plan.profileId !== req.currentUser.profile._id
            ? await requestServiceInstance.GetRequestByPlanId(req.params.id)
            : { Requests: [] };
        const RelatedOrders = await orderServiceInstance.getRelatedOrders(plan);
        const { UserIds } = await requestServiceInstance.GetApprovedUserByPlanId(req.params.id);
        const { Users } = await profileServiceInstance.getUsersByIds(UserIds);
        const { Plan } = await planServiceInstance.GetReviewsByPlanId(req.params.id);
        const reviews = Users.map(x => {
          const review = Plan.reviews.find(y => y.toUserId === x._id);
          return {
            ...x,
            review
          }
        });

        console.log({reviews});
        return res
          .status(200)
          .json({
            plan,
            requests: Requests,
            StatusPlanRequest,
            isUserHaveRequested,
            RelatedOrders,
            reviews,
            isFav: req.currentUser.profile.favouritePlan.includes(req.params.id)
          });
      } catch (e) {
        logger.error('🔥 error: %o', e);
        return next(e);
      }
    },
  );

  route.get(
    '/myplans',
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get('logger');
      logger.debug('Calling Sign-Up endpoint with body: %o', req.body);
      try {
        const planServiceInstance = Container.get(PlanService);
        const { Plans } = await planServiceInstance.GetMyPlans(req.currentUser.profile._id);
        return res.status(201).json({ Plans });
      } catch (e) {
        logger.error('🔥 error: %o', e);
        return next(e);
      }
    },
  );
  route.get(
    '/recent',
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get('logger');
      logger.debug('Calling Recent Plans endpoint with body: %o', req.body);
      try {
        const planServiceInstance = Container.get(PlanService);
        const { Plans } = await planServiceInstance.GetRecentPlans(req.currentUser.profile._id);
        return res.status(201).json({ Plans });
      } catch (e) {
        logger.error('🔥 error: %o', e);
        return next(e);
      }
    },
  );
  route.post(
    '/search',
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get('logger');
      logger.debug('Calling Recent Plans endpoint with body: %o', req.body);
      try {
        const planServiceInstance = Container.get(PlanService);
        const { Plans } = await planServiceInstance.Search({ ...req.body, id: req.currentUser.profile._id });
        console.log({ Plans });
        return res.status(201).json({ Data: Plans });
      } catch (e) {
        logger.error('🔥 error: %o', e);
        return next(e);
      }
    },
  );

  route.post(
    '/favourite',
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get('logger');
      logger.debug('Calling Recent Plans endpoint with body: %o', req.body);
      try {
        const profileServiceInstance = Container.get(ProfileService);
        if (req.currentUser.profile.favouritePlan.includes(req.body.id)) {
          const { Update } = await profileServiceInstance.RemoveFavouritePlan({ id: req.body.id, userId: req.currentUser.profile._id });
          return res.status(201).json({ Update: false });
        } else {
          const { Update } = await profileServiceInstance.AddFavouritePlan({ id: req.body.id, userId: req.currentUser.profile._id });
          return res.status(201).json({ Update: true });
        }
      } catch (e) {
        logger.error('🔥 error: %o', e);
        return next(e);
      }
    },
  );

  // route.get(
  //   '/userToReview/:id',
  //   middlewares.isAuth,
  //   middlewares.attachCurrentUser,
  //   async (req: Request, res: Response, next: NextFunction) => {
  //     const logger: Logger = Container.get('logger');
  //     logger.debug('Calling Recent Plans endpoint with body: %o', req.body);
  //     try {
  //       const requestServiceInstance = Container.get(RequestService);
  //       const planServiceInstance = Container.get(PlanService);
  //       const profileServiceInstance = Container.get(ProfileService);
  //       const { UserIds } = await requestServiceInstance.GetApprovedUserByPlanId(req.params.id);
  //       const { Users } = await profileServiceInstance.getUsersByIds(UserIds);
  //       const { Plans } = await planServiceInstance.GetReviewsByPlanId(req.params.id);
  //       return res.status(201).json({ Users, Plans });
  //     } catch (e) {
  //       logger.error('🔥 error: %o', e);
  //       return next(e);
  //     }
  //   },
  // );

  route.get(
    '/favourite',
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get('logger');
      logger.debug('Calling Recent Plans endpoint with body: %o', req.body);
      try {
        const planServiceInstance = Container.get(PlanService);
        const { Plans } = await planServiceInstance.GetFavouritePlans(req.currentUser.profile.favouritePlan);
        return res.status(201).json({ Plans });
      } catch (e) {
        logger.error('🔥 error: %o', e);
        return next(e);
      }
    },
  );

  route.put(
    '/markfull/:planId',
    middlewares.isAuth,
    middlewares.attachCurrentUser,
    async (req: Request, res: Response, next: NextFunction) => {
      const logger: Logger = Container.get('logger');
      logger.debug('Calling Marked Booked endpoint with body: %o', req.body);
      try {
        const planServiceInstance = Container.get(PlanService);
        const { Update } = await planServiceInstance.MarkedBook(req.params.planId);
        return res.status(201).json({ Update });
      } catch (e) {
        logger.error('🔥 error: %o', e);
        return next(e);
      }
    },
  );
};
