import { Router, Request, Response, NextFunction } from 'express';
import { Logger } from 'winston';
import { Container } from 'typedi';
import middlewares from '../middlewares';
import AirportService from '../../services/airport';
const route = Router();

export default (app: Router) => {
  app.use('/city', route);

  route.get('/', async (req: Request, res: Response, next: NextFunction) => {
    const logger: Logger = Container.get('logger');
    logger.debug('Calling Sign-Up endpoint with body: %o', req.body);
    try {
      const airportServiceInstance = Container.get(AirportService);
      const { cities } = await airportServiceInstance.GetCities();
      console.log({ cities });
      return res.status(200).json({ cities });
    } catch (e) {
      logger.error('🔥 error: %o', e);
      return next(e);
    }
  });
};
