import { Router, Request, Response, NextFunction } from 'express';
import { Logger } from 'winston';
import { Container } from 'typedi';
import middlewares from '../middlewares';
import OfferService from '../../services/offer';
const route = Router();

export default (app: Router) => {
  app.use('/offer', route);

  route.get('/', middlewares.isAuth, middlewares.attachCurrentUser, async (req: Request, res: Response, next: NextFunction) => {
    const logger: Logger = Container.get('logger');
    logger.debug('Calling Get Offers endpoint with body: %o', req.body);
    try {
      const offerServiceInstance = Container.get(OfferService);
      const { offers } = await offerServiceInstance.GetOffers();
      return res.status(200).json({ offers });
    } catch (e) {
      logger.error('🔥 error: %o', e);
      return next(e);
    }
  });
  route.get('/getByPlanId/:planId', middlewares.isAuth, middlewares.attachCurrentUser, async (req: Request, res: Response, next: NextFunction) => {
    const logger: Logger = Container.get('logger');
    logger.debug('Calling Get Offers endpoint with body: %o', req.body);
    try {
      const offerServiceInstance = Container.get(OfferService);
      const { offers } = await offerServiceInstance.GetOfferByPlanId({ planId: req.params.planId });
      return res.status(200).json({ offers });
    } catch (e) {
      logger.error('🔥 error: %o', e);
      return next(e);
    }
  });
  route.get('/getByOrderId/:orderId', middlewares.isAuth, middlewares.attachCurrentUser, async (req: Request, res: Response, next: NextFunction) => {
    const logger: Logger = Container.get('logger');
    logger.debug('Calling Get Offers endpoint with body: %o', req.body);
    try {
      const offerServiceInstance = Container.get(OfferService);
      const { offers } = await offerServiceInstance.GetOfferByOrderId({ orderId: req.params.orderId });
      return res.status(200).json({ offers });
    } catch (e) {
      logger.error('🔥 error: %o', e);
      return next(e);
    }
  });

  route.get('/getToUserId/:userId', middlewares.isAuth, middlewares.attachCurrentUser, async (req: Request, res: Response, next: NextFunction) => {
    const logger: Logger = Container.get('logger');
    logger.debug('Calling Get Offers endpoint with body: %o', req.body);
    try {
      const offerServiceInstance = Container.get(OfferService);
      const {  offers } = await offerServiceInstance.GetOfferedByUserId({ toUserId: req.params.userId});
      return res.status(200).json({ offers });
    } catch (e) {
      logger.error('🔥 error: %o', e);
      return next(e);
    }
  });

  route.get('/getByUserId/:userId', middlewares.isAuth, middlewares.attachCurrentUser, async (req: Request, res: Response, next: NextFunction) => {
    const logger: Logger = Container.get('logger');
    logger.debug('Calling Get Offers endpoint with body: %o', req.body);
    try {
      const offerServiceInstance = Container.get(OfferService);
      const {  offers } = await offerServiceInstance.GetOffersByUserId({ fromUserId: req.params.userId});
      return res.status(200).json({ offers });
    } catch (e) {
      logger.error('🔥 error: %o', e);
      return next(e);
    }
  });

  route.post('/create', middlewares.isAuth, middlewares.attachCurrentUser, async (req: Request, res: Response, next: NextFunction) => {
    const logger: Logger = Container.get('logger');
    logger.debug('Calling Get Offers endpoint with body: %o', req.body);
    try {
      const offerServiceInstance = Container.get(OfferService);
      const {  OfferRecord } = await offerServiceInstance.CreateOffer(req.body);
      return res.status(200).json({ OfferRecord });
    } catch (e) {
      logger.error('🔥 error: %o', e);
      return next(e);
    }
  });
  route.put('/accept/:offerId', middlewares.isAuth, middlewares.attachCurrentUser, async (req: Request, res: Response, next: NextFunction) => {
    const logger: Logger = Container.get('logger');
    logger.debug('Calling Get Offers endpoint with body: %o', req.body);
    try {
      const offerServiceInstance = Container.get(OfferService);
      const {  OfferRecord } = await offerServiceInstance.AcceptOffer(req.params.offerId);
      return res.status(200).json({ OfferRecord });
    } catch (e) {
      logger.error('🔥 error: %o', e);
      return next(e);
    }
  });
};
