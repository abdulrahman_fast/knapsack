import fs from 'fs';
import path from 'path';
const createDirectory = (uploadPath: string) => {
  uploadPath.split(path.sep).reduce((prevPath, folder) => {
    const currentPath = path.join(prevPath, folder, path.sep);
    if (!fs.existsSync(currentPath)) {
      fs.mkdirSync(currentPath);
    }
    return currentPath;
  }, '');
};

export const saveFileToDirectory = (path: string, name: string, base64: string): Promise<string> => {
  //create directory
  createDirectory(path);

  // Grab the extension to resolve any image error
  const ext = base64.split(';')[0].match(/jpeg|png|gif/)[0];
  // strip off the data: url prefix to get just the base64-encoded bytes
  const base64Image = base64.replace(/^data:image\/\w+;base64,/, '');
  // save file
  return new Promise((resolve, reject) => {
    fs.writeFile(`${path}${name}.${ext}`, base64Image, { encoding: 'base64' }, err => {
      if (err) reject(err);
      else resolve(`${name}.${ext}`);
    });
  });
};

export const readFileAsync = (path: string): Promise<any> => {
  return new Promise((resolve, reject) => {
    fs.readFile(`${path}`, 'utf8', (err, data) => {
      if (err) reject(err);
      else resolve(data);
    });
  });
};

export const substitutePlaceHolder = (originaltext: any, data: any) => {
  const placeholderCollection = originaltext.match(/[^{{\}}]+(?=}})/g);
  if (placeholderCollection && placeholderCollection.length > 0) {
    for (let i = 0; i < placeholderCollection.length; i++) {
      originaltext = originaltext.replace('{{' + placeholderCollection[i] + '}}', data[placeholderCollection[i]]);
    }
  }
  return originaltext;
};
