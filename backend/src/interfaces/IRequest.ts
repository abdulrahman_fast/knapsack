export interface IRequest {
  fromUserId: string;
  status: string;
  note: string;
  place: string;
  date: Date;
  itemName: string;
  weight: number;
  price: number;
  orderId: string;
  planId: string;
}

export interface IRequestInputDTO {
  fromUserId: string;
  status: string;
  note: string;
  place: string;
  itemName: string;
  date: Date;
  weight: number;
  price: number;
  orderId: string;
  planId: string;
}
