export interface IProfile {
  _id?: string;
  avatarUrl: string;
  firstName: string;
  lastName: string;
  contactEmail: string;
  contactPhone: string;
  country: string;
  favouriteOrders: string[],
  favouritePlan: string[],
  loginId: string;
}

export interface IProfileInputDTO {
  firstName: string;
  lastName: string;
  contactEmail: string;
  contactPhone: string;
  country: string;
  loginId: string;
}
