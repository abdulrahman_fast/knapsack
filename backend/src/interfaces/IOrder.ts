import { IAirport } from './IAirport';
import { IReview } from './IReview';

export interface IOrder {
  id: string;
  from: string;
  to: string;
  date: Date;
  booked: boolean;
  itemName: string;
  weight: number;
  price: number;
  note: string;
  reviews: string[];
  profileId: string;
}


export interface IOrderReview {
  id: string;
  from: string;
  to: string;
  date: Date;
  booked: boolean;
  itemName: string;
  weight: number;
  price: number;
  note: string;
  reviews: IReview[];
  profileId: string;
}

export interface IOrderResponseDTO {
  id: string;
  from: IAirport;
  to: IAirport;
  date: Date;
  booked: boolean,
  itemName: string;
  weight: number;
  price: number;
  note: string;
  profileId: string;
}
export interface IOrderInputDTO {
  from: string;
  to: string;
  date: Date;
  itemName: string;
  weight: number;
  price: number;
  note: string;
  profileId: string;
}
