export interface IUser {
  _id: string;
  name: string;
  email: string;
  password: string;
  tokenValidated: boolean;
  salt: string;
}

export interface IUserInputDTO {
  name: string;
  email: string;
  password: string;
}

export interface IEmailInputDTO {
  email: string;
}

export interface ITokenInputDTO {
  code: string;
  email: string;
}

export interface IPasswordInputDTO {
  email: string;
  password: string;
}
