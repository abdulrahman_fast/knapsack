import { IAirport } from './IAirport';

export interface IPlan {
  id: string;
  from: string;
  to: string;
  date: Date;
  itemName: string;
  space: number;
  price: number;
  booked: boolean;
  note: string;
  reviews: string[];
  profileId: string;
}

export interface IPlanResponseDTO {
  id: string;
  from: IAirport;
  to: IAirport;
  date: Date;
  booked: boolean;
  itemName: string;
  space: number;
  price: number;
  note: string;
}

export interface IPlanInputDTO {
  from: string;
  to: string;
  date: Date;
  itemName: string;
  space: number;
  price: number;
  note: string;
  profileId: string;
}
