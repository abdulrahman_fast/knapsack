export interface IFile {
  filename: string;
  path: string;
  size: number;
  fieldname: string;
  originalname: string;
  encoding: string;
  mimetype: string;
  destination: string;
}
