export interface IAirport {
  name: string;
  city: string;
  country: string;
  iata_code: string;
  _geoloc: {
    lat: number;
    lng: number;
  };
  _id: string;
  links_count: number;
  objectID: string;
}
