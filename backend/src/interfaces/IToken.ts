export interface IToken {
  _id: string;
  token: string;
  loginId: string;
  expiresAt: Date;
}
