export interface IReview {
  _id: string;
  fromUserId: string;
  toUserId: string;
  rating: number;
  note: string;
}

export interface IReviewInputDTO {
  fromUserId: string;
  toUserId: string;
  rating: number;
  note: string;
}
