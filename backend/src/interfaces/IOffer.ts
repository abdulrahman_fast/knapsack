export interface IOffer {
    fromUserId: string;
    toUserId: string;
    weight: Number,
    price: Number,
    orderId: string;
    travellerReview: string;
    ordererReview: string;
    planId:  string;
    status: string;
}

export interface IOfferInput {
    fromUserId: string;
    toUserId: string;
    weight: Number,
    price: Number,
    orderId?: string;
    planId?:  string;
    status: string;
}