import { Service, Inject } from 'typedi';
import { EventDispatcher, EventDispatcherInterface } from '../decorators/eventDispatcher';

@Service()
export default class AirportService {
  constructor(
    @Inject('airportModel') private airportModel: Models.AirportModel,
    @Inject('logger') private logger,
    @EventDispatcher() private eventDispatcher: EventDispatcherInterface,
  ) {}

  public async GetCities(): Promise<{ cities: { id: string; name: string }[] }> {
    try {
      const aiports = await this.airportModel.find({}).sort({ city: 1 });
      const uniqueArray = aiports.reduce(function (a, d) {
        if (a.indexOf(d.city) === -1) {
          a.push({ id: d._id, name: `${d.city}, ${d.country}` });
        }
        return a;
      }, []);
      return { cities: uniqueArray };
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }
}
