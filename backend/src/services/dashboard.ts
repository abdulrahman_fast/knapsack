import { Service, Inject } from 'typedi';
import { EventDispatcher, EventDispatcherInterface } from '../decorators/eventDispatcher';
import { Stats } from '../interfaces/IStats';

@Service()
export default class DashboardService {
  constructor(
    @Inject('orderModel') private orderModel: Models.OrderModel,
    @Inject('planModel') private planModel: Models.PlanModel,
    @Inject('logger') private logger,
    @EventDispatcher() private eventDispatcher: EventDispatcherInterface,
  ) {}

  public async GetStats(): Promise<{ stats: Stats }> {
    try {
      const orders = await this.orderModel.countDocuments();
      const plans = await this.planModel.countDocuments();

      return {
        stats: {
          plans,
          orders,
        },
      };
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }
}
