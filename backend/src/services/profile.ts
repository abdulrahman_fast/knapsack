import Container, { Service, Inject } from 'typedi';
import { EventDispatcher, EventDispatcherInterface } from '../decorators/eventDispatcher';
import { IProfile, IProfileInputDTO } from '../interfaces/IProfile';
import FileService from '../services/file';

@Service()
export default class ProfileService {
  constructor(
    @Inject('profileModel') private profileModel: Models.ProfileModel,
    @Inject('userModel') private userModel: Models.UserModel,
    @Inject('logger') private logger,
    @EventDispatcher() private eventDispatcher: EventDispatcherInterface,
  ) {}

  public async AddFavouriteOrder({ id, userId }: { id: string; userId: string }): Promise<{ Update: any }> {
    try {
      const Update = await this.profileModel.updateOne({ _id: userId }, { $push: { favouriteOrders: id } });
      return { Update };
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }

  public async getUsersByIds(UserIds: string[]): Promise<{ Users: IProfile[] }> {
    try {
      const Users = await this.profileModel.find({_id: {$in: UserIds}});
      return { Users };
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }


  public async AddFavouritePlan({ id, userId }: { id: string; userId: string }): Promise<{ Update: any }> {
    try {
      const Update = await this.profileModel.updateOne({ _id: userId }, { $push: { favouritePlan: id } });
      return { Update };
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }

  public async RemoveFavouriteOrder({ id, userId }: { id: string; userId: string }): Promise<{ Update: any }> {
    try {
      const Update = await this.profileModel.updateOne({ _id: userId }, { $pull: { favouriteOrders: id } });
      return { Update };
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }

  public async RemoveFavouritePlan({ id, userId }: { id: string; userId: string }): Promise<{ Update: any }> {
    try {
      const Update = await this.profileModel.updateOne({ _id: userId }, { $pull: { favouritePlan: id } });
      return { Update };
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }

  public async SetProfile(
    profileInputDTO: IProfileInputDTO,
    file: Express.Multer.File,
  ): Promise<{ profile: IProfile }> {
    try {
      const fileService = Container.get(FileService);
      const Record = await this.userModel.findOne({ _id: profileInputDTO.loginId });
      const { file: ProfileAvater } = await fileService.CreateFile(file);
      if (!Record) {
        throw new Error('User doesnot exist');
      }
      const Profile = await this.profileModel.findOne({ loginId: profileInputDTO.loginId });
      if (Profile) {
        throw new Error('Profile already exist');
      }
      const profileRecord = await this.profileModel.create({
        avatarUrl: ProfileAvater?.filename,
        firstName: profileInputDTO.firstName,
        lastName: profileInputDTO.lastName,
        favouriteOrders: [],
        favouritePlan: [],
        contactEmail: profileInputDTO.contactEmail,
        contactPhone: profileInputDTO.contactPhone,
        country: profileInputDTO.country,
        loginId: profileInputDTO.loginId,
      });
      return { profile: profileRecord };
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }
}
