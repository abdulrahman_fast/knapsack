import moment from 'moment';
import { Service, Inject } from 'typedi';
import { EventDispatcher, EventDispatcherInterface } from '../decorators/eventDispatcher';
import { IOrder, IOrderInputDTO, IOrderReview } from '../interfaces/IOrder';
import { IPlan, IPlanResponseDTO } from '../interfaces/IPlan';

@Service()
export default class OrderService {
  constructor(
    @Inject('orderModel') private orderModel: Models.OrderModel,
    @Inject('logger') private logger,
    @EventDispatcher() private eventDispatcher: EventDispatcherInterface,
  ) { }

  public async CreateOrder(orderInputDTO: IOrderInputDTO): Promise<{ order: IOrder }> {
    try {
      const orderRecord = await this.orderModel.create({
        from: orderInputDTO.from,
        to: orderInputDTO.to,
        date: orderInputDTO.date,
        itemName: orderInputDTO.itemName,
        reviews: [],
        weight: orderInputDTO.weight,
        booked: false,
        price: orderInputDTO.price,
        note: orderInputDTO.note,
        profileId: orderInputDTO.profileId,
      });
      return { order: orderRecord };
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }

  public async GetFavouriteOrders(orderIds: string[]): Promise<{ Orders: IOrder[] }> {
    try {
      const Orders = await this.orderModel.find({ _id: { $in: orderIds } })
        .populate({ path: 'profileId', model: 'Profile' })
        .populate({ path: 'to', model: 'Airport' })
        .populate({ path: 'reviews', model: 'Review' })
        .populate({ path: 'from', model: 'Airport' });
      return { Orders: Orders };
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }

  public async MarkedBook(orderId: string): Promise<{ Update: any }> {
    try {
      const Update = await this.orderModel.updateOne({ _id: orderId }, { booked: true });
      return { Update };
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }

  public async GetOrder(): Promise<{ Data: IOrder[] }> {
    try {
      const orderRecord = await this.orderModel
        .find({})
        .populate({ path: 'profileId', model: 'Profile' })
        .populate({ path: 'to', model: 'Airport' })
        .populate({ path: 'reviews', model: 'Review' })
        .populate({ path: 'from', model: 'Airport' });
      return { Data: orderRecord };
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }

  public async GetOrderById(id: string): Promise<{ order: IOrder }> {
    try {
      const orderRecord = await this.orderModel
        .findById(id)
        .populate({ path: 'profileId', model: 'Profile' })
        .populate({ path: 'to', model: 'Airport' })
        .populate({ path: 'reviews', model: 'Review' })
        .populate({ path: 'from', model: 'Airport' });
      return { order: orderRecord };
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }

  public async getRelatedOrders(plan: IPlanResponseDTO | any): Promise<{ Orders: IOrder[] }> {
    try {
      const Orders = await this.orderModel
        .find({
          to: plan.to._id,
          from: plan.from._id,
          price: { $lte: plan.price },
          date: { $lte: plan.date },
        })
        .populate({ path: 'profileId', model: 'Profile' })
        .populate({ path: 'reviews', model: 'Review' })
        .populate({ path: 'to', model: 'Airport' })
        .populate({ path: 'from', model: 'Airport' });


      return { Orders: Orders };
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }

  public async GetMyOrders(profileId: string): Promise<{ Orders: IOrder[] }> {
    try {
      const Orders = await this.orderModel
        .find({ profileId })
        .populate({ path: 'to', model: 'Airport' })
        .populate({ path: 'reviews', model: 'Review' })
        .populate({ path: 'from', model: 'Airport' });
      return { Orders };
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }
  public async GetReviewsByOrderId(orderId: string): Promise<{ Order: any }> {
    try {
      const Order = await this.orderModel
        .findOne({ orderId })
        .populate({
          path: 'reviews', model: 'Review',
          populate: { path: 'toUserId', model: 'User' }
        })
      return { Order };
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }

  public async GetRecentOrders(id: string): Promise<{ Orders: IOrder[] }> {
    try {
      const OrderRecord = await this.orderModel
        .find({ profileId: { $ne: id } }, null, { sort: { createdAt: -1 } })
        .populate({ path: 'profileId', model: 'Profile' })
        .populate({ path: 'to', model: 'Airport' })
        .populate({ path: 'from', model: 'Airport' })
        .populate({ path: 'reviews', model: 'Review' })
        .limit(10);
      return { Orders: OrderRecord };
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }
  public async getOrderIdsOfUser(userId: string): Promise<string[]> {
    try {
      const OrderRecords = await this.orderModel.find({ profileId: userId });
      return OrderRecords.map(x => x._id);
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }

  public async AddReview(planId: string, reviewId: string): Promise<any> {
    try {
      const OrderRecord = await this.orderModel.updateOne({ _id: planId }, { $push: { reviews: reviewId } });
      return OrderRecord;
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }
  public async Search({
    to,
    from,
    id,
    date,
    space,
  }: {
    to: string;
    from: string;
    date: Date | string | undefined;
    space: number | undefined;
    id: string;
  }): Promise<{ Orders: IOrder[] }> {
    try {
      let OrderRecords = await this.orderModel
        .find({ to, from, profileId: { $ne: id } })
        .populate({ path: 'profileId', model: 'Profile' })
        .populate({ path: 'to', model: 'Airport' })
        .populate({ path: 'reviews', model: 'Review' })
        .populate({ path: 'from', model: 'Airport' });

      if (date) {
        OrderRecords = OrderRecords.filter(x => moment(x.date).isBefore(date));
      }

      if (space) {
        OrderRecords = OrderRecords.filter(x => x.weight >= space);
      }

      return { Orders: OrderRecords };
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }
}
