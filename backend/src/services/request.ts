import { Service, Inject } from 'typedi';
import { EventDispatcher, EventDispatcherInterface } from '../decorators/eventDispatcher';
import { IRequest, IRequestInputDTO } from '../interfaces/IRequest';

@Service()
export default class RequestService {
  constructor(
    @Inject('requestModel') private RequestModel: Models.RequestModel,
    @Inject('logger') private logger,
    @EventDispatcher() private eventDispatcher: EventDispatcherInterface,
  ) {}

  public async CreateRequest(RequestInputDTO: IRequestInputDTO): Promise<{ Request: IRequest }> {
    try {
      const RequestRecord = await this.RequestModel.create({
        fromUserId: RequestInputDTO.fromUserId,
        note: RequestInputDTO.note,
        status: RequestInputDTO.status,
        orderId: RequestInputDTO.orderId,
        planId: RequestInputDTO.planId,
        place: RequestInputDTO.place,
        date: RequestInputDTO.date,
        itemName: RequestInputDTO.itemName,
        weight: RequestInputDTO.weight,
        price: RequestInputDTO.price,
      });
      return { Request: RequestRecord };
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }

  public async MarkRequest(MarkRequestInputDTO: {
    status: boolean;
    requestId: string;
  }): Promise<{ Request: IRequest }> {
    try {
      const RequestRecord = await this.RequestModel.updateOne(
        { _id: MarkRequestInputDTO.requestId },
        { status: MarkRequestInputDTO.status ? 'approved' : 'rejected' },
      );
      return { Request: RequestRecord };
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }

  public async GetRequests(): Promise<{ Requests: IRequest[] }> {
    try {
      const RequestRecords = await this.RequestModel.find({});
      return { Requests: RequestRecords };
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }

  public async GetApprovedUserByOrderId(orderId: string): Promise<{ UserIds: string[] }> {
    try {
      const UserIds =  await this.RequestModel.find({orderId, status: 'approved'});
      return { UserIds: UserIds.map(x => x.fromUserId) };
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }
  public async GetApprovedUserByPlanId(planId: string): Promise<{ UserIds: string[] }> {
    try {
      const UserIds =  await this.RequestModel.find({planId, status: 'approved'});
      return { UserIds: UserIds.map(x => x.fromUserId) };
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }
  public async GetMyApprovedRequest(userId: string): Promise<{ Requests: IRequest[] }> {
    try {
      const RequestRecords = await this.RequestModel.find({ fromUserId: userId, status: 'approved' })
      .populate({
        path: 'fromUserId',
        model: 'Profile',
      })
      .populate({
        path: 'planId',
        model: 'Plan',
        populate: [
          {
            path: 'from',
            model: 'Airport',
          },
          { path: 'to', model: 'Airport' },
        ],
      })
        .populate({
          path: 'orderId',
          model: 'Order',
          populate: [
            {
              path: 'from',
              model: 'Airport',
            },
            { path: 'to', model: 'Airport' },
          ],
        });
      return { Requests: RequestRecords };
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }

  public async GetMyPendingRequest(planIds: string[], orderIds: string[]): Promise<{ Requests: IRequest[] }> {
   console.log({planIds, orderIds})
    try {
      const RequestOrderRecords = await this.RequestModel.find({
        orderId: { $in: orderIds },
        status: 'pending',
      })
        .populate({
          path: 'fromUserId',
          model: 'Profile',
        })
        .populate({
          path: 'orderId',
          model: 'Order',
          populate: [
            {
              path: 'from',
              model: 'Airport',
            },
            { path: 'to', model: 'Airport' },
          ],
        });
      const RequestPlanRecords = await this.RequestModel.find({
        planId: { $in: planIds },
        status: 'pending',
      })
        .populate({
          path: 'fromUserId',
          model: 'Profile',
        })
        .populate({
          path: 'planId',
          model: 'Plan',
          populate: [
            {
              path: 'from',
              model: 'Airport',
            },
            { path: 'to', model: 'Airport' },
          ],
        });

      console.log([...RequestOrderRecords, ...RequestPlanRecords]);
      return { Requests: [...RequestOrderRecords, ...RequestPlanRecords] };
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }

  public async GetRequestByOrderId(orderId: string): Promise<{ Requests: IRequest[] }> {
    try {
      const RequestRecords = await this.RequestModel.find({ orderId: orderId, status: 'pending' })
        .populate({
          path: 'fromUserId',
          model: 'Profile',
        })
        .limit(10);
      return { Requests: RequestRecords };
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }
  public async GetRequestByPlanId(planId: string): Promise<{ Requests: IRequest[] }> {
    try {
      const RequestRecords = await this.RequestModel.find({ planId: planId, status: 'pending' })
        .populate({
          path: 'fromUserId',
          model: 'Profile',
        })
        .limit(10);
      return { Requests: RequestRecords };
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }
  public async isUserHaveRequested(orderId: string, profileId: string): Promise<boolean> {
    try {
      const documentExist = await this.RequestModel.exists({ orderId, fromUserId: profileId });
      return documentExist;
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }
  public async getStatusOrderRequest(orderId: string, profileId: string): Promise<string> {
    try {
      const document = await this.RequestModel.findOne({ orderId, fromUserId: profileId });
      return document.status;
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }

  public async isUserHavePlanRequested(planId: string, profileId: string): Promise<boolean> {
    try {
      const documentExist = await this.RequestModel.exists({ planId, fromUserId: profileId });
      return documentExist;
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }
  public async getStatusPlanRequest(planId: string, profileId: string): Promise<string> {
    try {
      const document = await this.RequestModel.findOne({ planId, fromUserId: profileId });
      return document.status;
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }
}
