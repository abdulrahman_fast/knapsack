import { Service, Inject } from 'typedi';
import { EventDispatcher, EventDispatcherInterface } from '../decorators/eventDispatcher';
import { IReview } from '../interfaces/IReview';

@Service()
export default class ReviewService {
  constructor(
    @Inject('reviewModel') private reviewModel: Models.ReviewModel,
    @Inject('logger') private logger,
    @EventDispatcher() private eventDispatcher: EventDispatcherInterface,
  ) {}

  public async GetReviews(): Promise<{ reviews: IReview[] }> {
    try {
      const reviews = await this.reviewModel.find({}).populate({path: 'fromUserId', model: 'Profile'}).populate({path: 'toUserId', model: 'Profile'});
      return { reviews };
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }
  

  public async GetReviewsOfUser(userId: string): Promise<{ reviews: IReview[] }> {
    try {
      const reviews = await this.reviewModel.find({toUserId: userId}).populate({path: 'fromUserId', model: 'Profile'}).populate({path: 'toUserId', model: 'Profile'});
      return { reviews };
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }

  public async RateUser({ fromUserId, toUserId, rating, note }: {fromUserId: string, toUserId: string, rating: number, note: string}): Promise<{ review: IReview }> {
    try {
      const review = await this.reviewModel.create({fromUserId, toUserId, rating, note});
      return { review };
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }

  
}
