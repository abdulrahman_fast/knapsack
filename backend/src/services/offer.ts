import { Service, Inject } from 'typedi';
import { EventDispatcher, EventDispatcherInterface } from '../decorators/eventDispatcher';
import { IOffer, IOfferInput } from '../interfaces/IOffer';

@Service()
export default class OfferService {
    constructor(
        @Inject('offerModel') private offerModel: Models.OfferModel,
        @Inject('logger') private logger,
        @EventDispatcher() private eventDispatcher: EventDispatcherInterface,
    ) { }

    public async GetOffers(): Promise<{ offers: IOffer[] }> {
        try {
            const offers = await this.offerModel.find({}).populate({
                path: 'fromUserId',
                model: 'Profile',
            },
                {
                    path: 'toUserId',
                    model: 'Profile',
                }).populate({ path: 'travellerReview', model: 'Review' })
                .populate({ path: 'ordererReview', model: 'Review' })
                .populate({
                    path: 'planId',
                    model: 'Plan',
                    populate: [
                        {
                            path: 'from',
                            model: 'Airport',
                        },
                        { path: 'to', model: 'Airport' },
                    ]
                }).populate({
                    path: 'orderId',
                    model: 'Order',
                    populate: [
                        {
                            path: 'from',
                            model: 'Airport',
                        },
                        { path: 'to', model: 'Airport' },
                    ]
                });
            return { offers };
        } catch (e) {
            this.logger.error(e);
            throw e;
        }
    }

    public async GetOfferByPlanId({ planId }): Promise<{ offers: IOffer[] }> {
        try {
            const offers = await this.offerModel.find({ planId }).populate({
                path: 'fromUserId',
                model: 'Profile',
            },
                {
                    path: 'toUserId',
                    model: 'Profile',
                }).populate({ path: 'travellerReview', model: 'Review' })
                .populate({ path: 'ordererReview', model: 'Review' })
                .populate({
                    path: 'planId',
                    model: 'Plan',
                    populate: [
                        {
                            path: 'from',
                            model: 'Airport',
                        },
                        { path: 'to', model: 'Airport' },
                    ]
                });
            return { offers };
        } catch (e) {
            this.logger.error(e);
            throw e;
        }
    }

    public async GetOfferByOrderId({ orderId }): Promise<{ offers: IOffer[] }> {
        try {
            const offers = await this.offerModel.find({ orderId }).populate({
                path: 'fromUserId',
                model: 'Profile',
            },
                {
                    path: 'toUserId',
                    model: 'Profile',
                }).populate({ path: 'travellerReview', model: 'Review' })
                .populate({ path: 'ordererReview', model: 'Review' })
                .populate({
                    path: 'orderId',
                    model: 'Order',
                    populate: [
                        {
                            path: 'from',
                            model: 'Airport',
                        },
                        { path: 'to', model: 'Airport' },
                    ]
                });
            return { offers };
        } catch (e) {
            this.logger.error(e);
            throw e;
        }
    }

    public async GetOffersByUserId({ fromUserId }): Promise<{ offers: IOffer[] }> {
        try {
            const offers = await this.offerModel.find({ fromUserId }).populate({
                path: 'fromUserId',
                model: 'Profile',
            },
                {
                    path: 'toUserId',
                    model: 'Profile',
                }).populate({ path: 'travellerReview', model: 'Review' })
                .populate({ path: 'ordererReview', model: 'Review' })
                .populate({
                    path: 'orderId',
                    model: 'Order',
                    populate: [
                        {
                            path: 'from',
                            model: 'Airport',
                        },
                        { path: 'to', model: 'Airport' },
                    ]
                })
                .populate({
                    path: 'planId',
                    model: 'Plan',
                    populate: [
                        {
                            path: 'from',
                            model: 'Airport',
                        },
                        { path: 'to', model: 'Airport' },
                    ]
                });
            return { offers };
        } catch (e) {
            this.logger.error(e);
            throw e;
        }
    }

    public async GetOfferedByUserId({ toUserId }): Promise<{ offers: IOffer[] }> {
        try {
            const offers = await this.offerModel.find({ toUserId }).populate({
                path: 'fromUserId',
                model: 'Profile',
            },
                {
                    path: 'toUserId',
                    model: 'Profile',
                })
                .populate({ path: 'travellerReview', model: 'Review' })
                .populate({ path: 'ordererReview', model: 'Review' })
                .populate({
                    path: 'orderId',
                    model: 'Order',
                    populate: [
                        {
                            path: 'from',
                            model: 'Airport',
                        },
                        { path: 'to', model: 'Airport' },
                    ]
                })
                .populate({
                    path: 'planId',
                    model: 'Plan',
                    populate: [
                        {
                            path: 'from',
                            model: 'Airport',
                        },
                        { path: 'to', model: 'Airport' },
                    ]
                });
            return { offers };
        } catch (e) {
            this.logger.error(e);
            throw e;
        }
    }

    public async CreateOffer({ toUserId, fromUserId, planId, orderId, weight, price, status }: IOfferInput): Promise<{ OfferRecord: IOffer }> {
        try {
            const OfferRecord = await this.offerModel.create({
                toUserId,
                fromUserId,
                planId,
                orderId,
                weight,
                price,
                status,
                ordererReview: null,
                travellerReview: null
            });
            return { OfferRecord };
        } catch (e) {
            this.logger.error(e);
            throw e;
        }
    }

    public async AddOrderReview({ offerId, reviewId }: { offerId: string, reviewId: string }) {
        try {
            const OfferRecord = await this.offerModel.updateOne({ _id: offerId }, {
                ordererReview: reviewId
            });
            return { OfferRecord };
        } catch (e) {
            this.logger.error(e);
            throw e;
        }
    }

    public async AddTravellerReview({ offerId, reviewId }: { offerId: string, reviewId: string }) {
        try {
            const OfferRecord = await this.offerModel.updateOne({ _id: offerId }, {
                travellerReview: reviewId
            });
            return { OfferRecord };
        } catch (e) {
            this.logger.error(e);
            throw e;
        }
    }

    public async AcceptOffer(offerId: string) {
        try {
            const OfferRecord = await this.offerModel.updateOne({ _id: offerId }, {
                status: 'accepted'
            });
            return { OfferRecord };
        } catch (e) {
            this.logger.error(e);
            throw e;
        }
    }


}
