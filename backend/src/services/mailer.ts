import { Service, Inject } from 'typedi';
import { readFileAsync, substitutePlaceHolder } from '../utils/filehandling';
import path from 'path';
import { IUser } from '../interfaces/IUser';
import config from '../config';
import sgMail from '@sendgrid/mail';

@Service()
export default class EmailService {
  constructor(@Inject('logger') private logger) {
    sgMail.setApiKey(config.sendgridApi);
  }
  public async sendSignUpEmail(user: IUser): Promise<any> {
    try {
      let html = '';
      try {
        html = await readFileAsync(path.join(__dirname, '..', '..', 'public', 'templates', 'signup.html'));
      } catch (error) {
        this.logger.error(error);
        return;
      }

      const url = `${''}/auth/verifyaccount/${user._id}`; // config.api.url
      html = substitutePlaceHolder(html, { fullname: user.name, email: user.email, code: url });

      const msg = {
        to: user.email,
        from: 'admin@passively.app',
        subject: 'Welcome to Smart Profile',
        html: html,
      };

      return new Promise(function (resolve, reject) {
        sgMail
          .send(msg)
          .then(response => {
            resolve(true);
          })
          .catch(error => {
            reject(error);
          });
      });
    } catch (error) {
      this.logger.error(error);
      throw new Error('Unable to send signup email');
    }
  }
}
