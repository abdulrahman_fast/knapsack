import { Service, Inject } from 'typedi';
import jwt from 'jsonwebtoken';
import MailerService from './mailer';
import config from '../config';
import argon2 from 'argon2';
import { randomBytes } from 'crypto';
import { IEmailInputDTO, ITokenInputDTO, IUser, IPasswordInputDTO } from '../interfaces/IUser';
import { EventDispatcher, EventDispatcherInterface } from '../decorators/eventDispatcher';
import events from '../subscribers/events';
import moment from 'moment';
import { off } from 'process';
import { IProfile } from '../interfaces/IProfile';

function makeid(length) {
  let result = '';
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
  const charactersLength = characters.length;
  for (let i = 0; i < length; i++) {
    result += characters.charAt(Math.floor(Math.random() * charactersLength));
  }
  return result;
}

@Service()
export default class AuthService {
  constructor(
    @Inject('userModel') private userModel: Models.UserModel,
    @Inject('tokenModel') private tokenModel: Models.TokenModel,
    @Inject('profileModel') private profileModel: Models.ProfileModel,
    private mailer: MailerService,
    @Inject('logger') private logger,
    @EventDispatcher() private eventDispatcher: EventDispatcherInterface,
  ) {}

  public async SignUp(emailInputDTO: IEmailInputDTO): Promise<{ user: IUser; token: string }> {
    try {
      const Record = await this.userModel.findOne({ email: emailInputDTO.email });
      let userRecord;
      if (Record) {
        if (Record.tokenValidated) {
          throw new Error('User already exist');
        } else {
          userRecord = Record;
        }
      } else {
        userRecord = await this.userModel.create({
          email: emailInputDTO.email,
          salt: '',
          password: '',
          tokenValidated: false,
          name: '',
        });
      }
      if (!userRecord) {
        throw new Error('User cannot be created');
      }
      this.logger.silly('Sending welcome email');
      const token = makeid(8);
      this.eventDispatcher.dispatch(events.user.signUp, { user: userRecord });
      this.tokenModel.create({
        token,
        loginId: userRecord._id,
        expiresAt: moment().add(10, 'minutes'),
      });
      const authToken = this.generateToken(userRecord);
      const user = userRecord.toObject();
      Reflect.deleteProperty(user, 'password');
      Reflect.deleteProperty(user, 'salt');
      return { user, token: authToken };
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }

  public async VerifyCode(codeInputDTO: ITokenInputDTO): Promise<{ user: IUser; token: string; codeVerify: boolean }> {
    try {
      const userRecord = await this.userModel.findOne({ email: codeInputDTO.email });
      if (!userRecord) {
        throw new Error('User cannot be found');
      }
      const token = await this.tokenModel.findOne({ loginId: userRecord.id, token: codeInputDTO.code });
      if (token && moment(token.expiresAt).isAfter(moment())) {
        await this.userModel.updateOne({ email: codeInputDTO.email }, { tokenValidated: true });
        return { user: userRecord, token: this.generateToken(userRecord), codeVerify: true };
      } else {
        return { user: userRecord, token: this.generateToken(userRecord), codeVerify: false };
      }
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }

  public async SetPassword(passwordInputDTO: IPasswordInputDTO): Promise<{ user: IUser }> {
    const userRecord = await this.userModel.findOne({ email: passwordInputDTO.email });
    if (!userRecord) {
      throw new Error('User cannot be found');
    }
    const salt = randomBytes(32);
    this.logger.silly('Hashing password');
    const hashedPassword = await argon2.hash(passwordInputDTO.password, { salt });
    this.logger.silly('Creating user db record');
    const updatedUserRecord = await this.userModel.updateOne(
      { _id: userRecord._id },
      {
        salt: salt.toString('hex'),
        password: hashedPassword,
      },
    );

    return { user: updatedUserRecord };
  }

  public async SignIn({
    email,
    password,
  }: {
    email: string;
    password: string;
  }): Promise<{ user: IProfile; token: string }> {
    const userRecord = await this.userModel.findOne({ email });
    if (!userRecord) {
      throw new Error('User not registered');
    }
    /**
     * We use verify from argon2 to prevent 'timing based' attacks
     */
    this.logger.silly('Checking password');
    const validPassword = await argon2.verify(userRecord.password, password);
    if (validPassword) {
      this.logger.silly('Password is valid!');
      this.logger.silly('Generating JWT');
      const token = this.generateToken(userRecord);
      const user = userRecord.toJSON();
      const profileRecord = await this.profileModel.findOne({ loginId: user._id });

      Reflect.deleteProperty(user, 'password');
      Reflect.deleteProperty(user, 'salt');
      /**
       * Easy as pie, you don't need passport.js anymore :)
       */
      return { user: profileRecord, token };
    } else {
      throw new Error('Invalid Password');
    }
  }

  private generateToken(user) {
    const today = new Date();
    const exp = new Date(today);
    exp.setDate(today.getDate() + 60);

    /**
     * A JWT means JSON Web Token, so basically it's a json that is _hashed_ into a string
     * The cool thing is that you can add custom properties a.k.a metadata
     * Here we are adding the userId, role and name
     * Beware that the metadata is public and can be decoded without _the secret_
     * but the client cannot craft a JWT to fake a userId
     * because it doesn't have _the secret_ to sign it
     * more information here: https://softwareontheroad.com/you-dont-need-passport
     */
    this.logger.silly(`Sign JWT for userId: ${user._id}`);
    return jwt.sign(
      {
        _id: user._id, // We are gonna use this in the middleware 'isAuth'
        role: user.role,
        name: user.name,
        exp: exp.getTime() / 1000,
      },
      config.jwtSecret,
    );
  }
}
