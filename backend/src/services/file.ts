import { Service, Inject } from 'typedi';
import { EventDispatcher, EventDispatcherInterface } from '../decorators/eventDispatcher';
import { IFile } from '../interfaces/IFile';

@Service()
export default class FileService {
  constructor(
    @Inject('fileModel') private fileModel: Models.FileModel,
    @Inject('logger') private logger,
    @EventDispatcher() private eventDispatcher: EventDispatcherInterface,
  ) {}

  public async CreateFile(file: IFile): Promise<{ file: IFile }> {
    try {
      return { file: await this.fileModel.create(file) };
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }
}
