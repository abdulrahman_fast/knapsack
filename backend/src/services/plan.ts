import moment from 'moment';
import { Service, Inject } from 'typedi';
import { EventDispatcher, EventDispatcherInterface } from '../decorators/eventDispatcher';
import { IOrder, IOrderResponseDTO } from '../interfaces/IOrder';
import { IPlan, IPlanInputDTO, IPlanResponseDTO } from '../interfaces/IPlan';

@Service()
export default class PlanService {
  constructor(
    @Inject('planModel') private PlanModel: Models.PlanModel,
    @Inject('logger') private logger,
    @EventDispatcher() private eventDispatcher: EventDispatcherInterface,
  ) {}

  public async CreatePlan(PlanInputDTO: IPlanInputDTO): Promise<{ Plan: IPlan }> {
    try {
      const PlanRecord = await this.PlanModel.create({
        from: PlanInputDTO.from,
        to: PlanInputDTO.to,
        date: PlanInputDTO.date,
        itemName: PlanInputDTO.itemName,
        space: PlanInputDTO.space,
        booked: false,
        reviews: [],
        price: PlanInputDTO.price,
        note: PlanInputDTO.note,
        profileId: PlanInputDTO.profileId,
      });
      return { Plan: PlanRecord };
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }

  public async GetFavouritePlans(planIds: string[]): Promise<{ Plans: IPlan[] }> {
    try {
      const PlanRecord = await this.PlanModel.find({ _id: { $in: planIds } })
        .populate({ path: 'profileId', model: 'Profile' })
        .populate({ path: 'to', model: 'Airport' })
        .populate({ path: 'from', model: 'Airport' });
      return { Plans: PlanRecord };
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }
  public async GetPlan(): Promise<{ Data: IPlan[] }> {
    try {
      const PlanRecord = await this.PlanModel.find({})
        .populate({ path: 'profileId', model: 'Profile' })
        .populate({ path: 'to', model: 'Airport' })
        .populate({ path: 'from', model: 'Airport' });
      return { Data: PlanRecord };
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }

  public async MarkedBook(planId: string): Promise<{ Update: any }> {
    try {
      const Update = await this.PlanModel.updateOne({ _id: planId }, { booked: true });
      return { Update };
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }

  public async getRelatedTravellers(order: IOrderResponseDTO | any): Promise<{ Plans: IPlan[] }> {
    try {
      const PlanRecord = await this.PlanModel.find({
        to: order.to._id,
        from: order.from._id,
        price: { $lte: order.price },
        date: { $lte: order.date },
      })
        .populate({ path: 'profileId', model: 'Profile' })
        .populate({ path: 'to', model: 'Airport' })
        .populate({ path: 'from', model: 'Airport' });
      return { Plans: PlanRecord };
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }

  public async GetMyPlans(profileId: string): Promise<{ Plans: IPlan[] }> {
    try {
      const Plans = await this.PlanModel.find({ profileId })
        .populate({ path: 'to', model: 'Airport' })
        .populate({ path: 'from', model: 'Airport' });
      return { Plans };
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }

  public async GetPlanById(id: string): Promise<{ plan: IPlan }> {
    try {
      const planRecord = await this.PlanModel.findById(id)
        .populate({ path: 'profileId', model: 'Profile' })
        .populate({ path: 'to', model: 'Airport' })
        .populate({ path: 'from', model: 'Airport' });
      return { plan: planRecord };
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }

  public async GetRecentPlans(id: string): Promise<{ Plans: IPlan[] }> {
    try {
      const PlanRecord = await this.PlanModel.find(
        { profileId: { $ne: id }, date: { $gte: new Date() }, booked: false },
        null,
        { sort: { createdAt: -1 } },
      )
        .populate({ path: 'profileId', model: 'Profile' })
        .populate({ path: 'to', model: 'Airport' })
        .populate({ path: 'from', model: 'Airport' })
        .limit(10);
      return { Plans: PlanRecord };
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }
  public async getPlanIdsOfUser(userId: string): Promise<string[]> {
    try {
      const PlanRecord = await this.PlanModel.find({ profileId: userId, date: { $gte: new Date() }, booked: false });
      return PlanRecord.map(x => x._id);
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }

  public async AddReview(planId: string, reviewId: string): Promise<any> {
    try {
      const PlanRecord = await this.PlanModel.updateOne({_id: planId} ,  { $push: { reviews: reviewId } } );
      return PlanRecord;
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }
  public async GetReviewsByPlanId(planId: string): Promise<{ Plan: any }> {
    try {
      const Plan = await this.PlanModel
        .findOne({ planId })
        .populate({
          path: 'reviews', model: 'Review',
          populate: { path: 'toUserId', model: 'User' }
        })
      return { Plan };
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }
  public async Search({
    to,
    from,
    id,
    date,
    space,
  }: {
    to: string;
    from: string;
    date: Date | string | undefined;
    space: number | undefined;
    id: string;
  }): Promise<{ Plans: IPlan[] }> {
    try {
      let PlanRecord = await this.PlanModel.find({
        to,
        from,
        profileId: { $ne: id },
        date: { $gte: new Date() },
        booked: false,
      })
        .populate({ path: 'profileId', model: 'Profile' })
        .populate({ path: 'to', model: 'Airport' })
        .populate({ path: 'from', model: 'Airport' });

      if (date) {
        PlanRecord = PlanRecord.filter(x => moment(x.date).isBefore(date));
      }

      if (space) {
        PlanRecord = PlanRecord.filter(x => x.space >= space);
      }
      return { Plans: PlanRecord };
    } catch (e) {
      this.logger.error(e);
      throw e;
    }
  }
}
