import mongoose from 'mongoose';
import { IProfile } from '../interfaces/IProfile';

const Profile = new mongoose.Schema(
  {
    avatarUrl: {
      type: String,
    },
    firstName: {
      type: String,
    },
    lastName: {
      type: String,
    },
    contactEmail: {
      type: String,
      lowercase: true,
      unique: true,
      index: true,
    },
    contactPhone: String,
    country: {
      type: String,
    },
    favouriteOrders: {type: [mongoose.SchemaTypes.ObjectId], ref: 'order'},
    favouritePlan: {type: [mongoose.SchemaTypes.ObjectId], ref: 'plan'},
    loginId: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'user',
    },
  },
  { timestamps: true },
);

export default mongoose.model<IProfile & mongoose.Document>('Profile', Profile);
