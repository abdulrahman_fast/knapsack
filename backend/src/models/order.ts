import mongoose from 'mongoose';
import { IOrder } from '../interfaces/IOrder';

const Order = new mongoose.Schema(
  {
    from: {
      type: String,
    },
    to: {
      type: String,
    },
    date: {
      type: Date,
    },
    itemName: {
      type: String,
    },
    weight: Number,
    price: {
      type: Number,
    },
    booked: {
      type: Boolean,
      default: false
    },
    note: String,
    reviews: {
      type: [mongoose.SchemaTypes.ObjectId],
      ref: 'review',
    },
    profileId: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'profile',
    },
  },
  { timestamps: true },
);

export default mongoose.model<IOrder & mongoose.Document>('Order', Order);
