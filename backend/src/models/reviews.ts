import mongoose from 'mongoose';
import { IReview } from '../interfaces/IReview';

const Review = new mongoose.Schema(
  {
    fromUserId: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'user',
    },
    toUserId: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'user',
    },
    rating: Number,
    note: String,
  },
  { timestamps: true },
);

export default mongoose.model<IReview & mongoose.Document>('Review', Review);
