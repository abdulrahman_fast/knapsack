import mongoose from 'mongoose';
import { IFile } from '../interfaces/IFile';

const File = new mongoose.Schema(
  {
    filename: {
      type: String,
      required: true,
    },
    path: {
      type: String,
      required: true,
    },
    size: {
      type: Number,
      required: true,
    },
    fieldname: {
      type: String,
      required: true,
    },
    originalname: {
      type: String,
      required: true,
    },
    encoding: {
      type: String,
      required: true,
    },
    mimetype: {
      type: String,
      requird: true,
    },
    destination: {
      type: String,
      requird: true,
    },
  },
  {
    timestamps: true,
  },
);
export default mongoose.model<IFile & mongoose.Document>('File', File);
