import mongoose from 'mongoose';
import { IRequest } from '../interfaces/IRequest';

const Request = new mongoose.Schema(
  {
    fromUserId: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'profile',
    },
    status: {
      type: String,
      enum: ['pending', 'rejected', 'approved'],
    },
    place: String,
    date: Date,
    weight: Number,
    itemName: String,
    offer: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'Offer',
    },
    price: Number,
    orderId: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'order',
    },
    planId: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'plan',
    },
    note: String,
  },
  { timestamps: true },
);

export default mongoose.model<IRequest & mongoose.Document>('Request', Request);
