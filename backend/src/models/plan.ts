import mongoose from 'mongoose';
import { IPlan } from '../interfaces/IPlan';

const Plan = new mongoose.Schema(
  {
    from: {
      type: String,
    },
    to: {
      type: String,
    },
    date: {
      type: Date,
    },
    itemName: {
      type: String,
    },
    space: Number,
    price: {
      type: Number,
    },
    reviews: {
      type: [mongoose.SchemaTypes.ObjectId],
      ref: 'review',
    },
    booked: {
      type: Boolean,
      default: false
    },
    note: String,
    profileId: {
      type: mongoose.SchemaTypes.ObjectId,
      ref: 'profile',
    },
  },
  { timestamps: true },
);

export default mongoose.model<IPlan & mongoose.Document>('Plan', Plan);
