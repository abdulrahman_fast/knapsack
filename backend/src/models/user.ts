import { IUser } from '../interfaces/IUser';
import mongoose from 'mongoose';

const User = new mongoose.Schema(
  {
    name: {
      type: String,
      index: true,
    },
    email: {
      type: String,
      lowercase: true,
      unique: true,
      index: true,
    },
    tokenValidated: {
      type: Boolean,
    },
    reviews: {
      type: [mongoose.SchemaTypes.ObjectId],
      ref: 'review',
    },
    password: String,
    salt: String,
    role: {
      type: String,
      default: 'user',
    },
  },
  { timestamps: true },
);

export default mongoose.model<IUser & mongoose.Document>('User', User);
