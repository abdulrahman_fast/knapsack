import mongoose from 'mongoose';
import { IAirport } from '../interfaces/IAirport';

const Aiport = new mongoose.Schema(
  {
    name: String,
    city: String,
    country: String,
    iata_code: String,
    _geoloc: {
      lat: Number,
      lng: Number,
    },
    links_count: String,
    objectID: String,
  },
  { timestamps: true },
);

export default mongoose.model<IAirport & mongoose.Document>('Airport', Aiport);
