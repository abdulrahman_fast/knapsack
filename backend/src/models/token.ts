import mongoose, { Schema } from 'mongoose';
import { IToken } from '../interfaces/IToken';
const Token = new mongoose.Schema(
  {
    token: {
      type: String,
      index: true,
    },
    loginId: {
      type: Schema.Types.ObjectId,
      ref: 'user',
      required: true,
    },
    expiresAt: {
      type: Date,
    },
  },
  { timestamps: true },
);

export default mongoose.model<IToken & mongoose.Document>('Token', Token);
