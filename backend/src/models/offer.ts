import mongoose from 'mongoose';
import { IOffer } from '../interfaces/IOffer';

const Offer = new mongoose.Schema(
    {
        fromUserId: {
            type: mongoose.SchemaTypes.ObjectId,
            ref: 'profile',
        },
        toUserId: {
            type: mongoose.SchemaTypes.ObjectId,
            ref: 'profile'
        },
        weight: Number,
        price: Number,
        orderId: {
            type: mongoose.SchemaTypes.ObjectId,
            ref: 'order',
        },
        travellerReview: {
            type: mongoose.SchemaTypes.ObjectId,
            ref: 'review',
        },
        ordererReview: {
            type: mongoose.SchemaTypes.ObjectId,
            ref: 'review',
        },
        planId:  {
            type: mongoose.SchemaTypes.ObjectId,
            ref: 'plan',
        },
        status: {
            type: String,
            default: 'pending',
            enum: ['pending', 'accepted', 'recieved', 'reviewed'],
        },
    },
    { timestamps: true },
);

export default mongoose.model<IOffer & mongoose.Document>('Offer', Offer);
