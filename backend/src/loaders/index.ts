import expressLoader from './express';
import dependencyInjectorLoader from './dependencyInjector';
import mongooseLoader from './mongoose';
import jobsLoader from './jobs';
import Logger from './logger';
//We have to import at least all the events once so they can be triggered
import './events';

export default async ({ expressApp }) => {
  const mongoConnection = await mongooseLoader();
  Logger.info('✌️ DB loaded and connected!');

  /**
   * WTF is going on here?
   *
   * We are injecting the mongoose models into the DI container.
   * I know this is controversial but will provide a lot of flexibility at the time
   * of writing unit tests, just go and check how beautiful they are!
   */

  const userModel = {
    name: 'userModel',
    // Notice the require syntax and the '.default'
    model: require('../models/user').default,
  };
  const tokenModel = {
    name: 'tokenModel',
    // Notice the require syntax and the '.default'
    model: require('../models/token').default,
  };

  const profileModel = {
    name: 'profileModel',
    model: require('../models/profile').default,
  };
  const orderModel = {
    name: 'orderModel',
    model: require('../models/order').default,
  };
  const planModel = {
    name: 'planModel',
    model: require('../models/plan').default,
  };
  const fileModel = {
    name: 'fileModel',
    model: require('../models/file').default,
  };

  const requestModel = {
    name: 'requestModel',
    model: require('../models/request').default,
  };

  const airportModel = {
    name: 'airportModel',
    model: require('../models/airport').default,
  };
  const offerModel = {
    name: 'offerMode',
    model: require('../models/offer').default,
  };
  const reviewModel = {
    name: 'reviewModel',
    model: require('../models/reviews').default,
  };

  // It returns the agenda instance because it's needed in the subsequent loaders
  const { agenda } = await dependencyInjectorLoader({
    mongoConnection,
    models: [
      userModel,
      tokenModel,
      profileModel,
      orderModel,
      fileModel,
      planModel,
      airportModel,
      requestModel,
      reviewModel,
      offerModel
    ],
  });
  Logger.info('✌️ Dependency Injector loaded');

  await jobsLoader({ agenda });
  Logger.info('✌️ Jobs loaded');

  await expressLoader({ app: expressApp });
  Logger.info('✌️ Express loaded');
};
